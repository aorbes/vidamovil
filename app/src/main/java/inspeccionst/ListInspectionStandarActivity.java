package inspeccionst;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AuditRegistryDao;
import com.movil.vida.dao.DaoSession;

public class ListInspectionStandarActivity extends AppCompatActivity {
    private RecyclerView listInspectionSt;
    private Toolbar toolbar;
    private InspectionStandardAdapter standardAdapter;
    AuditRegistryDao auditRegistryDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_inspection_standar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inspecciones Estándar Registrados");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        auditRegistryDao = daoSession.getAuditRegistryDao();

        standardAdapter = new InspectionStandardAdapter(this, auditRegistryDao.loadAll());
        listInspectionSt = (RecyclerView) findViewById(R.id.listInspectionSt);
        listInspectionSt.setHasFixedSize(true);
        listInspectionSt.setAdapter(standardAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listInspectionSt.setLayoutManager(llm);
    }
}
