package inspeccionst;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AppPhotos;
import com.movil.vida.dao.AppPhotosDao;
import com.movil.vida.dao.AuditFieldRegistryDao;
import com.movil.vida.dao.AuditItemRegistryDao;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.AuditRegistryDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAuditDao;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

public class InspectionStandardAdapter extends RecyclerView.Adapter<InspectionStandardAdapter.MyViewHolder> {

    private Context context;
    private List<AuditRegistry> auditRegistries;
    GeneralAuditDao generalAuditDao;
    AuditFieldRegistryDao auditFieldRegistryDao;
    AppPhotosDao appPhotosDao;
    AuditItemRegistryDao auditItemRegistryDao;
    AuditRegistryDao auditRegistryDao;

    public InspectionStandardAdapter(Context context, List<AuditRegistry> auditRegistries) {
        this.context = context;
        this.auditRegistries = auditRegistries;
        DaoSession daoSession = ((DaoApp) context.getApplicationContext()).getDaoSession();
        generalAuditDao = daoSession.getGeneralAuditDao();
        auditFieldRegistryDao = daoSession.getAuditFieldRegistryDao();
        appPhotosDao = daoSession.getAppPhotosDao();
        auditItemRegistryDao = daoSession.getAuditItemRegistryDao();
        auditRegistryDao = daoSession.getAuditRegistryDao();

    }

    @Override
    public InspectionStandardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_inspection_st_items, parent, false);
        return new InspectionStandardAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(InspectionStandardAdapter.MyViewHolder holder, int position) {
        holder.lblDate.setText(auditRegistries.get(position).getRegistrationDate());
        holder.lblStandard.setText(generalAuditDao.load(auditRegistries.get(position).getGeneralAuditId()).getName());
        holder.lblResult.setText(auditRegistries.get(position).getResult());
        int result = Integer.parseInt(auditRegistries.get(position).getResult());
        if (result > 98) {
            holder.lblIndicator.setText("Excelente");
        } else if (result > 95 && result < 98) {
            holder.lblIndicator.setText("Muy Bueno");
        } else if (result > 90 && result < 95) {
            holder.lblIndicator.setText("Bueno");
        } else if (result > 85 && result < 90) {
            holder.lblIndicator.setText("Regular");
        } else if (result > 75 && result < 85) {
            holder.lblIndicator.setText("Insatisfactorio");
        } else {
            holder.lblIndicator.setText("Malo");
        }
        if (auditRegistries.get(position).getSynState() == 0) {
            holder.imgSync.setImageResource(R.drawable.ic_sync_black_24dp);
        } else {
            holder.imgSync.setImageResource(R.drawable.ic_check_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return auditRegistries.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgSync;
        TextView lblDate;
        TextView lblStandard;
        TextView lblResult;
        TextView lblIndicator;
        Button btnEdit;
        Button btnDelete;

        public MyViewHolder(View itemView) {
            super(itemView);
            imgSync = itemView.findViewById(R.id.imgSync);
            lblDate = itemView.findViewById(R.id.lblDate);
            lblStandard = itemView.findViewById(R.id.lblStandard);
            lblResult = itemView.findViewById(R.id.lblResult);
            lblIndicator = itemView.findViewById(R.id.lblIndicator);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnEdit.setOnClickListener(view -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    AuditRegistry auditRegistry = auditRegistries.get(getAdapterPosition());
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    args.putLong("InspectionStId", auditRegistry.getId());
                    Intent intent = new Intent(view.getContext(), WizardAddInspectionStActivity.class);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(view -> {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setTitle("Eliminar el Registro?");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("Eliminar", (v2, v3) -> {
                    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                        AuditRegistry auditRegistry = auditRegistries.get(getAdapterPosition());
                        auditFieldRegistryDao.queryBuilder().where(AuditFieldRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                        auditItemRegistryDao.queryBuilder().where(AuditItemRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                        QueryBuilder<AppPhotos> qb = appPhotosDao.queryBuilder();
                        qb.and(AppPhotosDao.Properties.ParentId.eq(auditRegistry.getId()), AppPhotosDao.Properties.Type.eq("IE"));
                        qb.buildDelete().executeDeleteWithoutDetachingEntities();
                        auditRegistryDao.delete(auditRegistry);
                        auditRegistries.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                        Toast.makeText(context, "Se eliminó el registro correctamente", Toast.LENGTH_SHORT).show();
                    }
                });
                adb.setNegativeButton("Cancelar", (v3, v4) -> {
                });
                adb.show();
            });
        }
    }
}
