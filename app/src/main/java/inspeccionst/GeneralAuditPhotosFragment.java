package inspeccionst;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.movil.vida.BuildConfig;
import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AppPhotos;
import com.movil.vida.dao.AppPhotosDao;
import com.movil.vida.dao.AuditFieldRegistry;
import com.movil.vida.dao.AuditFieldRegistryDao;
import com.movil.vida.dao.AuditItemRegistry;
import com.movil.vida.dao.AuditItemRegistryDao;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.AuditRegistryDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAudit;
import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import adapters.SlideAdapter;
import di.GeneralAuditSingleton;
import me.relex.circleindicator.CircleIndicator;
import utils.FileUtil;
import utils.ImageUtils;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralAuditPhotosFragment extends Fragment implements Step, BlockingStep {

    private static final int IMAGE_GALLERY_REQUEST = 25;
    private static final int REQUEST_TAKE_PHOTO = 20;
    private ConstraintLayout lytSliderPhotosGenAudit;
    private FloatingActionButton btnAddImage;
    private FloatingActionButton btnSlideDeleteImage;
    private ViewPager viewPager;
    private SlideAdapter myadapter;
    CircleIndicator indicator;
    ArrayList<byte[]> listImages = new ArrayList<>();
    GeneralAudit generalAudit = GeneralAuditSingleton.getInstance();
    AppPhotosDao appPhotosDao;
    AuditRegistryDao auditRegistryDao;
    AuditFieldRegistryDao auditFieldRegistryDao;
    AuditItemRegistryDao auditItemRegistryDao;
    AuditRegistry auditRegistry = GeneralAuditSingleton.getAuditRegistryInstance();

    private AlertDialog dialogAddPhoto;
    private Button btnGallery;
    private Button btnCammera;
    private AlertDialog dialogDelete;
    private Button deleteImage;
    private Button cancelDelete;

    public GeneralAuditPhotosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_general_audit_photos, container, false);
        //init Daos

        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        appPhotosDao = daoSession.getAppPhotosDao();
        auditRegistryDao = daoSession.getAuditRegistryDao();
        auditFieldRegistryDao = daoSession.getAuditFieldRegistryDao();
        auditItemRegistryDao = daoSession.getAuditItemRegistryDao();

        lytSliderPhotosGenAudit = v.findViewById(R.id.lytSliderPhotosGenAudit);
        btnAddImage = lytSliderPhotosGenAudit.findViewById(R.id.btnSlideAddImage);
        btnSlideDeleteImage = lytSliderPhotosGenAudit.findViewById(R.id.btnSlideDeleteImage);
        btnAddImage.setOnClickListener(v1 -> {
            dialogAddPhoto.show();
        });

        btnSlideDeleteImage.setOnClickListener(v2 -> {
            dialogDelete.show();
        });


        if (auditRegistry.getId() != null) {
            listImages = new ArrayList<>();
            QueryBuilder<AppPhotos> qb = appPhotosDao.queryBuilder();
            qb.where(AppPhotosDao.Properties.ParentId.eq(auditRegistry.getId()), AppPhotosDao.Properties.Type.eq("IE"));
            for (AppPhotos photos : qb.list()) {
                listImages.add(photos.getPhoto());
            }
        }

        viewPager = v.findViewById(R.id.viewPagerImages);
        indicator = v.findViewById(R.id.indicator);
        myadapter = new SlideAdapter(getContext(), listImages, viewPager);
        viewPager.setAdapter(myadapter);
        indicator.setViewPager(viewPager);
        myadapter.registerDataSetObserver(indicator.getDataSetObserver());
        ViewPagerArrowIndicator viewPagerArrowIndicator = v.findViewById(R.id.viewPagerArrowIndicator);
        viewPagerArrowIndicator.bind(viewPager);
        hideDeleteButton(listImages.size());

//Add DeleteImage
        AlertDialog.Builder mBuilderDelImage = new AlertDialog.Builder(getContext());
        View mViewDelImage = inflater.inflate(R.layout.delete_layout, null);
        mBuilderDelImage.setView(mViewDelImage);
        dialogDelete = mBuilderDelImage.create();
        deleteImage = mViewDelImage.findViewById(R.id.btnDelete);
        deleteImage.setOnClickListener(view1 -> {
            try {
                myadapter.removeView();
                indicator.setViewPager(viewPager);
                hideDeleteButton(myadapter.getCount());
                dialogDelete.cancel();
            } catch (Exception e) {
                dialogDelete.cancel();
                Toast.makeText(getContext(), "Error al eliminar imagen", Toast.LENGTH_LONG).show();
            }
        });
        cancelDelete = mViewDelImage.findViewById(R.id.btnCanlcel);
        cancelDelete.setOnClickListener(view1 -> {
            dialogDelete.cancel();
        });
        // init dialogsAddImage
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.cammera_or_gallery, null);
        mBuilder.setView(mView);
        dialogAddPhoto = mBuilder.create();
        btnGallery = mView.findViewById(R.id.btnGallery);
        btnCammera = mView.findViewById(R.id.btnCammera);
        btnGallery.setOnClickListener(view -> {
            onImageGalleryClicked();
        });
        btnCammera.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            5);
                }
            } else {
                dispatchTakePictureIntent();
            }
        });
        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }
        }
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(getContext());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    public void onImageGalleryClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);
        photoPickerIntent.setDataAndType(data, "image/*");
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }

    public void hideDeleteButton(int size) {
        if (size > 0) {
            btnSlideDeleteImage.setVisibility(View.VISIBLE);
        } else {
            btnSlideDeleteImage.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialogAddPhoto.cancel();
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                Bitmap myBitmap = ImageUtils.compressImage(new File(ImageUtils.currentImagePath), getContext());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                listImages.add(byteArray);
                myadapter.notifyDataSetChanged();
                viewPager.setCurrentItem(myadapter.getCount());
                hideDeleteButton(myadapter.getCount());
            }
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                try {
                    Bitmap image = ImageUtils.compressImage(FileUtil.from(getContext(), data.getData()), getContext());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    listImages.add(byteArray);
                    myadapter.notifyDataSetChanged();
                    viewPager.setCurrentItem(myadapter.getCount());
                    hideDeleteButton(myadapter.getCount());

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "No se puede abrir la imagen", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        Long auditRegId;
        if (auditRegistry.getId() != null) {
            auditRegistryDao.update(auditRegistry);
            auditRegId = auditRegistry.getId();
            Toast.makeText(getContext(), "Se ha actualizado el registro ", Toast.LENGTH_SHORT).show();
        } else {
            auditRegistry.setSynState(0);
            auditRegId = auditRegistryDao.insert(auditRegistry);
            Toast.makeText(getContext(), "Se ha agregado un nuevo registro ", Toast.LENGTH_SHORT).show();
        }
        auditFieldRegistryDao.queryBuilder().where(AuditFieldRegistryDao.Properties.AuditId.eq(auditRegId)).buildDelete().executeDeleteWithoutDetachingEntities();
        for (AuditFieldRegistry fieldRegistry : GeneralAuditSingleton.getAuditFieldRegistriesInstance()) {
            fieldRegistry.setAuditId(auditRegId);
            auditFieldRegistryDao.insert(fieldRegistry);
        }
        auditItemRegistryDao.queryBuilder().where(AuditItemRegistryDao.Properties.AuditId.eq(auditRegId)).buildDelete().executeDeleteWithoutDetachingEntities();
        for (AuditItemRegistry itemRegistry : GeneralAuditSingleton.getAuditItemRegistriesInstance()) {
            itemRegistry.setAuditId(auditRegId);
            auditItemRegistryDao.insert(itemRegistry);
        }
        QueryBuilder<AppPhotos> qb = appPhotosDao.queryBuilder();
        qb.where(AppPhotosDao.Properties.ParentId.eq(auditRegistry.getId()), AppPhotosDao.Properties.Type.eq("IE"));
        qb.buildDelete().executeDeleteWithoutDetachingEntities();
        for (byte[] photo : listImages) {
            AppPhotos photos = new AppPhotos();
            photos.setParentId(auditRegistry.getId());
            photos.setType("IE");
            photos.setPhoto(photo);
            appPhotosDao.insert(photos);
        }

        startActivity(new Intent(getContext(), MenuInspectionStActivity.class));
        GeneralAuditSingleton.emptyAuditFieldRegistries();
        GeneralAuditSingleton.emptyAuditItemRegistries();
        GeneralAuditSingleton.emptyAuditResgistry();
        GeneralAuditSingleton.emptyGeneralAudit();
        getActivity().finish();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
