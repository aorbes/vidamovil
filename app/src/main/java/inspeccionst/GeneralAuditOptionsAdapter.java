package inspeccionst;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.dao.GeneralAudit;

import java.util.List;

import di.GeneralAuditSingleton;

public class GeneralAuditOptionsAdapter extends RecyclerView.Adapter<GeneralAuditOptionsAdapter.MyViewHolder> {
    private List<GeneralAudit> options;
    private Context context;

    public GeneralAuditOptionsAdapter(List<GeneralAudit> options, Context context) {
        this.options = options;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inspection_optins, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.lblOptiondescrption.setText(options.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView lblOptiondescrption;
        private CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            lblOptiondescrption = itemView.findViewById(R.id.lblOptiondescrption);
            cardView = itemView.findViewById(R.id.cardView);
            cardView.setOnClickListener(v1 -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    GeneralAudit i = options.get(getAdapterPosition());
                    GeneralAuditSingleton.getInstance().setId(Long.valueOf(i.getId()));
                    context.startActivity(new Intent(context, WizardAddInspectionStActivity.class));
                    AppCompatActivity activity = (AppCompatActivity) v1.getContext();
                    activity.finish();
                }
            });

        }
    }
}
