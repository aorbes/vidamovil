package inspeccionst;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.Area;
import com.movil.vida.dao.AreaDao;
import com.movil.vida.dao.AuditFieldRegistry;
import com.movil.vida.dao.AuditFieldRegistryDao;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.Contractor;
import com.movil.vida.dao.ContractorDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAudit;
import com.movil.vida.dao.GeneralAuditField;
import com.movil.vida.dao.GeneralAuditFieldDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.stringSpinnerAdapter;
import beans.EditTextBean;
import beans.SpinnerBean;
import di.GeneralAuditSingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionStGeneralInfoFragment extends Fragment implements Step, BlockingStep, DatePickerDialog.OnDateSetListener {


    private EditText date;
    private Long dateFieldId;
    private ScrollView scroll;

    //dinamic fields
    private LinearLayout mainLayout;
    private Spinner spMainPlace;
    private Spinner spinner;
    private EditText inputText;
    GeneralAuditFieldDao auditFieldDao;
    UserDao userDao;
    AreaDao areaDao;
    ContractorDao contractorDao;
    AuditFieldRegistryDao auditFieldRegistryDao;
    GeneralAuditFieldDao generalAuditFieldDao;
    List<SpinnerBean> spinners = new ArrayList<>();
    List<EditTextBean> inputs = new ArrayList<>();
    GeneralAudit generalAudit = GeneralAuditSingleton.getInstance();
    AuditRegistry auditRegistry = GeneralAuditSingleton.getAuditRegistryInstance();
    private String emails = "";
    private MultiSpinner multiSpinner;
    private MultiSpinner.MultiSpinnerListener onSelectedListener;
    List<AuditFieldRegistry> auditFieldRegistries = GeneralAuditSingleton.getAuditFieldRegistriesInstance();
    private AuditFieldRegistry field;

    public InspectionStGeneralInfoFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inspection_st_general_info, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        auditFieldDao = daoSession.getGeneralAuditFieldDao();
        userDao = daoSession.getUserDao();
        areaDao = daoSession.getAreaDao();
        contractorDao = daoSession.getContractorDao();
        auditFieldRegistryDao = daoSession.getAuditFieldRegistryDao();
        generalAuditFieldDao = daoSession.getGeneralAuditFieldDao();

        scroll = v.findViewById(R.id.scrollInspectionStGenData);
        mainLayout = v.findViewById(R.id.mainLayout);
        mainLayout.setFocusableInTouchMode(true);
        spMainPlace = v.findViewById(R.id.spMainPlace);
        List<String> iPlace = new ArrayList<>();
        iPlace.add("Unacem");
        iPlace.add("Cantyvol");

        stringSpinnerAdapter stringSpinnerAdapterPlaces = new stringSpinnerAdapter(getContext(), iPlace);
        spMainPlace.setAdapter(stringSpinnerAdapterPlaces);
        LinearLayout.LayoutParams layoutParamsInputs = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParamsInputs.setMargins(0, 0, 0, 16);
        LinearLayout.LayoutParams layoutParamsLabels = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParamsLabels.setMargins(0, 0, 0, 8);

        List<GeneralAuditField> fields = auditFieldDao.queryBuilder().where(GeneralAuditFieldDao.Properties.AuditId.eq(generalAudit.getId())).list();
        final int sdk = android.os.Build.VERSION.SDK_INT;
        final List<User> listUser = userDao.loadAll();
        Collections.sort(listUser, (user1, user2) -> {
            return user1.getName().compareTo(user2.getName());
        });

        List<String> userNames = new ArrayList<>();
        userNames.add(getResources().getString(R.string.select_option));
        for (User user : userDao.loadAll()) {
            userNames.add(user.getName());
        }
        if (userNames.size() > 0) {
            Collections.sort(userNames.subList(1, userNames.size()));
        }
        stringSpinnerAdapter userSpinnerAdapter = new stringSpinnerAdapter(getContext(), userNames);


        List<String> areaNames = new ArrayList<>();
        areaNames.add(getResources().getString(R.string.select_option));
        for (Area area : areaDao.loadAll()) {
            areaNames.add(area.getName());
        }
        if (areaNames.size() > 0) {
            Collections.sort(areaNames.subList(1, areaNames.size()));
        }
        stringSpinnerAdapter adapterArea = new stringSpinnerAdapter(getContext(), areaNames);


        List<String> contractorNames = new ArrayList<>();
        contractorNames.add(getResources().getString(R.string.select_option));
        for (Contractor contractor : contractorDao.loadAll()) {
            contractorNames.add(contractor.getName());
        }
        if (contractorNames.size() > 0) {
            Collections.sort(contractorNames.subList(1, contractorNames.size()));
        }
        stringSpinnerAdapter contractorSpinnerAdapter = new stringSpinnerAdapter(getContext(), contractorNames);

        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (16 * scale + 0.5f);
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).getType().equals("E")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText(fields.get(i).getName());
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                spinner = new Spinner(getContext());
                spinners.add(new SpinnerBean(
                        fields.get(i), spinner, textView));
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    spinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    spinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                spinner.setAdapter(userSpinnerAdapter);
                mainLayout.addView(spinner, layoutParamsInputs);
                continue;
            }
            if (fields.get(i).getType().equals("C")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText(fields.get(i).getName());
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                spinner = new Spinner(getContext());
                spinners.add(new SpinnerBean(
                        fields.get(i), spinner, textView));
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    spinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    spinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                spinner.setAdapter(contractorSpinnerAdapter);

                mainLayout.addView(spinner, layoutParamsInputs);
                continue;
            }
            if (fields.get(i).getType().equals("O")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText(fields.get(i).getName());
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                spinner = new Spinner(getContext());
                spinners.add(new SpinnerBean(
                        fields.get(i), spinner, textView));
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    spinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    spinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                spinner.setAdapter(stringSpinnerAdapterPlaces);

                mainLayout.addView(spinner, layoutParamsInputs);
                continue;
            }
            if (fields.get(i).getType().equals("D")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText("Fecha");
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                date = new EditText(getContext());
                dateFieldId = fields.get(i).getId();
                date.setFocusable(false);
                date.setHint(R.string.date_field);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    date.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    date.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                date.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_date_range_black_24dp, 0);

                date.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
                date.setOnClickListener(view -> {
                    Calendar now = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(InspectionStGeneralInfoFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.setTitle("Selecciona la Fecha");
                    datePickerDialog.show(getActivity().getFragmentManager(), "");
                });
                mainLayout.addView(date, layoutParamsInputs);
                continue;
            }
            if (fields.get(i).getType().equals("A")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText(fields.get(i).getName());
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                spinner = new Spinner(getContext());
                spinners.add(new SpinnerBean(
                        fields.get(i), spinner, textView));
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    spinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    spinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                spinner.setAdapter(adapterArea);

                mainLayout.addView(spinner, layoutParamsInputs);
                continue;
            }
            if (fields.get(i).getType().equals("S")) {
                TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textView.setText(fields.get(i).getName());
                textView.setBackground(null);
                textView.setFocusable(false);
                mainLayout.addView(textView, layoutParamsLabels);
                inputText = new EditText(getContext());
                inputs.add(new EditTextBean(fields.get(i), inputText));
                inputText.setHint(R.string.error_field_required);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    inputText.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    inputText.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                inputText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_mode_edit_black_24dp, 0);

                inputText.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
                mainLayout.addView(inputText, layoutParamsInputs);
                continue;
            }
        }

        TextView textView = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
        textView.setText("Reportar a");
        textView.setBackground(null);
        textView.setFocusable(false);
        mainLayout.addView(textView, layoutParamsLabels);

        multiSpinner = new MultiSpinner(getContext());
        multiSpinner.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
        multiSpinner.setHint("Seleccione los usuarios a notificar");
        multiSpinner.setTextSize(16);
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            multiSpinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
        } else {
            multiSpinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
        }
        multiSpinner.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_mode_edit_black_24dp, 0);
        final ArrayAdapter<User> adapterUser = new ArrayAdapter<User>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listUser);
        onSelectedListener = selected -> {
            emails = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    emails += adapterUser.getItem(i).getId() + ",";
                }
            }

        };
        multiSpinner.setAdapter(adapterUser, false, onSelectedListener);
        mainLayout.addView(multiSpinner, layoutParamsInputs);

        TextView view = new EditText(getContext());
        view.setFocusable(false);
        view.setBackground(null);
        mainLayout.addView(view, layoutParamsLabels);

        scroll = v.findViewById(R.id.scrollInspectionStGenData);

        if (auditRegistry.getId() != null) {
            auditFieldRegistries = auditFieldRegistryDao.queryBuilder().where(AuditFieldRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).list();
            //  Log.i("Hay estos campos",auditFieldRegistries.size()+"++++++");
            spMainPlace.setSelection(stringSpinnerAdapterPlaces.getPosition(auditRegistry.getMainPlace()));
            date.setText(auditRegistry.getRegistrationDate());
            emails = auditRegistry.getEmails();
            if (!auditRegistry.getEmails().equals("")) {
                boolean[] selectedItems = new boolean[adapterUser.getCount()];
                String[] listemails = auditRegistry.getEmails().split(",");
                for (int i = 0; i < listemails.length; i++) {
                    selectedItems[adapterUser.getPosition(userDao.load(Long.parseLong(listemails[i])))] = true;
                }
                multiSpinner.setSelected(selectedItems);
            }
            for (int i = 0; i < spinners.size(); i++) {
                for (AuditFieldRegistry fieldRegistry : auditFieldRegistries) {
                    GeneralAuditField field = generalAuditFieldDao.queryBuilder().where(GeneralAuditFieldDao.Properties.Id.eq(fieldRegistry.getFieldId())).unique();
                    if (spinners.get(i).getGeneralAuditField().getName().equals(field.getName())) {
                        if (spinners.get(i).getGeneralAuditField().getType().equals("E")) {
                            spinners.get(i).getSpinner().setSelection(userSpinnerAdapter.getPosition(fieldRegistry.getValue()));
                            continue;
                        }
                        if (spinners.get(i).getGeneralAuditField().getType().equals("C")) {
                            spinners.get(i).getSpinner().setSelection(contractorSpinnerAdapter.getPosition(fieldRegistry.getValue()));
                            continue;
                        }
                        if (spinners.get(i).getGeneralAuditField().getType().equals("O")) {
                            spinners.get(i).getSpinner().setSelection(stringSpinnerAdapterPlaces.getPosition(fieldRegistry.getValue()));
                            continue;
                        }
                        if (spinners.get(i).getGeneralAuditField().getType().equals("A")) {
                            spinners.get(i).getSpinner().setSelection(adapterArea.getPosition(fieldRegistry.getValue()));
                            continue;
                        }
                    }
                }
            }
            for (int i = 0; i < inputs.size(); i++) {
                for (AuditFieldRegistry fieldRegistry : auditFieldRegistries) {
                    GeneralAuditField field = generalAuditFieldDao.queryBuilder().where(GeneralAuditFieldDao.Properties.Id.eq(fieldRegistry.getFieldId())).unique();
                    // Log.i("Input Values",field.getName()+"+++");
                    if (inputs.get(i).getGeneralAuditField().getName().equals(field.getName())) {
                        inputs.get(i).getEditText().setText(fieldRegistry.getValue());
                    }
                }
            }
        }
        return v;
    }

    boolean validate() {
        if (date.getText().toString().isEmpty() || date.getText().toString().contains(getString(R.string.error_field_required))) {
            date.setError(getString(R.string.error_field_required));
            date.setTextColor(Color.RED);
            date.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, date.getTop());
            });
            return false;
        }
        for (SpinnerBean spinnerBean : spinners) {
            if (spinnerBean.getSpinner().getSelectedItem().toString().equals(getResources().getString(R.string.select_option))) {
                TextView errorText = (TextView) spinnerBean.getSpinner().getSelectedView();
                errorText.setError(getString(R.string.error_field_required));
                errorText.setTextColor(Color.RED);
                errorText.setText(getString(R.string.error_field_required));
                scroll.post(() -> {
                    scroll.smoothScrollTo(0, spinnerBean.getTextView().getTop());
                });
                return false;
            }
        }
        for (EditTextBean editTextBean : inputs) {
            if (editTextBean.getEditText().getText().toString().isEmpty()) {
                editTextBean.getEditText().requestFocus();
                editTextBean.getEditText().setError(getString(R.string.error_field_required));
                scroll.post(() -> {
                    scroll.smoothScrollTo(0, editTextBean.getEditText().getBottom());
                });
                return false;
            }
        }

        return true;
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            auditFieldRegistries = new ArrayList<>();
            auditRegistry.setAuditorId(userDao.queryBuilder().where(UserDao.Properties.Name.eq(Session.getUserName(getContext()))).unique().getId());
            // auditRegistry.setAuditorId(1L);
            auditRegistry.setCreatorUser(Session.getUserName(getContext()));
            auditRegistry.setGeneralAuditId(GeneralAuditSingleton.getInstance().getId());
            auditRegistry.setMainPlace(spMainPlace.getSelectedItem().toString());
            auditRegistry.setRegistrationDate(date.getText().toString());
            auditRegistry.setEmails(emails);
            GeneralAuditSingleton.setAuditRegistry(auditRegistry);

            for (SpinnerBean spinnerBean : spinners) {
                AuditFieldRegistry auditFieldRegistry = new AuditFieldRegistry();
                auditFieldRegistry.setFieldId(spinnerBean.getGeneralAuditField().getId());
                auditFieldRegistry.setValue(spinnerBean.getSpinner().getSelectedItem().toString());
                auditFieldRegistries.add(auditFieldRegistry);
            }
            for (EditTextBean editTextBean : inputs) {
                AuditFieldRegistry auditFieldRegistry = new AuditFieldRegistry();
                auditFieldRegistry.setValue(editTextBean.getEditText().getText().toString());
                auditFieldRegistry.setFieldId(editTextBean.getGeneralAuditField().getId());
                auditFieldRegistries.add(auditFieldRegistry);
            }
            AuditFieldRegistry auditFieldRegistry = new AuditFieldRegistry();
            auditFieldRegistry.setValue(date.getText().toString());
            auditFieldRegistry.setFieldId(dateFieldId);
            auditFieldRegistries.add(auditFieldRegistry);
            GeneralAuditSingleton.setAuditFieldRegistries(auditFieldRegistries);
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        GeneralAuditSingleton.emptyAuditFieldRegistries();
        GeneralAuditSingleton.emptyAuditItemRegistries();
        GeneralAuditSingleton.emptyAuditResgistry();
        GeneralAuditSingleton.emptyGeneralAudit();
        startActivity(new Intent(getContext(), MenuInspectionStActivity.class));
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        date.setTextColor(Color.BLACK);
        date.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
        date.setError(null);
    }
}
