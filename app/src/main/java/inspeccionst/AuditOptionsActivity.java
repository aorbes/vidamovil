package inspeccionst;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAuditDao;

public class AuditOptionsActivity extends AppCompatActivity {

    private RecyclerView listAuditOptions;
    private Toolbar toolbarAuditIptions;
    private GeneralAuditOptionsAdapter auditOptionsAdapter;
    GeneralAuditDao generalAuditDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_options);
        toolbarAuditIptions = (Toolbar) findViewById(R.id.toolbarAuditIptions);
        setSupportActionBar(toolbarAuditIptions);
        getSupportActionBar().setTitle("Inspeccionés Estándar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        generalAuditDao = daoSession.getGeneralAuditDao();

        auditOptionsAdapter = new GeneralAuditOptionsAdapter(generalAuditDao.loadAll(), this);

        listAuditOptions = (RecyclerView) findViewById(R.id.listAuditOptions);
        listAuditOptions.setHasFixedSize(true);
        listAuditOptions.setAdapter(auditOptionsAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listAuditOptions.setLayoutManager(llm);

    }
}
