package inspeccionst;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AppPhotos;
import com.movil.vida.dao.AppPhotosDao;
import com.movil.vida.dao.AuditFieldRegistry;
import com.movil.vida.dao.AuditFieldRegistryDao;
import com.movil.vida.dao.AuditItemRegistry;
import com.movil.vida.dao.AuditItemRegistryDao;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.AuditRegistryDao;
import com.movil.vida.dao.DaoSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class MenuInspectionStActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private LinearLayout layout_add_inspection_standar, list;
    private AuditRegistryDao auditRegistryDao;
    private TextView lblUploaded;
    private TextView lblNotUploaded;
    private Button btnUploadAudits;
    private AuditFieldRegistryDao auditFieldRegistryDao;
    private AppPhotosDao appPhotosDao;
    private AuditItemRegistryDao auditItemRegistryDao;
    String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_inspection_st);
        toolbar = (Toolbar) findViewById(R.id.inspections_standar_menu_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menú inspecciones estándar");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init Dao
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        auditRegistryDao = daoSession.getAuditRegistryDao();
        auditFieldRegistryDao = daoSession.getAuditFieldRegistryDao();
        auditItemRegistryDao = daoSession.getAuditItemRegistryDao();
        appPhotosDao = daoSession.getAppPhotosDao();
        url = Session.getBaseUrl(MenuInspectionStActivity.this) + "/uploadgeneralaudit";
        layout_add_inspection_standar = (LinearLayout) findViewById(R.id.layout_add_inspection_standar);
        layout_add_inspection_standar.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), AuditOptionsActivity.class));
        });
        list = (LinearLayout) findViewById(R.id.layout_list_inspection_standar);
        list.setOnClickListener(view -> {
            startActivity(new Intent(this, ListInspectionStandarActivity.class));
        });

        lblUploaded = (TextView) findViewById(R.id.lblUploaded);
        lblNotUploaded = (TextView) findViewById(R.id.lblNotUploaded);
        btnUploadAudits = (Button) findViewById(R.id.btnUploadAudits);
        btnUploadAudits.setOnClickListener(view -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(MenuInspectionStActivity.this);
            adb.setTitle("Iniciar la subida de registros a vida?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                if (auditRegistryDao.queryBuilder().where(AuditRegistryDao.Properties.SynState.eq(0)).list().size() > 0) {
                    new HttpRequestTask(MenuInspectionStActivity.this).execute();
                } else if (auditRegistryDao.loadAll().size() < 1) {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(MenuInspectionStActivity.this);
                    goLogin.setMessage("No hay registros!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("Aceptar", (dialogInterface1, i1) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                } else {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(MenuInspectionStActivity.this);
                    goLogin.setMessage("Todos los registros se han subido a vida!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("Aceptar", (dialogInterface2, i2) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                }
            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();
        });


        updateUploadeds();
    }

    private void updateUploadeds() {
        Long uploaded = auditRegistryDao.queryBuilder().where(AuditRegistryDao.Properties.SynState.eq(1)).count();
        lblUploaded.setText(uploaded + "");
        Long notUploaded = auditRegistryDao.queryBuilder().where(AuditRegistryDao.Properties.SynState.eq(0)).count();
        lblNotUploaded.setText(notUploaded + "");
    }

    private class HttpRequestTask extends AsyncTask<Void, String, Void> {
        public int total;
        public int uploaded = 0;
        public boolean conn = false;
        ProgressDialog progDailog;
        Context context;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();

        public HttpRequestTask(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total = auditRegistryDao.queryBuilder().where(AuditRegistryDao.Properties.SynState.eq(0)).list().size();
            progDailog = new ProgressDialog(MenuInspectionStActivity.this);
            progDailog.setTitle("Subiendo Registros");
            progDailog.setMessage("Espere mientras se suben los registros..." + uploaded + "/" + total);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (AuditRegistry auditRegistry : auditRegistryDao.queryBuilder().where(AuditRegistryDao.Properties.SynState.eq(0)).list()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("generalAuditId", auditRegistry.getGeneralAuditId());
                    jsonObject.put("mainPlace", auditRegistry.getMainPlace());
                    jsonObject.put("auditorId", auditRegistry.getAuditorId());
                    jsonObject.put("registrationDate", auditRegistry.getRegistrationDate());
                    jsonObject.put("emails", auditRegistry.getEmails());
                    jsonObject.put("userLogin", Session.getUserName(MenuInspectionStActivity.this));
                    jsonObject.put("result", auditRegistry.getResult());
                    jsonObject.put("observation", auditRegistry.getObservation());

                    //fill fiels
                    JSONArray jsonFiedls = new JSONArray();
                    for (AuditFieldRegistry fieldRegistry : auditFieldRegistryDao.queryBuilder().where(AuditFieldRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).list()) {
                        JSONObject jsonAction = new JSONObject();
                        jsonAction.put("id", fieldRegistry.getFieldId());
                        jsonAction.put("name", fieldRegistry.getValue());
                        jsonFiedls.put(jsonAction);
                    }
                    jsonObject.put("fields", jsonFiedls);

                    //fill items
                    JSONArray jsonItems = new JSONArray();
                    for (AuditItemRegistry itemRegistry : auditItemRegistryDao.queryBuilder().where(AuditItemRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).list()) {
                        JSONObject jsonAction = new JSONObject();
                        jsonAction.put("id", itemRegistry.getItemId());
                        jsonAction.put("name", itemRegistry.getSelectedOption());
                        jsonItems.put(jsonAction);
                    }
                    jsonObject.put("items", jsonItems);

                    //fill images
                    JSONArray arrayimages = new JSONArray();
                    for (AppPhotos appPhotos : appPhotosDao.queryBuilder().where(AppPhotosDao.Properties.ParentId.eq(auditRegistry.getId()), AppPhotosDao.Properties.Type.eq("IE")).list()) {
                        arrayimages.put(Base64.encodeToString(appPhotos.getPhoto(),
                                Base64.DEFAULT));
                    }
                    jsonObject.put("images", arrayimages);

                    HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
                    Log.i("Tyring to upload", entity.getBody());
                    ResponseEntity<String> response =
                            restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

                    // ResponseEntity<ExperientialPlan> responseEntity = restTemplate.postForEntity(url,  experientialPlan, ExperientialPlan.class);
                    conn = true;
                    Log.i("Uploaded", auditRegistry.getId() + "--------------------------------------");
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Espere mientras se suben los registros..." + uploaded++ + "/" + total);
                        auditRegistry.setSynState(1);
                        auditRegistryDao.update(auditRegistry);
                    } else {
                        conn = false;
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                updateUploadeds();
                android.support.v7.app.AlertDialog.Builder goLogin = new android.support.v7.app.AlertDialog.Builder(context);
                goLogin.setMessage("Se subió los registros a vida correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                android.support.v7.app.AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }
}
