package inspeccionst;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.AuditRegistryDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAuditDao;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import di.GeneralAuditSingleton;

public class WizardAddInspectionStActivity extends AppCompatActivity implements StepperLayout.StepperListener {
    private StepperLayout mStepperLayout;
    AuditRegistryDao auditRegistryDao;
    GeneralAuditDao generalAuditDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_add_inspection_st);
        Bundle b = getIntent().getExtras();

        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        auditRegistryDao = daoSession.getAuditRegistryDao();
        generalAuditDao = daoSession.getGeneralAuditDao();

        if (b != null) {
            GeneralAuditSingleton.getAuditRegistryInstance();
            AuditRegistry auditRegistry = auditRegistryDao.load(b.getLong("InspectionStId"));
            GeneralAuditSingleton.setAuditRegistry(auditRegistry);
            GeneralAuditSingleton.setGeneralAudit(generalAuditDao.load(auditRegistry.getGeneralAuditId()));

        }
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new InspectionStSteperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
        // startActivity(new Intent(getApplicationContext(), MenuInspectionStActivity.class));
    }
}
