package inspeccionst;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AuditItemRegistry;
import com.movil.vida.dao.AuditItemRegistryDao;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.GeneralAudit;
import com.movil.vida.dao.GeneralAuditCategory;
import com.movil.vida.dao.GeneralAuditCategoryDao;
import com.movil.vida.dao.GeneralAuditItem;
import com.movil.vida.dao.GeneralAuditItemDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import adapters.stringSpinnerAdapter;
import beans.AuditItemBean;
import di.GeneralAuditSingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionStSpecificDataFragment extends Fragment implements Step, BlockingStep {

    private Spinner spinner;
    private EditText txtComment;
    private CardView percetageView;
    //dinamic inputs
    private LinearLayout mainLayout;
    GeneralAuditCategoryDao auditCategoryDao;
    GeneralAuditItemDao auditItemDao;
    AuditItemRegistryDao auditItemRegistryDao;
    GeneralAudit generalAudit = GeneralAuditSingleton.getInstance();
    List<AuditItemBean> itemsAudit = new ArrayList<>();
    List<AuditItemRegistry> itemRegistries = GeneralAuditSingleton.getAuditItemRegistriesInstance();
    private SeekBar seekBarPercentage;
    private TextView lblPercentage;
    private TextView txtEvaluation;
    private int total = 0;
    private ScrollView scroll;
    private int percentage;
    AuditRegistry auditRegistry = GeneralAuditSingleton.getAuditRegistryInstance();
    public InspectionStSpecificDataFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inspection_st_specific_data, container, false);

        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        auditCategoryDao = daoSession.getGeneralAuditCategoryDao();
        auditItemDao = daoSession.getGeneralAuditItemDao();
        auditItemRegistryDao = daoSession.getAuditItemRegistryDao();

        mainLayout = v.findViewById(R.id.mainLayout);
        seekBarPercentage = v.findViewById(R.id.seekBarPercentage);
        seekBarPercentage.setMax(100);
//        seekBarPercentage.setMin(0);
        seekBarPercentage.setEnabled(false);
        lblPercentage = v.findViewById(R.id.lblPercentage);
        txtEvaluation = v.findViewById(R.id.txtEvaluation);
        percetageView = v.findViewById(R.id.percetageView);

        scroll = v.findViewById(R.id.scrollAuditItems);
        //percetageView.setBackgroundColor(Color.parseColor("#069dc9"));

        LinearLayout.LayoutParams layoutParamsLabels = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParamsLabels.setMargins(0, 0, 0, 8);
        LinearLayout.LayoutParams layoutParamsInputs = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );


        List<GeneralAuditCategory> categories = auditCategoryDao.queryBuilder().where(GeneralAuditCategoryDao.Properties.AuditId.eq(generalAudit.getId())).list();
        int sdk = android.os.Build.VERSION.SDK_INT;
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (16 * scale + 0.5f);
        for (GeneralAuditCategory auditCategory : categories) {
            TextView textView = new EditText(getContext());
            textView.setText(auditCategory.getName());
            textView.setBackground(null);
            textView.setFocusable(false);
            textView.setTextColor(getResources().getColor(R.color.blue_dark));
            mainLayout.addView(textView, layoutParamsLabels);
            for (GeneralAuditItem auditItem : auditItemDao.queryBuilder().where(GeneralAuditItemDao.Properties.AuditCategoryId.eq(auditCategory.getId())).list()) {
                total += 2;
                TextView textViewItem = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
                textViewItem.setText(auditItem.getName());
                textViewItem.setBackground(null);
                textViewItem.setFocusable(false);
                mainLayout.addView(textViewItem, layoutParamsLabels);

                spinner = new Spinner(getContext());

                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    spinner.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                } else {
                    spinner.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
                }
                String[] options = auditItem.getOptions().split("|");
                List<String> items = new ArrayList<>();
                items.add(getResources().getString(R.string.select_option));
                for (int i = 0; i < options.length; i++) {
                    if (options[i].equals("C")) {
                        items.add("Cumple");
                        continue;
                    }
                    if (options[i].equals("N")) {
                        items.add("No Cumple");
                        continue;
                    }
                    if (options[i].equals("P")) {
                        items.add("Cumple Parcialmente");
                        continue;
                    }
                    if (options[i].equals("A")) {
                        items.add("No Aplica");

                        continue;
                    }
                    if (options[i].equals("I")) {
                        items.add("No es posible inspecionar");
                        continue;
                    }
                }
                stringSpinnerAdapter spinnerAdapter = new stringSpinnerAdapter(getContext(), items);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    int count = 0;

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if (count >= 1) {
                            calculatePercentage();
                        }
                        count++;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                itemsAudit.add(new AuditItemBean(auditCategory.getId(), auditItem.getId(), null, spinner, textViewItem, spinnerAdapter));
                spinner.setAdapter(spinnerAdapter);
                mainLayout.addView(spinner, layoutParamsInputs);
                //Observations


            }
        }
        TextView lblTExtArea = new EditText(new ContextThemeWrapper(getContext(), R.style.InputLabel));
        lblTExtArea.setText("Observaciones");
        lblTExtArea.setBackground(null);
        lblTExtArea.setFocusable(false);
        mainLayout.addView(lblTExtArea, layoutParamsLabels);

        txtComment = new EditText(getContext());
        txtComment.setMinLines(3);
        txtComment.setHint("Ingrese Observaciones");
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            txtComment.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
        } else {
            txtComment.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.custom_spinner_background));
        }
        txtComment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_mode_edit_black_24dp, 0);
        txtComment.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
        mainLayout.addView(txtComment, layoutParamsInputs);

        TextView view = new EditText(getContext());
        view.setFocusable(false);
        view.setBackground(null);
        mainLayout.addView(view, layoutParamsLabels);
        if (auditRegistry.getId() != null) {
            //lblPercentage.setText(auditRegistry.getResult());
            itemRegistries = auditItemRegistryDao.queryBuilder().where(AuditItemRegistryDao.Properties.AuditId.eq(auditRegistry.getId())).list();
            txtComment.setText(auditRegistry.getObservation());
            for (int i = 0; i < itemsAudit.size(); i++) {
                for (AuditItemRegistry itemRegistry : itemRegistries) {
                    if (itemsAudit.get(i).getItemId().equals(itemRegistry.getItemId())) {
                        if (itemRegistry.getSelectedOption().equals("C")) {
                            itemsAudit.get(i).getSpinner().setSelection(itemsAudit.get(i).getAdapter().getPosition("Cumple"));
                        }
                        if (itemRegistry.getSelectedOption().equals("N")) {
                            itemsAudit.get(i).getSpinner().setSelection(itemsAudit.get(i).getAdapter().getPosition("No Cumple"));
                        }
                        if (itemRegistry.getSelectedOption().equals("P")) {
                            itemsAudit.get(i).getSpinner().setSelection(itemsAudit.get(i).getAdapter().getPosition("Cumple Parcialmente"));
                        }
                        if (itemRegistry.getSelectedOption().equals("A")) {
                            itemsAudit.get(i).getSpinner().setSelection(itemsAudit.get(i).getAdapter().getPosition("No Aplica"));
                        }
                        if (itemRegistry.getSelectedOption().equals("I")) {
                            itemsAudit.get(i).getSpinner().setSelection(itemsAudit.get(i).getAdapter().getPosition("No es posible inspecionar"));
                        }
                    }
                }
            }

        }
        calculatePercentage();
        return v;
    }

    void calculatePercentage() {
        int items = 0;
        int total = this.total;
        for (AuditItemBean item : itemsAudit) {
            if (item.getSpinner().getSelectedItem().toString().equals("Cumple")) {
                items += 2;
            } else if (item.getSpinner().getSelectedItem().toString().equals("Cumple Parcialmente")) {
                items += 1;
            } else if (item.getSpinner().getSelectedItem().toString().equals("No Aplica")) {
                total -= 2;
            }
        }
        float x = (float) items / total;
        percentage = Math.round(x * 100);
        lblPercentage.setText(percentage + "%");
        seekBarPercentage.setProgress(percentage);
        if (percentage > 98) {
            txtEvaluation.setText("Excelente");
        } else if (percentage > 95 && percentage < 98) {
            txtEvaluation.setText("Muy Bueno");
        } else if (percentage > 90 && percentage < 95) {
            txtEvaluation.setText("Bueno");
        } else if (percentage > 85 && percentage < 90) {
            txtEvaluation.setText("Regular");
        } else if (percentage > 75 && percentage < 85) {
            txtEvaluation.setText("Insatisfactorio");
        } else {
            txtEvaluation.setText("Malo");
        }
    }


    boolean validate() {
        for (AuditItemBean item : itemsAudit) {
            if (item.getSpinner().getSelectedItem().toString().equals(getResources().getString(R.string.select_option))) {
                TextView errorText = (TextView) item.getSpinner().getSelectedView();
                errorText.setError(getString(R.string.error_field_required));
                errorText.setTextColor(Color.RED);
                errorText.setText(getString(R.string.error_field_required));
                scroll.post(() -> {
                    scroll.smoothScrollTo(0, item.getTextView().getTop());
                });
                return false;
            }
        }
        return true;
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            itemRegistries = new ArrayList<>();
            for (AuditItemBean itemBean : itemsAudit) {
                AuditItemRegistry itemRegistry = new AuditItemRegistry();
                itemRegistry.setAuditId(generalAudit.getId());
                itemRegistry.setCategoryId(itemBean.getCategoryId());
                itemRegistry.setItemId(itemBean.getItemId());
                if (itemBean.getSpinner().getSelectedItem().equals("Cumple")) {
                    itemRegistry.setSelectedOption("C");
                }
                if (itemBean.getSpinner().getSelectedItem().equals("No Cumple")) {
                    itemRegistry.setSelectedOption("N");
                }
                if (itemBean.getSpinner().getSelectedItem().equals("Cumple Parcialmente")) {
                    itemRegistry.setSelectedOption("P");
                }
                if (itemBean.getSpinner().getSelectedItem().equals("No Aplica")) {
                    itemRegistry.setSelectedOption("A");
                }
                if (itemBean.getSpinner().getSelectedItem().equals("No es posible inspecionar")) {
                    itemRegistry.setSelectedOption("I");
                }
                itemRegistries.add(itemRegistry);
            }
            GeneralAuditSingleton.setAuditItemRegistries(itemRegistries);
            GeneralAuditSingleton.getAuditRegistryInstance().setObservation(txtComment.getText().toString());
            GeneralAuditSingleton.getAuditRegistryInstance().setResult(percentage + "");
            callback.goToNextStep();
        }


    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
}
