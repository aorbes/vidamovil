package inspeccions.AddInspectionAction;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.movil.vida.BuildConfig;
import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AreaInsActionImages;
import com.movil.vida.dao.AreaInsActionImagesDao;
import com.movil.vida.dao.AreaInspectionAction;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.DaoSession;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapters.SlideAdapter;
import di.areaInspectionActionSingleton;
import me.relex.circleindicator.CircleIndicator;
import utils.FileUtil;
import utils.ImageUtils;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddAreaInspectionDetailPhotoFragment extends Fragment implements Step, BlockingStep {


    private static final int IMAGE_GALLERY_REQUEST = 25;
    private static final int REQUEST_TAKE_PHOTO = 20;
    CircleIndicator indicator;
    private AreaInspectionActionDao area_inspection_actionDao;
    private AreaInspectionAction area_inspection_action = areaInspectionActionSingleton.getInstance();
    private AreaInsActionImagesDao area_ins_action_imagesDao;
    private ViewPager pager;
    private SlideAdapter adapter;
    private List<byte[]> images = new ArrayList<>();
    private ConstraintLayout lytSliderPhotos;
    private FloatingActionButton btnAddImage;
    private FloatingActionButton btnSlideDeleteImage;

    private AlertDialog dialogAddPhoto;
    private Button btnGallery;
    private Button btnCammera;
    private AlertDialog dialogDelete;
    private Button deleteImage;
    private Button cancelDelete;
    public AddAreaInspectionDetailPhotoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        area_inspection_actionDao = daoSession.getAreaInspectionActionDao();
        area_ins_action_imagesDao = daoSession.getAreaInsActionImagesDao();

        View view = inflater.inflate(R.layout.fragment_add_area_inspection_detail_photo, container, false);
        lytSliderPhotos = view.findViewById(R.id.lytAreaInsActionImages);
        btnAddImage = lytSliderPhotos.findViewById(R.id.btnSlideAddImage);
        btnSlideDeleteImage = lytSliderPhotos.findViewById(R.id.btnSlideDeleteImage);
        btnAddImage.setOnClickListener(v1 -> {
            dialogAddPhoto.show();
        });

        btnSlideDeleteImage.setOnClickListener(v2 -> {
            dialogDelete.show();
        });
        AlertDialog.Builder mBuilderDelImage = new AlertDialog.Builder(getContext());
        View mViewDelImage = inflater.inflate(R.layout.delete_layout, null);
        mBuilderDelImage.setView(mViewDelImage);
        dialogDelete = mBuilderDelImage.create();
        deleteImage = mViewDelImage.findViewById(R.id.btnDelete);
        deleteImage.setOnClickListener(view1 -> {
            try {
                adapter.removeView();
                indicator.setViewPager(pager);
                hideDeleteButton(adapter.getCount());
                dialogDelete.cancel();
            } catch (Exception e) {
                dialogDelete.cancel();
                Toast.makeText(getContext(), "Error al eliminar imagen", Toast.LENGTH_LONG).show();
            }
        });
        cancelDelete = mViewDelImage.findViewById(R.id.btnCanlcel);
        cancelDelete.setOnClickListener(view1 -> {
            dialogDelete.cancel();
        });
        // init dialogsAddImage
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.cammera_or_gallery, null);
        mBuilder.setView(mView);
        dialogAddPhoto = mBuilder.create();
        btnGallery = mView.findViewById(R.id.btnGallery);
        btnCammera = mView.findViewById(R.id.btnCammera);
        btnGallery.setOnClickListener(view2 -> {
            onImageGalleryClicked();
        });
        btnCammera.setOnClickListener(view2 -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            5);
                }
            } else {
                dispatchTakePictureIntent();
            }
        });

        pager = view.findViewById(R.id.viewPagerImages);
        if (area_inspection_action.getId() != null) {
            images = new ArrayList<>();
            for (AreaInsActionImages actionImages : area_ins_action_imagesDao.queryBuilder().where(AreaInsActionImagesDao.Properties.AreaInspectionActionId.eq(area_inspection_action.getId())).list()) {
                images.add(actionImages.getImage());
            }
        }
        adapter = new SlideAdapter(getContext(), images, pager);
        indicator = view.findViewById(R.id.indicator);
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);
        adapter.registerDataSetObserver(indicator.getDataSetObserver());
        return view;
    }

    public void hideDeleteButton(int size) {
        if (size > 0) {
            btnSlideDeleteImage.setVisibility(View.VISIBLE);
        } else {
            btnSlideDeleteImage.setVisibility(View.GONE);
        }
    }

    public void onImageGalleryClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);
        photoPickerIntent.setDataAndType(data, "image/*");
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }
        }
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(getContext());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialogAddPhoto.cancel();
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                Bitmap myBitmap = ImageUtils.compressImage(new File(ImageUtils.currentImagePath), getContext());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                images.add(byteArray);
                adapter.notifyDataSetChanged();
                pager.setCurrentItem(adapter.getCount());
                hideDeleteButton(adapter.getCount());
            }
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                try {
                    Bitmap image = ImageUtils.compressImage(FileUtil.from(getContext(), data.getData()), getContext());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    images.add(byteArray);
                    adapter.notifyDataSetChanged();
                    pager.setCurrentItem(adapter.getCount());
                    hideDeleteButton(adapter.getCount());

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "No se puede abrir la imagen", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        Long id;
        area_inspection_action.setInspectionId(areaInspectionActionSingleton.inspectionId);
        if (area_inspection_action.getId() == null) {
            area_inspection_action.setSyncState(0);
            id = area_inspection_actionDao.insert(area_inspection_action);
            Toast.makeText(getContext(), "Se agregado una nueva acción", Toast.LENGTH_LONG).show();
        } else {
            area_ins_action_imagesDao.queryBuilder().where(AreaInsActionImagesDao.Properties.AreaInspectionActionId.eq(area_inspection_action.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
            area_inspection_actionDao.update(area_inspection_action);
            id = area_inspection_action.getId();
            Toast.makeText(getContext(), "Se ha actualizado la acción", Toast.LENGTH_LONG).show();
        }

        for (byte[] photo : images) {
            AreaInsActionImages actionImages = new AreaInsActionImages();
            actionImages.setImage(photo);
            actionImages.setAreaInspectionActionId(id);
            actionImages.setInspectionId(areaInspectionActionSingleton.inspectionId);
            area_ins_action_imagesDao.insert(actionImages);
        }

        areaInspectionActionSingleton.emptyAreainspectionAction();
        startActivity(new Intent(getContext(), AreaInspectionsDetailsListActivity.class));
        getActivity().finish();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
}
