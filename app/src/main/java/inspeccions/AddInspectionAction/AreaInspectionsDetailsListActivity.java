package inspeccions.AddInspectionAction;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.DaoSession;

import di.areaInspectionActionSingleton;

public class AreaInspectionsDetailsListActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private FloatingActionButton btnAddAreaInspectionDetail;
    private InspectionsActionsAdapter actionsAdapter;
    private AreaInspectionActionDao areaInspectionActionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_inspections_details_list);
        toolbar = (Toolbar) findViewById(R.id.areainspectiondetailtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Acciones de la Inspección ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        areaInspectionActionDao = daoSession.getAreaInspectionActionDao();

        recyclerView = (RecyclerView) findViewById(R.id.areainspectiondeatillist);
        recyclerView.setHasFixedSize(true);
        btnAddAreaInspectionDetail = (FloatingActionButton) findViewById(R.id.btnAddAreaInspectionDetail);
        actionsAdapter = new InspectionsActionsAdapter(areaInspectionActionDao.queryBuilder().where(AreaInspectionActionDao.Properties.InspectionId.eq(areaInspectionActionSingleton.inspectionId)).list(), AreaInspectionsDetailsListActivity.this);
        recyclerView.setAdapter(actionsAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
        btnAddAreaInspectionDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AddInsAreaDetailsWizardActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
