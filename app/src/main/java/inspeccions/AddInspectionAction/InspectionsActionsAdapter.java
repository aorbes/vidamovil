package inspeccions.AddInspectionAction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AreaInspectionAction;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.DaoSession;

import java.util.List;

public class InspectionsActionsAdapter extends RecyclerView.Adapter<InspectionsActionsAdapter.MyViewHolder> {
    private AreaInspectionActionDao areaInspectionActionDao;
    private List<AreaInspectionAction> list;
    private Context context;


    public InspectionsActionsAdapter(List<AreaInspectionAction> list, Context context) {
        this.list = list;
        this.context = context;
        DaoSession daoSession = ((DaoApp) context.getApplicationContext()).getDaoSession();
        areaInspectionActionDao = daoSession.getAreaInspectionActionDao();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_inspection_action_item, parent, false);
        return new InspectionsActionsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.lblId.setText(list.get(position).getId().toString());
        holder.lblPlace.setText(list.get(position).getPlace());
        holder.lblType.setText(list.get(position).getActionType());
        holder.lblLimitDate.setText(list.get(position).getLimitDate());
        if (list.get(position).getSyncState() == 0) {
            holder.imgSyncState.setImageResource(R.drawable.ic_sync_black_24dp);
        } else {
            holder.imgSyncState.setImageResource(R.drawable.ic_check_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView lblId, lblPlace, lblType, lblLimitDate;
        private Button btnEdit, btnDelete;
        private ImageView imgSyncState;
        public MyViewHolder(View itemView) {
            super(itemView);
            lblId = itemView.findViewById(R.id.lblId);
            lblPlace = itemView.findViewById(R.id.lblPlace);
            lblType = itemView.findViewById(R.id.lblType);
            lblLimitDate = itemView.findViewById(R.id.lblLimitDate);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            imgSyncState = itemView.findViewById(R.id.imgSyncState);
            btnEdit.setOnClickListener(view -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    AreaInspectionAction action = list.get(getAdapterPosition());
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    args.putLong("InspectionActionId", action.getId());
                    Intent intent = new Intent(view.getContext(), AddInsAreaDetailsWizardActivity.class);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(v -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    AlertDialog.Builder adb = new AlertDialog.Builder(context);
                    adb.setTitle("Eliminar la Acción?");
                    adb.setIcon(android.R.drawable.ic_dialog_alert);
                    adb.setPositiveButton("Eliminar", (v2, v3) -> {
                        AreaInspectionAction i = list.get(getAdapterPosition());
                        areaInspectionActionDao.delete(i);
                        list.remove(getAdapterPosition());
                        notifyItemRemoved(getAdapterPosition());
                    });
                    adb.setNegativeButton("Cancelar", (v3, v4) -> {
                    });
                    adb.show();
                }
            });


        }
    }
}
