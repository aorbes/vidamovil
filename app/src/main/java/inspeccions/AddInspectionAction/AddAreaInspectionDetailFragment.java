package inspeccions.AddInspectionAction;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AreaInspectionAction;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import adapters.stringSpinnerAdapter;
import adapters.userSpinnerAdapter;
import di.areaInspectionActionSingleton;
import inspeccions.AddInspection.AreaInspectionsListActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddAreaInspectionDetailFragment extends Fragment implements Step, BlockingStep {
    private EditText txtareaincidentdetailplace;
    private EditText txtareaincidentdetailexactplace;
    private Spinner spAreaIncidentDetailType;
    private Spinner spAreaIncidentDetailInversion;
    private EditText txtAreaInspectionDetailCause;
    private EditText txtAreaInspectionDetailCorrective;
    private Spinner spAreaIncidentDetailPrority;
    private Spinner spAreaIncidentDetailActionEject;
    private EditText txtAreaInspectionDetailInitDate;
    private EditText txtAreaInspectionDetailExecutionDate;
    private Spinner spAreaInspectionDetailInforPerson;
    private MultiSpinner spAreaInspectionDetailClosePerson;
    private MultiSpinner.MultiSpinnerListener areaInspectionDeatailsCloseListener;
    private EditText txtAreaInspectionComment;
    private TextView lblPlace, lblExactPlace, lblCorrectiveAction,
            lblCauseDesciption, lblStartDate, lblExecDate, lblCloseInform;
    private UserDao userDao;
    private DatePickerDialog.OnDateSetListener from_dateListener, to_dateListener;
    private AreaInspectionAction area_inspection_action = areaInspectionActionSingleton.getInstance();
    private String closeUsersInfo = "";
    private ScrollView scroll;

    public AddAreaInspectionDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_area_inspection_detail, container, false);

        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        userDao = daoSession.getUserDao();
        from_dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                String x = "";
                String y = "";
                if (dayOfMonth < 10) {
                    x = "0";
                }
                if (monthOfYear < 10) {
                    y = "0";
                }

                txtAreaInspectionDetailInitDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
                txtAreaInspectionDetailInitDate.setError(null);
            }
        };


        to_dateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                String x = "";
                String y = "";
                if (dayOfMonth < 10) {
                    x = "0";
                }
                if (monthOfYear < 10) {
                    y = "0";
                }

                txtAreaInspectionDetailExecutionDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
                txtAreaInspectionDetailExecutionDate.setError(null);
            }
        };

        txtareaincidentdetailplace = view.findViewById(R.id.txtareaincidentdetailplace);
        lblPlace = view.findViewById(R.id.lblPlace);
        txtareaincidentdetailexactplace = view.findViewById(R.id.txtareaincidentdetailexactplace);
        lblExactPlace = view.findViewById(R.id.lblExactPlace);
        spAreaIncidentDetailType = view.findViewById(R.id.spAreaIncidentDetailType);
        List<String> iType = new ArrayList<>();
        iType.add("Correctiva");
        iType.add("Preventiva");
        stringSpinnerAdapter stringSpinnerAdapterType = new stringSpinnerAdapter(getContext(), iType);
        spAreaIncidentDetailType.setAdapter(stringSpinnerAdapterType);

        spAreaIncidentDetailInversion = view.findViewById(R.id.spAreaIncidentDetailInversion);
        List<String> iInversion = new ArrayList<>();
        iInversion.add("No");
        iInversion.add("Si");
        stringSpinnerAdapter stringSpinnerAdapterInversion = new stringSpinnerAdapter(getContext(), iInversion);
        spAreaIncidentDetailInversion.setAdapter(stringSpinnerAdapterInversion);

        txtAreaInspectionDetailCause = view.findViewById(R.id.txtAreaInspectionDetailCause);
        lblCauseDesciption = view.findViewById(R.id.lblCauseDesciption);

        txtAreaInspectionDetailCorrective = view.findViewById(R.id.txtAreaInspectionDetailCorrective);
        lblCorrectiveAction = view.findViewById(R.id.lblCorrectiveAction);

        spAreaIncidentDetailPrority = view.findViewById(R.id.spAreaIncidentDetailPrority);
        List<String> iPriority = new ArrayList<>();
        iPriority.add("Alto");
        iPriority.add("Bajo");
        stringSpinnerAdapter stringSpinnerAdapterPriority = new stringSpinnerAdapter(getContext(), iPriority);
        spAreaIncidentDetailPrority.setAdapter(stringSpinnerAdapterPriority);

        spAreaIncidentDetailActionEject = view.findViewById(R.id.spAreaIncidentDetailActionEject);
        final List<User> listUser = userDao.loadAll();
        // listUser.add(new User(9999l, "Seleccione un usuario", "select@email.com"));
        Collections.sort(listUser, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getName().compareTo(o2.getName());
            }

        });

        final userSpinnerAdapter userSpinnerAdapter = new userSpinnerAdapter(getContext(), listUser);
        spAreaIncidentDetailActionEject.setAdapter(userSpinnerAdapter);
        spAreaIncidentDetailActionEject.post(new Runnable() {

            @Override
            public void run() {
                int indexUser = -1;
                for (int i = 0; i < listUser.size(); i++) {
                    if (listUser.get(i).getName().equals(Session.getUserName(getContext()))) {
                        indexUser = i;
                        break;
                    }
                }
                if (indexUser > 0) {
                    spAreaIncidentDetailActionEject.setSelection(indexUser);
                }
              /*  TextView errorText = (TextView) customSpinner.getSelectedView();
                errorText.setError("anything here, just to add the icon");*/
            }
        });

        txtAreaInspectionDetailInitDate = view.findViewById(R.id.txtAreaInspectionDetailInitDate);
        lblStartDate = view.findViewById(R.id.lblStartDate);

        txtAreaInspectionDetailInitDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(from_dateListener,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setTitle("Selecciona la Fecha Inicial");
                datePickerDialog.show(getActivity().getFragmentManager(), "");

            }
        });

        txtAreaInspectionDetailExecutionDate = view.findViewById(R.id.txtAreaInspectionDetailExecutionDate);
        lblExecDate = view.findViewById(R.id.lblExecDate);
        txtAreaInspectionDetailExecutionDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(to_dateListener,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setTitle("Selecciona la Fecha de Ejecución");
                datePickerDialog.show(getActivity().getFragmentManager(), "");
            }
        });

        spAreaInspectionDetailInforPerson = view.findViewById(R.id.spAreaIncidentDetailInforPerson);
        spAreaInspectionDetailInforPerson.setAdapter(userSpinnerAdapter);
        spAreaInspectionDetailInforPerson.post(new Runnable() {
            @Override
            public void run() {
                int indexUser = -1;
                for (int i = 0; i < listUser.size(); i++) {
                    if (listUser.get(i).getName().equals(Session.getUserName(getContext()))) {
                        indexUser = i;
                        break;
                    }
                }
                if (indexUser > 0) {
                    spAreaInspectionDetailInforPerson.setSelection(indexUser);
                }
              /*  TextView errorText = (TextView) customSpinner.getSelectedView();
                errorText.setError("anything here, just to add the icon");*/
            }
        });

        spAreaInspectionDetailClosePerson = view.findViewById(R.id.spAreaIncidentDetailClosePerson);
        lblCloseInform = view.findViewById(R.id.lblCloseInform);
        areaInspectionDeatailsCloseListener = selected -> {
            closeUsersInfo = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    closeUsersInfo += userSpinnerAdapter.getItem(i).getId() + ",";
                }
                spAreaInspectionDetailClosePerson.setError(null);
            }
        };
        spAreaInspectionDetailClosePerson.setAdapter(userSpinnerAdapter, false, areaInspectionDeatailsCloseListener);


        txtAreaInspectionComment = view.findViewById(R.id.txtAreaInspectionComment);
        from_dateListener = (view1, year, monthOfYear, dayOfMonth) -> {
            String x = "";
            String y = "";
            if (dayOfMonth < 10) {
                x = "0";
            }
            if (monthOfYear < 10) {
                y = "0";
            }

            txtAreaInspectionDetailInitDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
            txtAreaInspectionDetailInitDate.setError(null);
            txtAreaInspectionDetailInitDate.setTextColor(Color.BLACK);
        };


        to_dateListener = (view1, year, monthOfYear, dayOfMonth) -> {
            String x = "";
            String y = "";
            if (dayOfMonth < 10) {
                x = "0";
            }
            if (monthOfYear < 10) {
                y = "0";
            }

            txtAreaInspectionDetailExecutionDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
            txtAreaInspectionDetailExecutionDate.setError(null);
            txtAreaInspectionDetailExecutionDate.setTextColor(Color.BLACK);
        };
        scroll = view.findViewById(R.id.inspectionactionScroll);
        if (area_inspection_action.getId() != null) {
            txtareaincidentdetailplace.setText(area_inspection_action.getPlace());
            txtareaincidentdetailexactplace.setText(area_inspection_action.getExactPlace());
            spAreaIncidentDetailType.setSelection(stringSpinnerAdapterType.getPosition(area_inspection_action.getActionType()));
            spAreaIncidentDetailInversion.setSelection(stringSpinnerAdapterInversion.getPosition(area_inspection_action.getInversion()));
            txtAreaInspectionDetailCause.setText(area_inspection_action.getCauseDescription());
            txtAreaInspectionDetailCorrective.setText(area_inspection_action.getCorrectiveAction());
            spAreaIncidentDetailPrority.setSelection(stringSpinnerAdapterPriority.getPosition(area_inspection_action.getPriority()));
            spAreaIncidentDetailActionEject.setSelection(userSpinnerAdapter.getPOsition(userDao.loadByRowId(area_inspection_action.getActionExecuteEesponsible())));
            txtAreaInspectionDetailInitDate.setText(area_inspection_action.getInitDate());
            txtAreaInspectionDetailExecutionDate.setText(area_inspection_action.getLimitDate());
            spAreaInspectionDetailInforPerson.setSelection(userSpinnerAdapter.getPOsition(userDao.loadByRowId(area_inspection_action.getBreachInform())));
            boolean[] selectedUsers = new boolean[userSpinnerAdapter.getCount()];
            if (!area_inspection_action.getCloseEmployeesNotify().equals("")) {
                String[] listemails = area_inspection_action.getCloseEmployeesNotify().split(",");
                closeUsersInfo = area_inspection_action.getCloseEmployeesNotify();
                for (int i = 0; i < listemails.length; i++) {
                    Long idu = Long.parseLong(listemails[i]);
                    User user = userDao.load(idu);
                    int pos = userSpinnerAdapter.getPOsition(user);
                    selectedUsers[pos] = true;
                }
                spAreaInspectionDetailClosePerson.setSelected(selectedUsers);
            }
            txtAreaInspectionComment.setText(area_inspection_action.getComment());
        }

        return view;
    }


    public void save() {
        area_inspection_action.setPlace(txtareaincidentdetailplace.getText().toString());
        area_inspection_action.setExactPlace(txtareaincidentdetailexactplace.getText().toString());
        area_inspection_action.setActionType(spAreaIncidentDetailType.getSelectedItem().toString());
        area_inspection_action.setInversion(spAreaIncidentDetailInversion.getSelectedItem().toString());
        area_inspection_action.setCauseDescription(txtAreaInspectionDetailCause.getText().toString());
        area_inspection_action.setCorrectiveAction(txtAreaInspectionDetailCorrective.getText().toString());
        area_inspection_action.setPriority(spAreaIncidentDetailPrority.getSelectedItem().toString());
        area_inspection_action.setActionExecuteEesponsible(((User) spAreaIncidentDetailActionEject.getSelectedItem()).getId());
        area_inspection_action.setInitDate(txtAreaInspectionDetailInitDate.getText().toString());
        area_inspection_action.setLimitDate(txtAreaInspectionDetailExecutionDate.getText().toString());
        area_inspection_action.setBreachInform(((User) spAreaInspectionDetailInforPerson.getSelectedItem()).getId());
        area_inspection_action.setCloseEmployeesNotify(closeUsersInfo);
        area_inspection_action.setComment(txtAreaInspectionComment.getText().toString());
    }

    public boolean validate() {
        if (txtareaincidentdetailplace.getText().toString().isEmpty()) {
            txtareaincidentdetailplace.requestFocus();
            txtareaincidentdetailplace.setError("Este campo es obligatorio");
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblPlace.getTop());
            });
            return false;
        }
        if (txtareaincidentdetailexactplace.getText().toString().isEmpty()) {
            txtareaincidentdetailexactplace.requestFocus();
            txtareaincidentdetailexactplace.setError("Este campo es obligatorio");
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExactPlace.getTop());
            });
            return false;
        }
        if (txtAreaInspectionDetailCause.getText().toString().isEmpty()) {
            txtAreaInspectionDetailCause.requestFocus();
            txtAreaInspectionDetailCause.setError("Este campo es obligatorio");
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblCauseDesciption.getTop());
            });
            return false;
        }
        if (txtAreaInspectionDetailCorrective.getText().toString().isEmpty()) {
            txtAreaInspectionDetailCorrective.requestFocus();
            txtAreaInspectionDetailCorrective.setError("Este campo es obligatorio");
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblCorrectiveAction.getTop());
            });
            return false;
        }
        if (txtAreaInspectionDetailInitDate.getText().toString().isEmpty() || txtAreaInspectionDetailInitDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtAreaInspectionDetailInitDate.requestFocus();
            txtAreaInspectionDetailInitDate.setError(getString(R.string.error_field_required));
            txtAreaInspectionDetailInitDate.setTextColor(Color.RED);
            txtAreaInspectionDetailInitDate.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblStartDate.getTop());
            });
            return false;
        }
        if (txtAreaInspectionDetailExecutionDate.getText().toString().isEmpty() || txtAreaInspectionDetailExecutionDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtAreaInspectionDetailExecutionDate.requestFocus();
            txtAreaInspectionDetailExecutionDate.setError(getString(R.string.error_field_required));
            txtAreaInspectionDetailExecutionDate.setTextColor(Color.RED);
            txtAreaInspectionDetailExecutionDate.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExecDate.getTop());
            });
            return false;
        }
        if (closeUsersInfo.isEmpty()) {
            spAreaInspectionDetailClosePerson.requestFocus();
            spAreaInspectionDetailClosePerson.setError("Este campo es olbigatorio");
            spAreaInspectionDetailClosePerson.setHint("Este campo es olbigatorio");
            spAreaInspectionDetailClosePerson.setHintTextColor(Color.RED);
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblCloseInform.getTop());
            });
            return false;
        }
        return true;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }


    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            save();
            areaInspectionActionSingleton.setArea_inspection_action(area_inspection_action);
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        areaInspectionActionSingleton.emptyAreainspectionAction();
        startActivity(new Intent(getContext(), AreaInspectionsListActivity.class));
    }
}
