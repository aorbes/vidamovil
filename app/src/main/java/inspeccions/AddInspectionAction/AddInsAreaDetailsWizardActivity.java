package inspeccions.AddInspectionAction;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.DaoSession;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import di.areaInspectionActionSingleton;

public class AddInsAreaDetailsWizardActivity extends AppCompatActivity implements StepperLayout.StepperListener{

    private StepperLayout mStepperLayout;
    private AreaInspectionActionDao actionDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ins_area_details_steps);


        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        actionDao = daoSession.getAreaInspectionActionDao();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            areaInspectionActionSingleton.getInstance();
            areaInspectionActionSingleton.setArea_inspection_action(actionDao.load(b.getLong("InspectionActionId")));
        }
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayoutAddAreaDetail);
        mStepperLayout.setAdapter(new AddAreaInspectionDetailAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
        Intent intent = new Intent(getApplicationContext(), AreaInspectionsDetailsListActivity.class);
        startActivity(intent);
    }
}
