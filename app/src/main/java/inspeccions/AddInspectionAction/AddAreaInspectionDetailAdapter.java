package inspeccions.AddInspectionAction;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.movil.vida.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by Alexander on 11/2/2018.
 */

public class AddAreaInspectionDetailAdapter extends AbstractFragmentStepAdapter {

    public AddAreaInspectionDetailAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
                return new AddAreaInspectionDetailFragment();
            case  1:
                return new AddAreaInspectionDetailPhotoFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle(null);
        switch (position) {
            case 0:
                builder.setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Cancelar")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setTitle("Datos Obligatorios");
                break;
            case 1:
                builder.setEndButtonLabel("Guardar Detalle")
                        .setBackButtonLabel("Anterior")
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Agregar Fotos");
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}


