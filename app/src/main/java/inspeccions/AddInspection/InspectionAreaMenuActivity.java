package inspeccions.AddInspection;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AreaInsActionImagesDao;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.AreaInspectionsDao;
import com.movil.vida.dao.DaoSession;

public class InspectionAreaMenuActivity extends AppCompatActivity {
    Toolbar toolbar = null;
    LinearLayout layout_add_inspection_area;
    LinearLayout layout_list_inspection_area;
    String url;
    private Button btnUploadAreaIns;
    private TextView lblUploaded, lblNotUploaded;
    private AreaInspectionsDao areaInspectionsDao;
    private AreaInspectionActionDao actionDao;
    private AreaInsActionImagesDao actionImagesDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_area_menu);
        toolbar = (Toolbar) findViewById(R.id.inspections_area_menu_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inspecciones de Área");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init DAOS
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        areaInspectionsDao = daoSession.getAreaInspectionsDao();
        actionDao = daoSession.getAreaInspectionActionDao();
        actionImagesDao = daoSession.getAreaInsActionImagesDao();

        url = Session.getBaseUrl(InspectionAreaMenuActivity.this) + "/uploadareainspections";
        layout_add_inspection_area = (LinearLayout) findViewById(R.id.layout_add_inspection_area);
        layout_add_inspection_area.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), AreaInspectionsListActivity.class));
        });
        layout_list_inspection_area = (LinearLayout) findViewById(R.id.layout_list_inspection_area);
        layout_list_inspection_area.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), AreaInspectionsListActivity.class));
        });

        btnUploadAreaIns = (Button) findViewById(R.id.btnUploadAreaIns);
        btnUploadAreaIns.setOnClickListener(view -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(InspectionAreaMenuActivity.this);
            adb.setTitle("Iniciar la subida de registros a vida?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("OK", (dialogInterface, i) -> {

            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();
        });

        lblUploaded = (TextView) findViewById(R.id.txtUploaded);
        lblNotUploaded = (TextView) findViewById(R.id.txtNotUploaded);
        updateUploadeds();
    }

    private void updateUploadeds() {
/*        Long uploaded = areaInspectionsDao.queryBuilder().where(AreaInspectionsDao.Properties.SyncState.eq(1)).count();
        lblUploaded.setText(uploaded + "");
        Long notUploaded = areaInspectionsDao.queryBuilder().where(AreaInspectionsDao.Properties.SyncState.eq(0)).count();
        lblNotUploaded.setText(notUploaded + "");*/
    }

   /* private class HttpRequestTask extends AsyncTask<Void, String, Void> {
        public int total;
        public int uploaded = 0;
        public boolean conn = false;
        ProgressDialog progDailog;
        Context context;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();

        public HttpRequestTask(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total = areaInspectionsDao.queryBuilder().where(AreaInspectionsDao.Properties.SyncState.eq(0)).list().size();
            progDailog = new ProgressDialog(InspectionAreaMenuActivity.this);
            progDailog.setTitle("Subiendo Inspeciones de área");
            progDailog.setMessage("Espere mientras se suben los registros..." + uploaded + "/" + total);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (AreaInspections areaInspections : areaInspectionsDao.queryBuilder().where(AreaInspectionsDao.Properties.SyncState.eq(0)).list()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("creatorEmployee", areaInspections.getCreatorEmployee());
                    jsonObject.put("place", areaInspections.getPlace());
                    jsonObject.put("reporterEmployee", Session.getUserName(InspectionAreaMenuActivity.this));
                    jsonObject.put("revisionDate", areaInspections.getRevisionDate());
                    jsonObject.put("description", areaInspections.getDescription());
                    jsonObject.put("inspectedArea", areaInspections.getInspectedArea());
                    jsonObject.put("securityConsultor", areaInspections.getSecurityConsultor());
                    jsonObject.put("liderArea", areaInspections.getAreaLider());
                    jsonObject.put("supervisors", areaInspections.getSupervisors());
                    jsonObject.put("participants", areaInspections.getParticipants());

                    //set actions
                    JSONArray jsonArray = new JSONArray();
                    for (AreaInspectionAction action : actionDao.queryBuilder().where(AreaInspectionActionDao.Properties.InspectionId.eq(areaInspections.getId())).list()) {
                        JSONObject jsonAction = new JSONObject();
                        jsonAction.put("place", action.getPlace());
                        jsonAction.put("exactPlace", action.getExactPlace());
                        jsonAction.put("actionType", action.getActionType());
                        jsonAction.put("inversion", action.getInversion());
                        jsonAction.put("causeDescription", action.getCauseDescription());
                        jsonAction.put("correctiveAction", action.getCorrectiveAction());
                        jsonAction.put("priority", action.getPriority());
                        jsonAction.put("actionExecuteEesponsible", action.getActionExecuteEesponsible());
                        jsonAction.put("initDate", action.getInitDate());
                        jsonAction.put("limitDate", action.getLimitDate());
                        jsonAction.put("breachInform", action.getBreachInform());
                        jsonAction.put("closeEmployeesNotify", action.getCloseEmployeesNotify());
                        jsonAction.put("comment", action.getComment());
                        JSONArray arrayimages = new JSONArray();
                        for (AreaInsActionImages actionImages : actionImagesDao.queryBuilder().where(AreaInsActionImagesDao.Properties.AreaInspectionActionId.eq(action.getId())).list()) {
                            arrayimages.put(Base64.encodeToString(actionImages.getImage(),
                                    Base64.DEFAULT));
                        }
                        jsonAction.put("images", arrayimages);
                        jsonArray.put(jsonAction);
                    }
                    jsonObject.put("actions", jsonArray);
                    HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
                    Log.i("Tyring to upload", entity.getBody());
                    ResponseEntity<String> response =
                            restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

                    // ResponseEntity<ExperientialPlan> responseEntity = restTemplate.postForEntity(url,  experientialPlan, ExperientialPlan.class);
                    conn = true;
                    Log.i("Uploaded", areaInspections.getId() + "--------------------------------------");
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Espere mientras se suben los registros..." + uploaded++ + "/" + total);
                        areaInspections.setSyncState(1);
                        areaInspectionsDao.update(areaInspections);
                    } else {
                        conn = false;
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                updateUploadeds();
                android.support.v7.app.AlertDialog.Builder goLogin = new android.support.v7.app.AlertDialog.Builder(context);
                goLogin.setMessage("Se subió los registros a vida correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                android.support.v7.app.AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }*/
}
