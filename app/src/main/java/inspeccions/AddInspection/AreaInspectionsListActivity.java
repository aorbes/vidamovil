package inspeccions.AddInspection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.AreaInspections;
import com.movil.vida.dao.AreaInspectionsDao;
import com.movil.vida.dao.DaoSession;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

public class AreaInspectionsListActivity extends AppCompatActivity {
    private InspectionsAdapter inspectionsAdapter;
    private List<AreaInspections> list;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private String url;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AreaInspectionsDao areaInspectionsDao;
    private AreaInspectionActionDao actionDao;
    private Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_inspections_list);
        url = Session.getBaseUrl(this);
        toolbar = (Toolbar) findViewById(R.id.area_inspection_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Inspecciones en ejecución");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init Dao
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        areaInspectionsDao = daoSession.getAreaInspectionsDao();
        actionDao = daoSession.getAreaInspectionActionDao();

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(view -> {
            swipeRefreshLayout.setRefreshing(true);
            new DownLoadInspections(this).execute();
        });
        // areaInspectionsDao.deleteAll();
        list = areaInspectionsDao.loadAll();
        inspectionsAdapter = new InspectionsAdapter(list, AreaInspectionsListActivity.this, this, daoSession);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            new DownLoadInspections(this).execute();
        });
        recyclerView = (RecyclerView) findViewById(R.id.listareainspections);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(inspectionsAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(llm);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                swipeRefreshLayout.setRefreshing(true);
                new DownLoadInspections(this).execute();
                break;
            case R.id.menu_delete_actions:
                AlertDialog.Builder adb = new AlertDialog.Builder(AreaInspectionsListActivity.this);
                adb.setTitle("¿Eliminar Acciones Subidas a Vida?");
                adb.setIcon(R.drawable.delete_icon);
                adb.setMessage("Esta acción libera memoria");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    actionDao.queryBuilder().where(AreaInspectionActionDao.Properties.SyncState.eq(1)).buildDelete().executeDeleteWithoutDetachingEntities();
                    list.clear();
                    list.addAll(areaInspectionsDao.loadAll());
                    inspectionsAdapter.notifyDataSetChanged();
                    Toast.makeText(this, "Acciones subidas a vida eliminadas", Toast.LENGTH_LONG).show();
                });
                adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                adb.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public class DownLoadInspections extends AsyncTask<Void, Void, Void> {
        public boolean conn = false;
        ProgressDialog progDailog;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        private Context context;

        public DownLoadInspections(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(context);
            progDailog.setTitle("Descargando Inspecciones");
            progDailog.setMessage("Actualizando Lista...");
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                ResponseEntity<AreaInspections[]> responseEntity = restTemplate.getForEntity(url + "/getactiveinpsections", AreaInspections[].class);
                AreaInspections[] areaInspections = responseEntity.getBody();
                if (areaInspections.length > 0) {
                    areaInspectionsDao.deleteAll();
                    for (int y = 0; y < areaInspections.length; y++) {
                        areaInspectionsDao.insert(areaInspections[y]);
                    }
                }

                conn = true;
            } catch (Exception e) {
                e.printStackTrace();
                conn = false;
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            swipeRefreshLayout.setRefreshing(false);
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                list.clear();
                list.addAll(areaInspectionsDao.loadAll());
                inspectionsAdapter.notifyDataSetChanged();
            }
        }
    }

}
