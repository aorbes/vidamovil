package inspeccions.AddInspection;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.movil.vida.R;
import com.movil.vida.dao.AreaInspections;
import com.movil.vida.dao.AreaInspectionsDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.stringSpinnerAdapter;
import adapters.userSpinnerAdapter;

public class AddAreaInspectionActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    Toolbar toolbar;
    private Spinner spAreaInspectionAssesor, spAreaInspectionLeader, spInspectionAreaPlace, spAreaInspectionRegister;
    private MultiSpinner spAreaInspectionSupervisor, spAreaInspectionParticipants;
    UserDao userDao;
    private MultiSpinner.MultiSpinnerListener onSelectedListenerSupervisors, onSelectedListenerParticipants;
    private EditText txtareaInspectionDate, txtAreaInspectionDescription, txtAreaInspection;
    private TextView lblAreaInsAreaInspected, lblAreaInsSupervisors, lblArearInsExecDate;
    private ScrollView scrollViewAddAreaInspection;
    private AreaInspectionsDao area_inspectionsDao;
    private AreaInspections area_inspections = new AreaInspections();
    private String supervisors = "";
    private String participants = "";
    private Button btnSaveAreaInspection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_area_inspection);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
       //area_inspectionsDao.deleteAll();
        toolbar = (Toolbar) findViewById(R.id.add_inspections_area_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nueva Inspección de Área");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spInspectionAreaPlace = (Spinner) findViewById(R.id.spInspectionAreaPlace);
        List<String> iPlace = new ArrayList<>();
        iPlace.add("Unacem");
        iPlace.add("Cantyvol");
        stringSpinnerAdapter stringSpinnerAdapterIplace = new stringSpinnerAdapter(getApplicationContext(), iPlace);
        spInspectionAreaPlace.setAdapter(stringSpinnerAdapterIplace);
        txtAreaInspection = (EditText) findViewById(R.id.txtAreaInspection);
        txtAreaInspection.setOnClickListener(view -> {
            txtAreaInspection.clearFocus();
        });
        lblAreaInsAreaInspected = (TextView) findViewById(R.id.lblAreaInsAreaInspected);

        spAreaInspectionAssesor = (Spinner) findViewById(R.id.spAreaInspectionAssesor);
        List<User> listUser = userDao.loadAll();
        Collections.sort(listUser, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        userSpinnerAdapter adapterUsers = new userSpinnerAdapter(getApplicationContext(), listUser);
        spAreaInspectionAssesor.setAdapter(adapterUsers);
/*        spAreaInspectionAssesor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    if (!parent.getSelectedItem().equals("Seleccione el Asesor")) {
                        User user = (User) parent.getSelectedItem();
                        //ivl.setCompanion_id(user.getId());
                        area_inspections.setSecurity_consultor(user.getId());
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        spAreaInspectionLeader = (Spinner) findViewById(R.id.spAreaInspectionLeader);
        spAreaInspectionLeader.setAdapter(adapterUsers);
        /*spAreaInspectionLeader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    if (!parent.getSelectedItem().equals("Seleccione el Líder")) {
                        User user = (User) parent.getSelectedItem();
                        area_inspections.setArea_lider(user.getId());
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        spAreaInspectionSupervisor = (MultiSpinner) findViewById(R.id.spAreaInspectionSupervisor);
        final ArrayAdapter<User> adapterMultiUser = new ArrayAdapter<User>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, listUser);
        onSelectedListenerSupervisors = new MultiSpinner.MultiSpinnerListener() {
            public void onItemsSelected(boolean[] selected) {
                supervisors = "";
                for (int i = 0; i < selected.length; i++) {
                    if (selected[i]) {
                        supervisors += adapterMultiUser.getItem(i).getId() + ",";
                    }
                    spAreaInspectionSupervisor.setError(null);
                }
            }

        };

        spAreaInspectionSupervisor.setAdapter(adapterMultiUser, false, onSelectedListenerSupervisors);

        spAreaInspectionParticipants = (MultiSpinner) findViewById(R.id.spAreaInspectionParticipants);
        onSelectedListenerParticipants = new MultiSpinner.MultiSpinnerListener() {
            public void onItemsSelected(boolean[] selected) {
                participants = "";
                for (int i = 0; i < selected.length; i++) {
                    if (selected[i]) {
                        participants += adapterMultiUser.getItem(i).getId() + ",";
                    }
                }
            }

        };
        spAreaInspectionParticipants.setAdapter(adapterMultiUser, false, onSelectedListenerParticipants);

        spAreaInspectionRegister = (Spinner) findViewById(R.id.spAreaInspectionRegister);
        spAreaInspectionRegister.setAdapter(adapterUsers);
        /*spAreaInspectionRegister.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (count >= 1) {
                    if (!adapterView.getSelectedItem().equals("Seleccione quién lo realiza")) {
                        User user = (User) adapterView.getSelectedItem();
                        area_inspections.setReporter_employee(user.getId());
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        txtareaInspectionDate = (EditText) findViewById(R.id.txtareaInspectionDate);


        txtareaInspectionDate.setOnClickListener(view -> {
                Calendar now = Calendar.getInstance();
                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddAreaInspectionActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setTitle("Selecciona la Fecha");
                datePickerDialog.show(getFragmentManager(), "");
        });

        txtAreaInspection = (EditText) findViewById(R.id.txtAreaInspection);
        lblArearInsExecDate = (TextView) findViewById(R.id.lblArearInsExecDate);
        txtAreaInspectionDescription = (EditText) findViewById(R.id.txtAreaInspectionDescription);
        txtAreaInspectionDescription.setOnClickListener(view -> txtAreaInspectionDescription.clearFocus());
        lblAreaInsSupervisors = (TextView) findViewById(R.id.lblAreaInsSupervisors);
        btnSaveAreaInspection = (Button) findViewById(R.id.btnSaveAreaInspection);
        btnSaveAreaInspection.setOnClickListener(view -> {
                addAreaInspection();
        });
        scrollViewAddAreaInspection = (ScrollView) findViewById(R.id.scrollViewAddAreaInspection);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            area_inspections = area_inspectionsDao.load(b.getLong("InspectionId"));
            spInspectionAreaPlace.setSelection(stringSpinnerAdapterIplace.getPosition(area_inspections.getPlace()));
            txtAreaInspection.setText(area_inspections.getInspectedArea());
            spAreaInspectionAssesor.setSelection(adapterUsers.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(area_inspections.getSecurityConsultor())).unique()));
            spAreaInspectionLeader.setSelection(adapterUsers.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(area_inspections.getAreaLider())).unique()));
            boolean[] selectedItemsSup = new boolean[adapterMultiUser.getCount()];
            if (!area_inspections.getSupervisors().equals("")) {
                String[] listemails = area_inspections.getSupervisors().split(",");
                supervisors = area_inspections.getSupervisors();
                for (int i = 0; i < listemails.length; i++) {
                    Long idu = Long.parseLong(listemails[i]);
                    User user = userDao.load(idu);
                    int pos = adapterMultiUser.getPosition(user);
                    selectedItemsSup[pos] = true;
                }
                spAreaInspectionSupervisor.setSelected(selectedItemsSup);
            }
            boolean[] selectedItemsPart = new boolean[adapterMultiUser.getCount()];
            if (!area_inspections.getParticipants().equals("")) {
                String[] listemails = area_inspections.getParticipants().split(",");
                participants = area_inspections.getParticipants();
                for (int i = 0; i < listemails.length; i++) {
                    Long idu = Long.parseLong(listemails[i]);
                    User user = userDao.load(idu);
                    int pos = adapterMultiUser.getPosition(user);
                    selectedItemsPart[pos] = true;
                }
                spAreaInspectionParticipants.setSelected(selectedItemsPart);
            }
            spAreaInspectionRegister.setSelection(adapterUsers.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(area_inspections.getCreatorEmployee())).unique()));
            txtareaInspectionDate.setText(area_inspections.getRevisionDate());
            txtAreaInspectionDescription.setText(area_inspections.getDescription());

        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtareaInspectionDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
        txtareaInspectionDate.setError(null);
        txtareaInspectionDate.setTextColor(Color.BLACK);
    }

    public boolean validate() {
        if (txtAreaInspection.getText().toString().isEmpty()) {
            txtAreaInspection.requestFocus();
            txtAreaInspection.setError("Este campo es obligatorio");
            scrollViewAddAreaInspection.post(() -> {
                scrollViewAddAreaInspection.smoothScrollTo(0, lblAreaInsAreaInspected.getTop());
            });
            return false;
        }
        if (supervisors.isEmpty()) {
            spAreaInspectionSupervisor.setError("Este campo es olbigatorio");
            spAreaInspectionSupervisor.setHint("Este campo es olbigatorio");
            spAreaInspectionSupervisor.setHintTextColor(Color.RED);
            scrollViewAddAreaInspection.post(() -> {
                scrollViewAddAreaInspection.smoothScrollTo(0, lblAreaInsSupervisors.getTop());
            });
            return false;
        }
        if (txtareaInspectionDate.getText().toString().isEmpty()) {
            txtareaInspectionDate.setError(getString(R.string.error_field_required));
            txtareaInspectionDate.setTextColor(Color.RED);
            txtareaInspectionDate.setText(getString(R.string.error_field_required));
            scrollViewAddAreaInspection.post(() -> {
                scrollViewAddAreaInspection.smoothScrollTo(0, lblArearInsExecDate.getTop());
            });
            return false;
        }

        return true;
    }

    public void addAreaInspection() {
        if (validate()) {
            area_inspections.setPlace(spInspectionAreaPlace.getSelectedItem().toString());
            area_inspections.setInspectedArea(txtAreaInspection.getText().toString());
            area_inspections.setSecurityConsultor(((User) spAreaInspectionAssesor.getSelectedItem()).getId());
            area_inspections.setAreaLider(((User) spAreaInspectionLeader.getSelectedItem()).getId());
            area_inspections.setSupervisors(supervisors);
            area_inspections.setParticipants(participants);
            area_inspections.setCreatorEmployee(((User) spAreaInspectionRegister.getSelectedItem()).getId());
            area_inspections.setRevisionDate(txtareaInspectionDate.getText().toString());
            area_inspections.setDescription(txtAreaInspectionDescription.getText().toString());
            if (area_inspections.getId() != null) {
                area_inspectionsDao.update(area_inspections);
                Toast.makeText(getApplicationContext(), "Se ha  actulizado la inspección correctamente ", Toast.LENGTH_LONG).show();
            } else {
                // area_inspections.setSyncState(0);
                Long id = area_inspectionsDao.insert(area_inspections);
                Toast.makeText(getApplicationContext(), "Se ha  registrado la inspección  correctamente " + id, Toast.LENGTH_LONG).show();
            }
            Intent intent = new Intent(getApplicationContext(), AreaInspectionsListActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
