package inspeccions.AddInspection;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AreaInsActionImages;
import com.movil.vida.dao.AreaInsActionImagesDao;
import com.movil.vida.dao.AreaInspectionAction;
import com.movil.vida.dao.AreaInspectionActionDao;
import com.movil.vida.dao.AreaInspections;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

import di.areaInspectionActionSingleton;
import inspeccions.AddInspectionAction.AreaInspectionsDetailsListActivity;

public class InspectionsAdapter extends RecyclerView.Adapter<InspectionsAdapter.MyViewHolder> {
    private List<AreaInspections> items;
    private Context context;
    private Activity activity;
    private UserDao userDao;
    private DaoSession daoSession;
    private AreaInspectionActionDao areaInspectionActionDao;
    private AreaInsActionImagesDao actionImagesDao;
    private String url;

    public InspectionsAdapter(List<AreaInspections> items, Context context, Activity activity, DaoSession daoSession) {
        this.context = context;
        this.items = items;
        this.activity = activity;
        this.daoSession = daoSession;
        this.userDao = daoSession.getUserDao();
        this.areaInspectionActionDao = daoSession.getAreaInspectionActionDao();
        this.actionImagesDao = daoSession.getAreaInsActionImagesDao();
        url = Session.getBaseUrl(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_inspection_item, parent, false);
        return new InspectionsAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.areainspectiondate.setText(items.get(position).getRevisionDate());
        holder.areainspectionplace.setText(items.get(position).getInspectedArea());
        User user = userDao.load((items.get(position).getCreatorEmployee()));
        holder.areainspectionleader.setText(user.getName());
        holder.lblActions.setText(areaInspectionActionDao.queryBuilder().where(AreaInspectionActionDao.Properties.InspectionId.eq(items.get(position).getId())).list().size() + "");
        holder.txtParticipants.setText(items.get(position).getParticipants());
        String participants[] = items.get(position).getParticipants().split(",");
        String colab = "";
        for (int i = 0; i < participants.length; i++) {
            int number = i + 1;
            colab += "<b>" + number + "</b>" + " " + participants[i] + "<br>";
        }
        String finalColab = colab;
        holder.btnParticipants.setOnClickListener(view -> {
            AlertDialog.Builder goPart = new AlertDialog.Builder(context);
            goPart.setTitle("Participantes de la inspección");
            goPart.setMessage(Html.fromHtml(finalColab));
            goPart.setCancelable(false);
            goPart.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                dialogInterface.cancel();
            });
            AlertDialog alertLogin = goPart.create();
            alertLogin.show();
        });
        holder.btnAddAction.setOnClickListener(v -> {
            areaInspectionActionSingleton.setInspectionId(items.get(position).getId());
            activity.startActivity(new Intent(context, AreaInspectionsDetailsListActivity.class));
            //  activity.finish();
        });


        holder.btnUploadActions.setOnClickListener(view -> {
            if (areaInspectionActionDao.queryBuilder().where(AreaInspectionActionDao.Properties.SyncState.eq(0)).list().size() > 0) {
                new HttpRequestTask(context).execute();
            } else {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("No hay acciones sin subir a vida!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView areainspectiondate;
        TextView areainspectionplace;
        TextView areainspectionleader;
        TextView lblActions;
        TextView txtParticipants;
        private Button btnAddAction;
        private Button btnUploadActions;
        private ImageView imgSyncState;
        private Button btnParticipants;
        public MyViewHolder(View v) {
            super(v);
            mCardView = v.findViewById(R.id.card_view_inspection);
            areainspectiondate = v.findViewById(R.id.areainspectiondate);
            areainspectionplace = v.findViewById(R.id.areainspectionplace);
            areainspectionleader = v.findViewById(R.id.areainspectionsleader);
            lblActions = v.findViewById(R.id.lblActions);
            btnAddAction = v.findViewById(R.id.btnAddAction);
            txtParticipants = v.findViewById(R.id.txtParticipants);
            btnUploadActions = v.findViewById(R.id.btnUploadActions);
            imgSyncState = v.findViewById(R.id.imgSyncState);
            btnParticipants = v.findViewById(R.id.btnParticipants);
        }

    }

    private class HttpRequestTask extends AsyncTask<Void, String, Void> {
        public int total;
        public int uploaded = 0;
        public boolean conn = false;
        ProgressDialog progDailog;
        Context context;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();

        public HttpRequestTask(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total = areaInspectionActionDao.queryBuilder().where(AreaInspectionActionDao.Properties.SyncState.eq(0)).list().size();
            progDailog = new ProgressDialog(context);
            progDailog.setTitle("Subiendo Acciones");
            progDailog.setMessage("Espere mientras se suben las acciones..." + uploaded + "/" + total);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                JSONObject jsonAction = new JSONObject();
                for (AreaInspectionAction action : areaInspectionActionDao.queryBuilder().where(AreaInspectionActionDao.Properties.SyncState.eq(0)).list()) {
                    jsonAction.put("inspectionId", action.getInspectionId());
                    jsonAction.put("place", action.getPlace());
                    jsonAction.put("exactPlace", action.getExactPlace());
                    jsonAction.put("actionType", action.getActionType());
                    jsonAction.put("inversion", action.getInversion());
                    jsonAction.put("causeDescription", action.getCauseDescription());
                    jsonAction.put("correctiveAction", action.getCorrectiveAction());
                    jsonAction.put("priority", action.getPriority());
                    jsonAction.put("actionExecuteEesponsible", action.getActionExecuteEesponsible());
                    jsonAction.put("initDate", action.getInitDate());
                    jsonAction.put("limitDate", action.getLimitDate());
                    jsonAction.put("breachInform", action.getBreachInform());
                    jsonAction.put("closeEmployeesNotify", action.getCloseEmployeesNotify());
                    jsonAction.put("comment", action.getComment());
                    jsonAction.put("employeeCreator", Session.getUserName(context));
                    JSONArray arrayimages = new JSONArray();
                    for (AreaInsActionImages actionImages : actionImagesDao.queryBuilder().where(AreaInsActionImagesDao.Properties.AreaInspectionActionId.eq(action.getId())).list()) {
                        arrayimages.put(Base64.encodeToString(actionImages.getImage(),
                                Base64.DEFAULT));
                    }
                    jsonAction.put("images", arrayimages);
                    HttpEntity<String> entity = new HttpEntity<String>(jsonAction.toString(), headers);
                    Log.i("Tyring to upload", entity.getBody());
                    ResponseEntity<String> response =
                            restTemplate.exchange(url + "/uploadareainspectionactions", HttpMethod.POST, entity, String.class);
                    conn = true;
                    Log.i("Uploaded", action.getId() + "--------------------------------------");
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Espere mientras se suben los registros..." + uploaded++ + "/" + total);
                        action.setSyncState(1);
                        areaInspectionActionDao.update(action);
                    } else {
                        conn = false;
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Se subió las acciones a vida correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }
}
