package ivl.newivl;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.movil.vida.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by Alexander on 31/1/2018.
 */

public class IvlStepperAdapter extends AbstractFragmentStepAdapter {

    public IvlStepperAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
                return new AddIvlFragment();
            case  1:
                return new IvlProfileFragment();
            case 2:
                return new FragmentAddIvImages();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle("Wizard IVL");
        switch (position) {
            case 0:
                builder.setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Cancelar")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setTitle("Datos Obligatorios");
                break;
            case 1:
                builder.setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Anterior")
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Perfiles");
                break;
            case 2:
                builder
                        .setEndButtonLabel("Guardar IVL")
                        .setBackButtonLabel("Anterior")
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Agregar Fotos");

                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}
