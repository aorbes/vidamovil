package ivl.newivl;


import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.movil.vida.BuildConfig;
import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Ivl;
import com.movil.vida.dao.IvlCatalog;
import com.movil.vida.dao.IvlCatalogDao;
import com.movil.vida.dao.IvlCatalogReg;
import com.movil.vida.dao.IvlCatalogRegDao;
import com.movil.vida.dao.IvlDao;
import com.movil.vida.dao.IvlImages;
import com.movil.vida.dao.IvlImagesDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import adapters.SlideAdapter;
import di.IvlSingleton;
import di.leaderItemSingleton;
import ivl.ListIVLActivity;
import me.relex.circleindicator.CircleIndicator;
import utils.FileUtil;
import utils.ImageUtils;

import static android.app.Activity.RESULT_OK;


public class FragmentAddIvImages extends Fragment implements Step, BlockingStep {

    private IvlDao ivlDao;
    private static final int IMAGE_GALLERY_REQUEST = 25;
    private static final int REQUEST_TAKE_PHOTO = 20;
    private List<Integer> leaderitems = leaderItemSingleton.getInstance();
    IvlImagesDao ivl_imagesDao;
    ArrayList<byte[]> list_ivl_images = new ArrayList<>();
    CircleIndicator indicator;
    private IvlCatalogDao ivl_catalogDao;
    private IvlCatalogRegDao ivl_catalog_regDao;
    private Ivl ivl = IvlSingleton.getInstance();
    private ConstraintLayout lytivlimageslider;
    private ImageButton btnAddImage;
    private ImageButton btnSlideDeleteImage;
    private ViewPager viewPager;
    private SlideAdapter myadapter;


    private AlertDialog dialogAddPhoto;
    private Button btnGallery;
    private Button btnCammera;
    private AlertDialog dialogDelete;
    private Button deleteImage;
    private Button cancelDelete;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      View view=  inflater.inflate(R.layout.fragment_add_ivl_images, container, false);


        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        ivlDao = daoSession.getIvlDao();
        ivl_imagesDao = daoSession.getIvlImagesDao();
        ivl_catalogDao = daoSession.getIvlCatalogDao();
        ivl_catalog_regDao = daoSession.getIvlCatalogRegDao();

        lytivlimageslider = view.findViewById(R.id.lytivlimageslider);
        btnAddImage = lytivlimageslider.findViewById(R.id.btnSlideAddImage);
        btnSlideDeleteImage = lytivlimageslider.findViewById(R.id.btnSlideDeleteImage);
        btnAddImage.setOnClickListener(v1 -> {
            dialogAddPhoto.show();
        });

        btnSlideDeleteImage.setOnClickListener(v2 -> {
            dialogDelete.show();
        });

        //Add DeleteImage
        AlertDialog.Builder mBuilderDelImage = new AlertDialog.Builder(getContext());
        View mViewDelImage = inflater.inflate(R.layout.delete_layout, null);
        mBuilderDelImage.setView(mViewDelImage);
        dialogDelete = mBuilderDelImage.create();
        deleteImage = mViewDelImage.findViewById(R.id.btnDelete);
        deleteImage.setOnClickListener(view1 -> {
            try {
                myadapter.removeView();
                indicator.setViewPager(viewPager);
                hideDeleteButton(myadapter.getCount());
                dialogDelete.cancel();
            } catch (Exception e) {
                dialogDelete.cancel();
                Toast.makeText(getContext(), "Error al eliminar imagen", Toast.LENGTH_LONG).show();
            }
        });
        cancelDelete = mViewDelImage.findViewById(R.id.btnCanlcel);
        cancelDelete.setOnClickListener(view1 -> {
            dialogDelete.cancel();
        });
        // init dialogsAddImage
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.cammera_or_gallery, null);
        mBuilder.setView(mView);
        dialogAddPhoto = mBuilder.create();
        btnGallery = mView.findViewById(R.id.btnGallery);
        btnCammera = mView.findViewById(R.id.btnCammera);
        btnGallery.setOnClickListener(view2 -> {
            onImageGalleryClicked();
        });
        btnCammera.setOnClickListener(view2 -> {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            5);
                }
            } else {
                dispatchTakePictureIntent();
            }
        });

        if (ivl.getId() != null) {
            list_ivl_images = new ArrayList<>();
            for (IvlImages image : ivl_imagesDao.queryBuilder().where(IvlImagesDao.Properties.IvlId.eq(ivl.getId())).list()) {
                list_ivl_images.add(image.getImage());
            }
        }

        viewPager = view.findViewById(R.id.viewPagerImages);
        indicator = view.findViewById(R.id.indicator);
        myadapter = new SlideAdapter(getContext(), list_ivl_images, viewPager);
        viewPager.setAdapter(myadapter);
        indicator.setViewPager(viewPager);
        myadapter.registerDataSetObserver(indicator.getDataSetObserver());

        hideDeleteButton(myadapter.getCount());
      return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }
        }
    }

    public void hideDeleteButton(int size) {
        if (size > 0) {
            btnSlideDeleteImage.setVisibility(View.VISIBLE);
        } else {
            btnSlideDeleteImage.setVisibility(View.GONE);
        }
    }

    public void onImageGalleryClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);
        photoPickerIntent.setDataAndType(data, "image/*");
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(getContext());
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(getContext(), "Ocurrió un error al abrir la cámara", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialogAddPhoto.cancel();
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                Bitmap myBitmap = ImageUtils.compressImage(new File(ImageUtils.currentImagePath), getContext());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                list_ivl_images.add(byteArray);
                myadapter.notifyDataSetChanged();
                viewPager.setCurrentItem(myadapter.getCount());
                hideDeleteButton(myadapter.getCount());
            }
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                try {
                    Bitmap image = ImageUtils.compressImage(FileUtil.from(getContext(), data.getData()), getContext());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    list_ivl_images.add(byteArray);
                    myadapter.notifyDataSetChanged();
                    viewPager.setCurrentItem(myadapter.getCount());
                    hideDeleteButton(myadapter.getCount());

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "No se puede abrir la imagen", Toast.LENGTH_LONG).show();
                }

            }
        }
    }
    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        Long ivlId;
        if (ivl.getId() != null) {
            ivlDao.update(ivl);
            ivlId = ivl.getId();
            ivl_imagesDao.queryBuilder().where(IvlImagesDao.Properties.IvlId.eq(ivl.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
            ivl_catalog_regDao.queryBuilder().where(IvlCatalogRegDao.Properties.IvlId.eq(ivl.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
            Toast.makeText(getContext(), "Se ha actualizado  el IVL ", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), "Se ha agregado un nuevo IVL ", Toast.LENGTH_SHORT).show();
            ivl.setSyncState(0);
            ivlId = ivlDao.insert(ivl);
        }

        for (byte[] photo : list_ivl_images) {
            IvlImages ivlImages = new IvlImages();
            ivlImages.setImage(photo);
            ivlImages.setIvlId(ivlId);
            this.ivl_imagesDao.insert(ivlImages);
        }
        for (Integer integer : leaderitems) {
            IvlCatalogReg ivl_catalog_reg = new IvlCatalogReg();
            IvlCatalog ivl_catalog = ivl_catalogDao.load(integer.longValue());
            ivl_catalog_reg.setIvlcatId(ivl_catalog.getId());
            ivl_catalog_reg.setIvlId(ivlId);
            ivl_catalog_regDao.insert(ivl_catalog_reg);

        }
        IvlSingleton.emptyIvl();
        leaderItemSingleton.emptyLeaderItems();
        startActivity(new Intent(getContext(), ListIVLActivity.class));
        getActivity().finish();

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
callback.goToPrevStep();
    }
}
