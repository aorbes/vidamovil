package ivl.newivl;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.Area;
import com.movil.vida.dao.AreaDao;
import com.movil.vida.dao.Contractor;
import com.movil.vida.dao.ContractorDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Ivl;
import com.movil.vida.dao.IvlResposible;
import com.movil.vida.dao.IvlResposibleDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.areaSpinnerAdapter;
import adapters.contractorSpinnerAdapter;
import adapters.responsibleSpinnerAdapter;
import adapters.stringSpinnerAdapter;
import adapters.typesSpinnerAdapter;
import adapters.userSpinnerAdapter;
import beans.TypesBean;
import di.IvlSingleton;
import di.leaderItemSingleton;
import ivl.ListIVLActivity;
import ivl.MenuIVLActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddIvlFragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, Step, BlockingStep {

    UserDao userDao;
    ContractorDao contractorDao;
    AreaDao areaDao;
    Ivl ivl = IvlSingleton.getInstance();
    private Spinner spIVLPlace;
    private EditText txtDate;
    private TextView lblivldate;
    private EditText txtStartDate;
    private TextView lblivlStartDate;
    private EditText txtEndDate;
    private TextView lblivlEndDate;
    private Spinner spIVLResponsible;
    private EditText txtPersonNumber;
    private TextView lblivlNumberPeople;
    private Spinner spWorkPLace;
    private Spinner spInteractiontype;
    private EditText txtComents;
    private TextView lbltxtIVLComments;
    private Spinner spContractor;
    private Spinner spActivityType;
    private TextView lblspIVLActivity;
    private Spinner spLeader;
    private Spinner spAccompaiment;
    private ScrollView scrollViewAddIvl;
    private IvlResposibleDao ivlResposibleDao;

    public AddIvlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_ivl, container, false);
//local varibales
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        ivlResposibleDao = daoSession.getIvlResposibleDao();
        userDao = daoSession.getUserDao();
        contractorDao = daoSession.getContractorDao();
        areaDao = daoSession.getAreaDao();


        getActivity().setTitle("Registrar  IVL");
        spIVLPlace = view.findViewById(R.id.spIVLPlace);
        List<String> iPlace = new ArrayList<>();
        iPlace.add("Unacem");
        iPlace.add("Cantyvol");
        stringSpinnerAdapter stringSpinnerAdapterIplace = new stringSpinnerAdapter(getContext(), iPlace);
        spIVLPlace.setAdapter(stringSpinnerAdapterIplace);

        lblivldate = view.findViewById(R.id.lblivldate);
        txtDate = view.findViewById(R.id.ivldate);


        txtDate.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddIvlFragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setTitle("Selecciona la Fecha");
            datePickerDialog.show(getActivity().getFragmentManager(), "");
        });
        lblivlStartDate = view.findViewById(R.id.lblivlStartDate);
        txtStartDate = view.findViewById(R.id.ivlStratDate);
        txtStartDate.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog1 = TimePickerDialog.newInstance(AddIvlFragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog1.setTitle("Hora de inicio");

            timePickerDialog1.show(getActivity().getFragmentManager(), "");
        });
        lblivlEndDate = view.findViewById(R.id.lblivlEndDate);
        txtEndDate = view.findViewById(R.id.ivlEndDate);
        txtEndDate.setOnClickListener(v -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog2 = TimePickerDialog.newInstance(AddIvlFragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog2.setTitle("Hora de Finalización");
            timePickerDialog2.show(getActivity().getFragmentManager(), "");
        });
        spIVLResponsible = view.findViewById(R.id.spIVLResponsible);
        List<IvlResposible> listIvlResponsible = ivlResposibleDao.loadAll();
        Collections.sort(listIvlResponsible, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        responsibleSpinnerAdapter adapterResponsible = new responsibleSpinnerAdapter(getContext(), listIvlResponsible);
        spIVLResponsible.setAdapter(adapterResponsible);
        spIVLResponsible.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !parent.getSelectedItem().equals("Seleccione el Responsable")) {
                    IvlResposible responsible = (IvlResposible) parent.getSelectedItem();
                    ivl.setResponsibleId(responsible.getId());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing slelect
            }
        });

        lblivlNumberPeople = view.findViewById(R.id.lblivlNumberPeople);
        txtPersonNumber = view.findViewById(R.id.ivlNumberPeople);
        txtPersonNumber.setOnClickListener(v -> {
            if (txtPersonNumber.getText().toString().trim().equals("")) {
                txtPersonNumber.setError("Este campo es obligatorio");
            } else {
                txtPersonNumber.setError(null);
            }
        });
        spAccompaiment = view.findViewById(R.id.spIVLCompainion);
        List<User> listUser = userDao.loadAll();

        Collections.sort(listUser, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        userSpinnerAdapter adapterAccompanion = new userSpinnerAdapter(getContext(), listUser);
        spAccompaiment.setAdapter(adapterAccompanion);
        spAccompaiment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !parent.getSelectedItem().equals("Seleccione el Acompañante")) {
                    User user = (User) parent.getSelectedItem();
                    ivl.setCompanionId(user.getId());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing slelect
            }
        });


        spInteractiontype = view.findViewById(R.id.spIVLInteractionType);
        List<TypesBean> listinteractionType = new ArrayList<>();
        listinteractionType.add(new TypesBean("C", "IVL clásico con trabajos en curso"));
        listinteractionType.add(new TypesBean("D", "Desayuno de liderazgo"));
        listinteractionType.add(new TypesBean("S", "Coaching de seguridad individual (con personal operativo)"));
        listinteractionType.add(new TypesBean("G", "Coaching de seguridad grupal (con personal operativo)"));
        listinteractionType.add(new TypesBean("E", "Estudio de Caso de Seguridad"));
        listinteractionType.add(new TypesBean("A", "Reunion del plan de seguridad del Area"));
        listinteractionType.add(new TypesBean("P", "Reunión del Estandar con competentes"));
        listinteractionType.add(new TypesBean("R", "Reunión con supervisores de seguridad de contratistas en paradas / proyectos"));
        listinteractionType.add(new TypesBean("O", "Otro"));
        typesSpinnerAdapter adapterInteractionyTye = new typesSpinnerAdapter(getContext(), listinteractionType);
        spInteractiontype.setAdapter(adapterInteractionyTye);
        spInteractiontype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !parent.getSelectedItem().equals("Seleccione el Tipo de Interacción")) {
                    TypesBean interactionTypeBean = (TypesBean) parent.getSelectedItem();
                    ivl.setInteractionType(interactionTypeBean.getType());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing slelect
            }
        });


        spWorkPLace = view.findViewById(R.id.spIVLWorkPlace);

        areaSpinnerAdapter adapterArea = new areaSpinnerAdapter(getContext(), areaDao.loadAll());
        spWorkPLace.setAdapter(adapterArea);


        spLeader = view.findViewById(R.id.spIVLSupervisor);
        spLeader.setAdapter(adapterAccompanion);
   /*     spLeader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !parent.getSelectedItem().equals("Seleccione el Lider/Supervisor")) {
                    User user = (User) parent.getSelectedItem();
                    ivl.setLiderId(user.getId());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing selected
            }
        });*/
        spContractor = view.findViewById(R.id.spIVLContractor);
        final List<Contractor> contractorList = new ArrayList<>();
        contractorList.add(new Contractor(9999L, "Seleccione el Contratista", "99999999"));
        contractorList.addAll(contractorDao.loadAll());
        //spContractor.post(() -> spContractor.setSelection(contractorList.size() - 1));
        contractorSpinnerAdapter adapterContractor = new contractorSpinnerAdapter(getContext(), contractorList);
        spContractor.setAdapter(adapterContractor);
        spContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !((Contractor) parent.getSelectedItem()).getName().equals("Seleccione el Contratista")) {
                    Contractor contractor = (Contractor) parent.getSelectedItem();
                    ivl.setContractorId(contractor.getId());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing selected
            }
        });

        lblspIVLActivity = view.findViewById(R.id.lblspIVLActivity);
        spActivityType = view.findViewById(R.id.spIVLActivity);
        List<TypesBean> listactivityType = new ArrayList<>();
        listactivityType.add(new TypesBean("", "Seleccione el Tipo de Actividad"));
        listactivityType.add(new TypesBean("Y", "Proyectos"));
        listactivityType.add(new TypesBean("R", "Trabajo rutinario"));
        listactivityType.add(new TypesBean("P", "Trabajo programado"));
        listactivityType.add(new TypesBean("M", "Trabajo mayor"));
        listactivityType.add(new TypesBean("E", "Trabajo emergente"));
        listactivityType.add(new TypesBean("T", "Otros"));
        typesSpinnerAdapter adapterActivityTye = new typesSpinnerAdapter(getContext(), listactivityType);
        spActivityType.setAdapter(adapterActivityTye);

        spActivityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1 && !((TypesBean) parent.getSelectedItem()).getDescription().equals("Seleccione el Tipo de Actividad")) {
                    TypesBean typesBean = (TypesBean) parent.getSelectedItem();
                    ivl.setActivityType(typesBean.getType());
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
//When nothing slelect
            }
        });
        lbltxtIVLComments = view.findViewById(R.id.lbltxtIVLComments);
        txtComents = view.findViewById(R.id.txtIVLComments);

        scrollViewAddIvl = view.findViewById(R.id.scrollViewAddIvl);
        if (ivl.getId() != null) {
            spIVLPlace.setSelection(stringSpinnerAdapterIplace.getPosition(ivl.getMainPlace()));
            txtDate.setText(ivl.getCreationDate());
            txtStartDate.setText(ivl.getStartHour());
            txtEndDate.setText(ivl.getEndHour());
            spIVLResponsible.setSelection(adapterResponsible.getPosition(ivlResposibleDao.queryBuilder().where(IvlResposibleDao.Properties.Id.eq(ivl.getResponsibleId())).unique()));
            txtPersonNumber.setText(ivl.getNumPersons() + "");
            if (ivl.getCompanionId() != null) {
                spAccompaiment.setSelection(adapterAccompanion.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(ivl.getCompanionId())).unique()));
            }
            spInteractiontype.setSelection(adapterInteractionyTye.getPosition(ivl.getInteractionType()));
            spWorkPLace.setSelection(adapterArea.getPosition(areaDao.queryBuilder().where(AreaDao.Properties.Name.eq(ivl.getAreaName())).unique()));
            txtComents.setText(ivl.getComments());
            if (ivl.getLiderId() != null) {
                Log.i("Id del lider", ivl.getLiderId() + "");
                spLeader.setSelection(adapterAccompanion.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(ivl.getLiderId())).unique()));
            }
            if (ivl.getContractorId() != null) {
                Log.i("Id del contratista", ivl.getContractorId() + "");
                spContractor.setSelection(adapterContractor.getPosition(contractorDao.queryBuilder().where(ContractorDao.Properties.Id.eq(ivl.getContractorId())).unique()));
            }
            if (!ivl.getActivityType().isEmpty()) {
                spActivityType.setSelection(adapterActivityTye.getPosition(ivl.getActivityType()));
            }

        }

        return view;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtDate.setTextColor(Color.BLACK);
        txtDate.setError(null);
        txtDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hour = "";
        String min = "";
        if (hourOfDay < 10) {
            hour = "0";
        }
        if (minute < 10) {
            min = "0";
        }
        if (view.getTitle().equals("Hora de inicio")) {
            txtStartDate.setTextColor(Color.BLACK);
            txtStartDate.setError(null);
            txtStartDate.setText(hour + hourOfDay + ":" + min + minute);
        } else {
            txtEndDate.setText(hour + hourOfDay + ":" + min + minute);
            txtEndDate.setError(null);
            txtEndDate.setTextColor(Color.BLACK);
        }

    }


    private void addIVL() {
        ivl.setMainPlace(spIVLPlace.getSelectedItem().toString());
        ivl.setCreationDate(txtDate.getText().toString());
        ivl.setStartHour(txtStartDate.getText().toString());
        ivl.setEndHour(txtEndDate.getText().toString());
        if (!txtPersonNumber.getText().toString().equals("")) {
            ivl.setNumPersons(Integer.parseInt(txtPersonNumber.getText().toString()));
        }
        ivl.setCompanionId(spAccompaiment.getSelectedItemId());
        ivl.setResponsibleId(((IvlResposible) spIVLResponsible.getSelectedItem()).getId());
        ivl.setInteractionType(((TypesBean) spInteractiontype.getSelectedItem()).getType());
        ivl.setAreaName(((Area) (spWorkPLace.getSelectedItem())).getName());
        if (txtComents.getText() != null) {
            ivl.setComments(txtComents.getText().toString());
        }
        //if(!((User)(spLeader.getSelectedItem())).getName().contains("")){
        ivl.setLiderId(((User) (spLeader.getSelectedItem())).getId());
        // }
        ivl.setActivityType(((TypesBean) spActivityType.getSelectedItem()).getType());

    }

    public boolean validate() {

        if (txtDate.getText().toString().isEmpty() || txtDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtDate.requestFocus();
            txtDate.setError(getString(R.string.error_field_required));
            txtDate.setTextColor(Color.RED);
            txtDate.setText(getString(R.string.error_field_required));
            scrollViewAddIvl.post(() -> {
                scrollViewAddIvl.smoothScrollTo(0, lblivldate.getTop());
            });
            return false;
        }

        if (txtStartDate.getText().toString().isEmpty() || txtStartDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtStartDate.requestFocus();
            txtStartDate.setError(getString(R.string.error_field_required));
            txtStartDate.setTextColor(Color.RED);
            txtStartDate.setText(getString(R.string.error_field_required));
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, lblivlStartDate.getTop()));
            return false;
        }

        if (txtEndDate.getText().toString().isEmpty() || txtEndDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtEndDate.requestFocus();
            txtEndDate.setError(getString(R.string.error_field_required));
            txtEndDate.setTextColor(Color.RED);
            txtEndDate.setText(getString(R.string.error_field_required));
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, lblivlEndDate.getTop()));
            return false;
        }

       /* if (spIVLResponsible.getSelectedItem().toString().equals(getString(R.string.error_message))) {
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, spIVLResponsible.getBottom() - 150));
            return false;
        }*/
        if (txtPersonNumber.getText().toString().equals("")) {
            txtPersonNumber.requestFocus();
            txtPersonNumber.setError(getString(R.string.error_message));
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, lblivlNumberPeople.getTop()));
            return false;
        }
        if (spInteractiontype.getSelectedItem().toString().equals("Seleccione el Tipo de Interacción")) {
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, spInteractiontype.getTop()));
            return false;
        }
        if (spWorkPLace.getSelectedItem().toString().equals("Seleccione el Sitio de Trabajo")) {
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, spWorkPLace.getTop()));
            return false;
        }
        if (txtComents.getText().toString().equals("")) {
            txtComents.requestFocus();
            txtComents.setError(getString(R.string.error_message));
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, lbltxtIVLComments.getTop()));
            return false;
        }
        if (((TypesBean) spActivityType.getSelectedItem()).getDescription().contains("Seleccione el Tipo de Actividad")) {
            spActivityType.requestFocus();
            TextView errorText = (TextView) spActivityType.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            scrollViewAddIvl.post(() -> scrollViewAddIvl.smoothScrollTo(0, lblspIVLActivity.getTop()));
            return false;
        }
        return true;


    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
//When selected
    }

    @Override
    public void onError(@NonNull VerificationError error) {
//On error
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            try {
                addIVL();
            } catch (Exception e) {
                Log.i("Error", "Ocurrión error en el registro");
            }
            IvlSingleton.setIvl(ivl);
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
//When click on complete
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        if (ivl.getId() != null) {
            startActivity(new Intent(getContext(), ListIVLActivity.class));
        } else {
            startActivity(new Intent(getContext(), MenuIVLActivity.class));
        }
        IvlSingleton.emptyIvl();
        leaderItemSingleton.emptyLeaderItems();
        getActivity().finish();
    }
}
