package ivl.newivl;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlCatalogReg;
import com.movil.vida.dao.IvlCatalogRegDao;
import com.movil.vida.dao.IvlDao;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

import di.IvlSingleton;
import di.leaderItemSingleton;

public class WizardAddIvl extends AppCompatActivity implements StepperLayout.StepperListener{
    private StepperLayout mStepperLayout;
    private IvlDao ivlDao;
    private IvlCatalogRegDao ivlCatalogRegDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_add_ivl);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        ivlCatalogRegDao = daoSession.getIvlCatalogRegDao();
        ivlDao = daoSession.getIvlDao();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            IvlSingleton.getInstance();
            IvlSingleton.setIvl(ivlDao.load(b.getLong("IvlId")));
            leaderItemSingleton.getInstance();
            List<IvlCatalogReg> listCatReg = ivlCatalogRegDao.queryBuilder().where(IvlCatalogRegDao.Properties.IvlId.eq(b.getLong("IvlId"))).list();
            List<Integer> listleaderItems = new ArrayList<>();
            for (IvlCatalogReg item : listCatReg) {
                listleaderItems.add(item.getIvlcatId().intValue());
            }
            leaderItemSingleton.setLeaderItems(listleaderItems);
            // leaderItemSingleton.setLeaderItems(listCatReg.stream().map(x->x.getIvlcatId().intValue()).collect(Collectors.toList()));
        }
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayoutIVL);
        mStepperLayout.setAdapter(new IvlStepperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);

    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
     /*   Intent intent = new Intent(this, MenuIVLActivity.class);
        startActivity(intent);
        Log.i("Click Cancel wizard", ".............");*/
    }
}
