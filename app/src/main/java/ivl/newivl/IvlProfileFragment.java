package ivl.newivl;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlCatalog;
import com.movil.vida.dao.IvlCatalogDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.List;

import di.leaderItemSingleton;


public class IvlProfileFragment extends Fragment implements Step, BlockingStep {
    private android.support.v7.widget.GridLayout gridLayoutLeader, gridLayoutEmpoderate;
    private IvlCatalogDao ivl_catalogDao;
    private List<Integer> leaderitems = leaderItemSingleton.getInstance();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_ivl_profile_, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        ivl_catalogDao = daoSession.getIvlCatalogDao();


        gridLayoutLeader = view.findViewById(R.id.tabLayoutleader);
        gridLayoutLeader.removeAllViews();

        List<IvlCatalog> listIvlCatalog = ivl_catalogDao.queryBuilder().where(IvlCatalogDao.Properties.Typeprof.eq("L")).list();
        int column = 2;
        int row = listIvlCatalog.size();
        gridLayoutLeader.setColumnCount(column);
        gridLayoutLeader.setRowCount(row + 1);
        for (int i = 0, c = 0, r = 0; i < row; i++, c++) {
            if (c == column) {
                c = 0;
                r++;
            }
            // int finalI = i;
            boolean exist = false;
            for (int k = 0; k < leaderitems.size(); k++) {
                if (listIvlCatalog.get(i).getId().intValue() == leaderitems.get(k).intValue()) {
                    exist = true;
                }
            }//boolean exist= leaderitems.stream().anyMatch(x->x.equals(listIvlCatalog.get(finalI).getId().intValue()));
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setId(listIvlCatalog.get(i).getId().intValue());
            if (exist) {
                checkBox.setChecked(true);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        leaderitems.add(buttonView.getId());
                    } else {
                        int index = leaderitems.indexOf(buttonView.getId());
                        leaderitems.remove(index);
                    }

                }
            });

            TextView textView = new TextView(getContext());
            textView.setText(listIvlCatalog.get(i).getName());

            GridLayout.Spec rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            GridLayout.Spec colspan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            if (r == 0 && c == 0) {
                colspan = GridLayout.spec(GridLayout.UNDEFINED, 2);
                rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 2);
            }
            GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(
                    rowSpan, colspan);
            gridLayoutLeader.addView(checkBox, gridParam);
            gridLayoutLeader.addView(textView, gridParam);

        }

        gridLayoutEmpoderate = view.findViewById(R.id.gridLayoutEmpoderate);
        gridLayoutEmpoderate.removeAllViews();
        List<IvlCatalog> listIvlCatalogE = ivl_catalogDao.queryBuilder().where(IvlCatalogDao.Properties.Typeprof.eq("E")).list();
        int columnE = 2;
        int rowE = listIvlCatalogE.size();
        gridLayoutEmpoderate.setColumnCount(columnE);
        gridLayoutEmpoderate.setRowCount(rowE + 1);
        for (int i = 0, c = 0, r = 0; i < rowE; i++, c++) {
            if (c == columnE) {
                c = 0;
                r++;
            }
            //int finalI = i;
            boolean exist = false;
            for (int k = 0; k < leaderitems.size(); k++) {
                if (listIvlCatalogE.get(i).getId().intValue() == leaderitems.get(k).intValue()) {
                    exist = true;
                }
            }
            // boolean exist= leaderitems.stream().anyMatch(x->x.equals(listIvlCatalogE.get(finalI).getId().intValue()));
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setId(listIvlCatalogE.get(i).getId().intValue());
            if (exist) {
                checkBox.setChecked(true);
            }
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        leaderitems.add(buttonView.getId());
                    } else {
                        int index = leaderitems.indexOf(buttonView.getId());
                        leaderitems.remove(index);
                    }

                }
            });

            TextView textView = new TextView(getContext());
            textView.setText(listIvlCatalogE.get(i).getName());

            GridLayout.Spec rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            GridLayout.Spec colspan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            if (r == 0 && c == 0) {
                colspan = GridLayout.spec(GridLayout.UNDEFINED, 2);
                rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 2);
            }
            GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(
                    rowSpan, colspan);
            gridLayoutEmpoderate.addView(checkBox, gridParam);
            gridLayoutEmpoderate.addView(textView, gridParam);
        }


        return view;
    }


    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
leaderItemSingleton.setLeaderItems(leaderitems);
callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
callback.goToPrevStep();
    }
}
