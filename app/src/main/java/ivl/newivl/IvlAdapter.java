package ivl.newivl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Ivl;
import com.movil.vida.dao.IvlAction;
import com.movil.vida.dao.IvlActionDao;
import com.movil.vida.dao.IvlCatalogRegDao;
import com.movil.vida.dao.IvlDao;
import com.movil.vida.dao.IvlImagesDao;
import com.movil.vida.dao.IvlResposibleDao;

import java.util.ArrayList;
import java.util.List;

import di.IvlSingleton;
import ivl.ivlaction.ListIvlActionActivity;

/**
 * Created by alex_.
 */
public class IvlAdapter extends RecyclerView.Adapter<IvlAdapter.MyViewHolder> {

    private ArrayList<Ivl> item;
    private Context mContext;
    private IvlDao ivlDao;
    private IvlResposibleDao ivlResposibleDao;
    private IvlActionDao ivl_actionDao;
    private Activity activity;
    private IvlImagesDao ivlImagesDao;
    private IvlCatalogRegDao ivlCatalogRegDao;

    public IvlAdapter(ArrayList<Ivl> item, Context context, Activity activity) {
        this.item = item;
        this.mContext = context;
        this.activity = activity;
        DaoSession daoSession = ((DaoApp) context).getDaoSession();
        ivlResposibleDao = daoSession.getIvlResposibleDao();
        ivl_actionDao = daoSession.getIvlActionDao();
        ivlImagesDao = daoSession.getIvlImagesDao();
        ivlDao = daoSession.getIvlDao();
        ivlCatalogRegDao = daoSession.getIvlCatalogRegDao();

    }

    @Override
    public IvlAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ivl_item, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        mContext = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.lblRegistrationDateIvl.setText(item.get(position).getCreationDate());
        String type = "";
        if (item.get(position).getInteractionType().equals("C")) {
            type = "IVL clásico con trabajos en curso";
        } else if (item.get(position).getInteractionType().equals("D")) {
            type = "Desayuno de liderazgo";
        } else if (item.get(position).getInteractionType().equals("S")) {
            type = "Coaching de seguridad individual (con personal operativo)";
        } else if (item.get(position).getInteractionType().equals("G")) {
            type = "Coaching de seguridad grupal (con personal operativo)";
        } else if (item.get(position).getInteractionType().equals("E")) {
            type = "Estudio de Caso de Seguridad";
        } else if (item.get(position).getInteractionType().equals("A")) {
            type = "Reunion del plan de seguridad del Area";
        } else if (item.get(position).getInteractionType().equals("P")) {
            type = "Reunión del Estandar con competentes";
        } else if (item.get(position).getInteractionType().equals("R")) {
            type = "Reunión con supervisores de seguridad de contratistas en paradas / proyectos";
        } else if (item.get(position).getInteractionType().equals("O")) {
            type = "Otro";
        }
        holder.lblIvlType.setText(type);
        holder.lblIvlResposible.setText(ivlResposibleDao.load(item.get(position).getResponsibleId()).getName());
        int actions = 0;
        actions = ivl_actionDao.queryBuilder().where(IvlActionDao.Properties.IvlId.eq(item.get(position).getId())).list().size();
        holder.lblIvlActions.setText("" + actions);
        holder.btnAddAction.setOnClickListener(z -> {
            IvlSingleton.setIvl_action_id(item.get(position).getId());
            activity.startActivity(new Intent(mContext.getApplicationContext(), ListIvlActionActivity.class));

        });

        if (item.get(position).getSyncState() == 0) {
            holder.sync_state_image.setImageResource(R.drawable.ic_sync_black_24dp);
        } else {
            holder.sync_state_image.setImageResource(R.drawable.ic_check_black_24dp);
        }

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        TextView lblRegistrationDateIvl, lblIvlType, lblIvlResposible, lblIvlActions;
        ImageView sync_state_image;
        private Button btnDeleteIvl, btnAddAction, btnEditIvl;

        public MyViewHolder(View v) {
            super(v);
            sync_state_image = v.findViewById(R.id.ivl_sysn_state);
            mCardView = v.findViewById(R.id.card_view_ivl);
            lblRegistrationDateIvl = v.findViewById(R.id.lblRegistrationDateIvl);
            lblIvlType = v.findViewById(R.id.lblIvlType);
            lblIvlResposible = v.findViewById(R.id.lblIvlResposible);
            lblIvlActions = v.findViewById(R.id.lblIvlActions);
            btnDeleteIvl = v.findViewById(R.id.btnDeleteIvl);
            btnDeleteIvl.setOnClickListener(view -> {
                AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
                adb.setTitle("Está seguro querer eliminar el IVL?");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    try {
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            Ivl ivl = item.get(pos);

                            List<IvlAction> list = ivl_actionDao.queryBuilder().where(IvlActionDao.Properties.IvlId.eq(ivl.getId())).list();
                            for (IvlAction item : list) {
                                ivl_actionDao.deleteByKey(item.getId());
                            }
                            ivlImagesDao.queryBuilder().where(IvlImagesDao.Properties.IvlId.eq(ivl.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                            ivlCatalogRegDao.queryBuilder().where(IvlCatalogRegDao.Properties.IvlId.eq(ivl.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                            ivlDao.delete(ivl);
                            item.remove(pos);
                            notifyItemRemoved(pos);
                            notifyDataSetChanged();
                            Toast.makeText(mContext, "Se ha eliminado el registro correctamente", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(mContext, "No se pudo eliminar el IVL", Toast.LENGTH_SHORT).show();
                    }

                });
                adb.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                adb.show();
            });
            btnAddAction = v.findViewById(R.id.btnActions);
            btnEditIvl = v.findViewById(R.id.btnEditIvl);
            btnEditIvl.setOnClickListener(z -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    Ivl ivl = item.get(getAdapterPosition());
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) z.getContext();
                    args.putLong("IvlId", ivl.getId());
                    Intent intent = new Intent(z.getContext(), WizardAddIvl.class);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });

        }

    }

}
