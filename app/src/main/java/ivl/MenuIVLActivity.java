package ivl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Ivl;
import com.movil.vida.dao.IvlAction;
import com.movil.vida.dao.IvlActionDao;
import com.movil.vida.dao.IvlCatalogReg;
import com.movil.vida.dao.IvlCatalogRegDao;
import com.movil.vida.dao.IvlDao;
import com.movil.vida.dao.IvlImages;
import com.movil.vida.dao.IvlImagesDao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

import ivl.newivl.WizardAddIvl;

public class MenuIVLActivity extends AppCompatActivity {
    Toolbar toolbar=null;
    LinearLayout layout_add_ivl;
    LinearLayout layout_list_ivl;
    String url;
    private TextView lblUploaded;
    private TextView lblNotUploaded;
    private Button btnUploadIvl;
    private IvlDao ivlDao;
    private IvlImagesDao ivlImagesDao;
    private IvlCatalogRegDao ivlCatalogRegDao;
    private IvlActionDao ivlActionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_ivl);
        toolbar = (Toolbar) findViewById(R.id.ivl_menu_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Menú IVL");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        ivlDao = daoSession.getIvlDao();
        ivlImagesDao = daoSession.getIvlImagesDao();
        ivlCatalogRegDao = daoSession.getIvlCatalogRegDao();
        ivlActionDao = daoSession.getIvlActionDao();


        url = Session.getBaseUrl(MenuIVLActivity.this) + "/uploadivl";
        layout_add_ivl=(LinearLayout) findViewById(R.id.layout_add_ivl);
        layout_add_ivl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(), WizardAddIvl.class);
                startActivity(intent);
            }
        });
        layout_list_ivl=(LinearLayout) findViewById(R.id.layout_list_ivl);
        layout_list_ivl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(), ListIVLActivity.class);
                startActivity(intent);
            }
        });
        lblUploaded = (TextView) findViewById(R.id.txtUploaded);
        lblNotUploaded = (TextView) findViewById(R.id.txtNotUploaded);
        updateUploadeds();
        btnUploadIvl = (Button) findViewById(R.id.btnUploadIvl);
        btnUploadIvl.setOnClickListener(view -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(MenuIVLActivity.this);
            adb.setTitle("Inciar la subida de IVL a vida?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                if (ivlDao.queryBuilder().where(IvlDao.Properties.SyncState.eq(0)).list().size() > 0) {
                    new HttpRequestTask(MenuIVLActivity.this).execute();
                } else if (ivlDao.loadAll().size() < 1) {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(MenuIVLActivity.this);
                    goLogin.setMessage("No hay registros de IVL!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("Aceptar", (dialogInterface1, i1) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                } else {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(MenuIVLActivity.this);
                    goLogin.setMessage("Todos los IVL se han subido al servidor!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("Aceptar", (dialogInterface2, i2) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                }
            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();

        });

    }

    private void updateUploadeds() {
        Long incidentUploaded = ivlDao.queryBuilder().where(IvlDao.Properties.SyncState.eq(1)).count();
        lblUploaded.setText(incidentUploaded + "");
        Long incidentNotUploaded = ivlDao.queryBuilder().where(IvlDao.Properties.SyncState.eq(0)).count();
        lblNotUploaded.setText(incidentNotUploaded + "");
    }


    private class HttpRequestTask extends AsyncTask<Void, String, Void> {

        public boolean conn = false;
        public int total;
        public int uploaded = 0;
        ProgressDialog progDailog;
        int responseCode = -1;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        private Context context;

        public HttpRequestTask(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total = ivlDao.queryBuilder().where(IvlDao.Properties.SyncState.eq(0)).list().size();
            progDailog = new ProgressDialog(MenuIVLActivity.this);
            progDailog.setTitle("Subiendo IVl");
            progDailog.setMessage("Espere mientras se suben los registros..." + uploaded + "/" + total);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONObject jsonObject;
            for (Ivl ivl : ivlDao.loadAll()) {
                jsonObject = new JSONObject();
                try {
                    jsonObject.put("activityType", ivl.getActivityType());
                    jsonObject.put("area", ivl.getAreaName());
                    jsonObject.put("comments", ivl.getComments());
                    jsonObject.put("companionId", ivl.getCompanionId());
                    jsonObject.put("registryDate", ivl.getCreationDate());
                    jsonObject.put("endHour", ivl.getEndHour());
                    jsonObject.put("interactionType", ivl.getInteractionType());
                    jsonObject.put("liderId", ivl.getLiderId());
                    jsonObject.put("mainPlace", ivl.getMainPlace());
                    jsonObject.put("startHour", ivl.getStartHour());
                    jsonObject.put("numPersons", ivl.getNumPersons());
                    jsonObject.put("responsibleId", ivl.getResponsibleId());
                    List<IvlImages> lstimages = ivlImagesDao.queryBuilder().where(IvlImagesDao.Properties.IvlId.eq(ivl.getId())).list();
                    if (lstimages.size() > 0) {
                        JSONArray images = new JSONArray();
                        for (IvlImages ivl_images : lstimages) {
                            String imgString = Base64.encodeToString(ivl_images.getImage(),
                                    Base64.DEFAULT);
                            images.put(imgString);
                        }
                        jsonObject.put("images", images);
                    }

                    List<IvlCatalogReg> lstivlcatalogreg = ivlCatalogRegDao.queryBuilder().where(IvlCatalogRegDao.Properties.IvlId.eq(ivl.getId())).list();
                    if (lstivlcatalogreg.size() > 0) {
                        JSONArray selecteds = new JSONArray();
                        JSONObject jsonObject1;
                        for (IvlCatalogReg ivl_catalog_reg : lstivlcatalogreg) {
                            jsonObject1 = new JSONObject();
                            jsonObject1.put("name", ivl_catalog_reg.getIvlCatalog().getName());
                            jsonObject1.put("typeprof", ivl_catalog_reg.getIvlCatalog().getTypeprof());
                            selecteds.put(jsonObject1);
                        }
                        jsonObject.put("ivlprofiles", selecteds);
                    }
                    List<IvlAction> listActions = ivlActionDao.queryBuilder().where(IvlActionDao.Properties.IvlId.eq(ivl.getId())).list();
                    if (listActions.size() > 0) {
                        JSONArray actions = new JSONArray();
                        for (IvlAction action : ivlActionDao.queryBuilder().where(IvlActionDao.Properties.IvlId.eq(ivl.getId())).list()) {
                            JSONObject jsaction = new JSONObject();
                            jsaction.put("inprovementSituation", action.getImproveSituation());
                            jsaction.put("actionImprove", action.getImproveComprimise());
                            jsaction.put("responsibleId", action.getResponsibleId());
                            jsaction.put("expirationDate", action.getExpirationDate());
                            actions.put(jsaction);
                        }
                        jsonObject.put("actions", actions);
                    }

                    HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
                    ResponseEntity<String> response =
                            restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Espere mientras se suben los registros..." + uploaded++ + "/" + total);
                        conn = true;
                        ivl.setSyncState(1);
                        ivlDao.update(ivl);
                        System.out.println("Se creó el IVL" + response.getBody());
                    } else {
                        conn = false;
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                updateUploadeds();
                android.support.v7.app.AlertDialog.Builder goLogin = new android.support.v7.app.AlertDialog.Builder(context);
                goLogin.setMessage("Se subió los registros a vida correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                android.support.v7.app.AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }
}

