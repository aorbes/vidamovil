package ivl.ivlaction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlAction;
import com.movil.vida.dao.IvlActionDao;
import com.movil.vida.dao.UserDao;

import java.util.List;

/**
 * Created by Alexander on 11/3/2018.
 */

public class IvlActionAdapter extends RecyclerView.Adapter<IvlActionAdapter.MyViewHolder> {

    private List<IvlAction> list;
    private Context mContext;
    private UserDao userDao;
    private IvlActionDao ivlActionDao;

    public IvlActionAdapter(List<IvlAction> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
        DaoSession daoSession = ((DaoApp) mContext).getDaoSession();
        userDao = daoSession.getUserDao();
        ivlActionDao = daoSession.getIvlActionDao();
    }

    @Override
    public IvlActionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ivl_action_item_card, parent, false);
        IvlActionAdapter.MyViewHolder vh = new IvlActionAdapter.MyViewHolder(v);
        mContext = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtIvlActionId.setText(list.get(position).getId().toString());
        holder.lblImproveSituation.setText(list.get(position).getImproveSituation());
        holder.lblResponsible.setText(userDao.load(list.get(position).getResponsibleId()).getName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView lblImproveSituation, lblResponsible, txtIvlActionId;
        private Button btnDelete, btnEdit;

        public MyViewHolder(View itemView) {
            super(itemView);
            lblImproveSituation = itemView.findViewById(R.id.lblImproveSituation);
            lblResponsible = itemView.findViewById(R.id.lblResponsible);
            txtIvlActionId = itemView.findViewById(R.id.txtIvlActionId);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnDelete.setOnClickListener(z -> {
                AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
                adb.setTitle("Eliminar la acción?");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("Eliminar", (z2, z3) -> {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        IvlAction i = list.get(pos);
                        ivlActionDao.delete(i);
                        list.remove(pos);
                        notifyItemRemoved(pos);
                    }
                });
                adb.setNegativeButton("Cancelar", (v1, v2) -> {
                });
                adb.show();
            });
            btnEdit.setOnClickListener(z2 -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    IvlAction ivlAction = list.get(getAdapterPosition());
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) z2.getContext();
                    args.putLong("IvlActionId", ivlAction.getId());
                    Intent intent = new Intent(z2.getContext(), AddIvlActionActivity.class);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });
        }
    }
}
