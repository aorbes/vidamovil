package ivl.ivlaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlAction;
import com.movil.vida.dao.IvlActionDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.userSpinnerAdapter;
import di.IvlSingleton;

public class AddIvlActionActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    IvlAction ivl_action = new IvlAction();
    private Toolbar toolbar;
    private EditText txtIvlActionImprove, txtIvlActionImproveAction, txtIvlActionEndDate;
    private Spinner spIvlActionResponsible;
    private Button btnSaveIvlAction;
    private UserDao userDao;
    private IvlActionDao ivl_actionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ivl_action);
        toolbar = (Toolbar) findViewById(R.id.add_ivl_actions_listbar_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Nueva Acción");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        userDao = daoSession.getUserDao();
        ivl_actionDao = daoSession.getIvlActionDao();

        txtIvlActionImprove = (EditText) findViewById(R.id.txtIvlActionImprove);
        txtIvlActionImproveAction = (EditText) findViewById(R.id.txtIvlActionImproveAction);
        spIvlActionResponsible = (Spinner) findViewById(R.id.spIvlActionResponsible);
        List<User> users = new ArrayList<>();
        User defaultUser = new User();
        defaultUser.setName(getResources().getString(R.string.select_option));
        users.add(defaultUser);
        users.addAll(userDao.loadAll());
        Collections.sort(users.subList(1, users.size()), (user1, user2) -> {
            return user1.getName().compareTo(user2.getName());
        });
        userSpinnerAdapter userSpinnerAdapter = new userSpinnerAdapter(getApplicationContext(), users);
        spIvlActionResponsible.setAdapter(userSpinnerAdapter);
        txtIvlActionEndDate = (EditText) findViewById(R.id.txtIvlActionEndDate);
        txtIvlActionEndDate.setOnClickListener(z -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddIvlActionActivity.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setTitle("Selecciona la Fecha Fin");
            datePickerDialog.show(getFragmentManager(), "");
        });
        btnSaveIvlAction = (Button) findViewById(R.id.btnSaveIvlAction);
        btnSaveIvlAction.setOnClickListener(z -> {
            save();
        });
        Bundle b = getIntent().getExtras();
        if (b != null) {
            ivl_action = ivl_actionDao.load(b.getLong("IvlActionId"));
            txtIvlActionImprove.setText(ivl_action.getImproveSituation());
            txtIvlActionImproveAction.setText(ivl_action.getImproveComprimise());
            spIvlActionResponsible.setSelection(userSpinnerAdapter.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Id.eq(ivl_action.getResponsibleId())).unique()));
            txtIvlActionEndDate.setText(ivl_action.getExpirationDate());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_edit_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                save();
                return true;
            case R.id.edit:
                save();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (ivl_action.getId() != null) {
            menu.findItem(R.id.save).setVisible(false);
        } else {
            menu.findItem(R.id.edit).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtIvlActionEndDate.setTextColor(Color.BLACK);
        txtIvlActionEndDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
        txtIvlActionEndDate.setError(null);
    }

    public boolean validate() {
        if (txtIvlActionImprove.getText().toString().isEmpty()) {
            txtIvlActionImprove.setError("Este campo es obligatorio");
            txtIvlActionImprove.requestFocus();
            return false;
        }
        if (txtIvlActionImproveAction.getText().toString().isEmpty()) {
            txtIvlActionImproveAction.setError("Este campo es obligatorio");
            txtIvlActionImproveAction.requestFocus();
            return false;
        }

        if (((User) spIvlActionResponsible.getSelectedItem()).getName().equals(getResources().getString(R.string.select_option))) {
            spIvlActionResponsible.requestFocus();
            TextView errorText = (TextView) spIvlActionResponsible.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            return false;

        }
        if (txtIvlActionEndDate.getText().toString().isEmpty() || txtIvlActionEndDate.getText().toString().equals(getString(R.string.error_field_required))) {
            txtIvlActionEndDate.requestFocus();
            txtIvlActionEndDate.setError(getString(R.string.error_field_required));
            txtIvlActionEndDate.setTextColor(Color.RED);
            txtIvlActionEndDate.setText(getString(R.string.error_field_required));
            return false;
        }
        return true;
    }

    public void save() {
        if (validate()) {
            ivl_action.setImproveSituation(txtIvlActionImprove.getText().toString());
            ivl_action.setImproveComprimise(txtIvlActionImproveAction.getText().toString());
            ivl_action.setResponsibleId(((User) spIvlActionResponsible.getSelectedItem()).getId());
            ivl_action.setExpirationDate(txtIvlActionEndDate.getText().toString());
            ivl_action.setIvlId(IvlSingleton.ivl_action_id);
            if (ivl_action.getId() != null) {
                ivl_actionDao.update(ivl_action);
                Toast.makeText(this, "Se ha editado la acción ", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Se ha agregado una nueva acción", Toast.LENGTH_LONG).show();
                ivl_actionDao.insert(ivl_action);
            }
            startActivity(new Intent(getApplicationContext(), ListIvlActionActivity.class));
            finish();
        }
    }
}
