package ivl.ivlaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlAction;
import com.movil.vida.dao.IvlActionDao;

import java.util.ArrayList;

import di.IvlSingleton;

public class ListIvlActionActivity extends AppCompatActivity {

    private IvlActionDao ivl_actionDao;
    private FloatingActionButton btnAddIvlAction;
    private RecyclerView list_actions_ivls;
    private Toolbar toolbar;
    private IvlActionAdapter ivlActionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity_ivl_action);
        toolbar = (Toolbar) findViewById(R.id.ivl_actions_listbar_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Acciones de IVL");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        ivl_actionDao = daoSession.getIvlActionDao();

        btnAddIvlAction = (FloatingActionButton) findViewById(R.id.btnAddIvlAction);
        btnAddIvlAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AddIvlActionActivity.class));
            }
        });
        list_actions_ivls = (RecyclerView) findViewById(R.id.list_actions_ivls);
        ArrayList<IvlAction> items = new ArrayList<>();
        items.addAll(ivl_actionDao.queryBuilder().where(IvlActionDao.Properties.IvlId.eq(IvlSingleton.ivl_action_id)).list());
        list_actions_ivls = (RecyclerView) findViewById(R.id.list_actions_ivls);
        list_actions_ivls.setHasFixedSize(true);
        ivlActionAdapter = new IvlActionAdapter(items, getApplicationContext());
        list_actions_ivls.setAdapter(ivlActionAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        list_actions_ivls.setLayoutManager(llm);
    }
}
