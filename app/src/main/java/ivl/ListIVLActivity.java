package ivl;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Ivl;
import com.movil.vida.dao.IvlDao;

import java.util.ArrayList;

import di.IvlSingleton;
import ivl.newivl.IvlAdapter;

public class ListIVLActivity extends AppCompatActivity {
    IvlDao ivlDao;

    IvlAdapter adapter;
    Toolbar toolbar=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_ivl);
//ivlDao.deleteAll();
        toolbar = (Toolbar) findViewById(R.id.ivllistbar_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("IVL Registrados");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        ivlDao = daoSession.getIvlDao();


        ArrayList<Ivl> arrayList= new ArrayList<>();
        arrayList.addAll(ivlDao.loadAll());

        RecyclerView rv = (RecyclerView) findViewById(R.id.listivls);
        rv.setHasFixedSize(true);
        adapter = new IvlAdapter(arrayList, getApplicationContext(), this);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);
        IvlSingleton.emptyIvlActionId();




    }
}
