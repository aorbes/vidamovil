package objetives;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.movil.vida.R;

import java.util.List;

import beans.ItemBean;

public class ObjetivesAdapter extends RecyclerView.Adapter<ObjetivesAdapter.MyViewHolder> {

    private List<ItemBean> items;
    private Context context;

    public ObjetivesAdapter(List<ItemBean> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.objetive_item, parent, false);
        ObjetivesAdapter.MyViewHolder vh = new ObjetivesAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.objetiveName.setText(items.get(position).getName());
        holder.btnDetails.setOnClickListener(view -> {
            Intent intent = new Intent(context, ObjetiveDetails.class);
            Bundle args = new Bundle();
            args.putLong("objetiveId", items.get(position).getId());
            intent.putExtras(args);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView objetiveName;
        private Button btnDetails;

        public MyViewHolder(View itemView) {
            super(itemView);
            objetiveName = itemView.findViewById(R.id.onjetiveName);
            btnDetails = itemView.findViewById(R.id.btnSeeObject);
        }
    }
}
