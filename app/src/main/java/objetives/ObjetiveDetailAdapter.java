package objetives;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.movil.vida.R;

import java.util.List;

import beans.ItemBean;

public class ObjetiveDetailAdapter extends RecyclerView.Adapter<ObjetiveDetailAdapter.MyViewHolder> {

    private List<ItemBean> items;
    private Context context;

    public ObjetiveDetailAdapter(List<ItemBean> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ObjetiveDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.objetive_details_item, parent, false);
        ObjetiveDetailAdapter.MyViewHolder vh = new ObjetiveDetailAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ObjetiveDetailAdapter.MyViewHolder holder, int position) {
        holder.objetives.setText(items.get(position).getId().toString());
        holder.month.setText(items.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView month;
        TextView objetives;

        public MyViewHolder(View itemView) {
            super(itemView);
            month = itemView.findViewById(R.id.txtMonth);
            objetives = itemView.findViewById(R.id.txtObjetives);
        }
    }
}
