package objetives;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.movil.vida.R;
import com.movil.vida.Session;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import beans.ItemBean;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyObjetives extends AppCompatActivity {

    private List<ItemBean> items = new ArrayList<>();
    private RecyclerView list;
    private Toolbar toolbar;
    private ObjetivesAdapter adapter;
    private SwipeRefreshLayout swipemyobjetives;

    public MyObjetives() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_my_objetives);
        toolbar = (Toolbar) findViewById(R.id.myobjetivestoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Catálogo de Objetivos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipemyobjetives = (SwipeRefreshLayout) findViewById(R.id.swipemyobjetives);
        swipemyobjetives.setOnRefreshListener(() -> {
            new DownloadData(getApplicationContext()).execute();
            swipemyobjetives.setRefreshing(true);
        });
        RecyclerView rv = (RecyclerView) findViewById(R.id.myobjetiveslist);
        rv.setHasFixedSize(true);
        adapter = new ObjetivesAdapter(items, this);
        rv.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        try {
            new DownloadData(getApplicationContext()).execute();
        } catch (Exception e) {
        }
    }

    public class DownloadData extends AsyncTask<Void, Void, Void> {
        boolean conn = false;
        private String url;

        public DownloadData(Context mContext) {
            url = Session.getBaseUrl(mContext);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipemyobjetives.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
                ResponseEntity<ItemBean[]> objetives = restTemplate.getForEntity(url + "/getobjetivescatalog", ItemBean[].class);
                ItemBean[] objs = objetives.getBody();
                items.clear();
                if (objs.length > 0) {
                    for (int i = 0; i < objs.length; i++) {
                        ItemBean itemBean = new ItemBean();
                        itemBean.setId(objs[i].getId());
                        itemBean.setName(objs[i].getName());
                        items.add(itemBean);
                    }
                }
                conn = true;
            } catch (Exception e) {
                Log.i("Error en la xync", "xxxxxxxxxxxxx");
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            swipemyobjetives.setRefreshing(false);
            if (!conn) {
                try {
                    AlertDialog.Builder info = new AlertDialog.Builder(MyObjetives.this);
                    info.setTitle("Alerta");
                    info.setMessage("No se ha encontrado la información solicitada");
                    info.setCancelable(false);
                    info.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alertLogin = info.create();
                    alertLogin.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                adapter.notifyDataSetChanged();
            }
        }
    }


}
