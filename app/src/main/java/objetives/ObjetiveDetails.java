package objetives;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.movil.vida.R;
import com.movil.vida.Session;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import beans.ItemBean;

public class ObjetiveDetails extends AppCompatActivity {

    private Toolbar toolbar;
    private List<ItemBean> items = new ArrayList<>();
    private ObjetiveDetailAdapter adapter;
    private Long objetiveId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objetive_details);

        toolbar = (Toolbar) findViewById(R.id.objdetailToolbal);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Mis objetivos por mes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        RecyclerView rv = (RecyclerView) findViewById(R.id.objdetailslist);
        rv.setHasFixedSize(true);
        adapter = new ObjetiveDetailAdapter(items, this);
        rv.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        objetiveId = getIntent().getExtras().getLong("objetiveId");
        new DownloadData(ObjetiveDetails.this).execute();
    }
    public class DownloadData extends AsyncTask<Void, Void, Void> {
        boolean conn = false;
        private String url;
        private Integer year;

        public DownloadData(Context mContext) {
            url = Session.getBaseUrl(mContext);
            year= Calendar.getInstance().get(Calendar.YEAR);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
                ResponseEntity<ItemBean[]> objetives = restTemplate.getForEntity(url + "/getSecurityObjetivesbyuser?year="+year+"&employeeId="+Session.getEmployeeId(getApplicationContext())+"&objetiveId="+objetiveId, ItemBean[].class);
                ItemBean[] objs = objetives.getBody();
                if (objs.length > 0) {
                    for (int i = 0; i < objs.length; i++) {
                        ItemBean itemBean = new ItemBean();
                        itemBean.setId(objs[i].getId());
                        itemBean.setName(objs[i].getName());
                        items.add(itemBean);
                    }
                }
                conn = true;
            } catch (Exception e) {
                Log.i("Error en la xync", "xxxxxxxxxxxxx");
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            if (!conn) {
                AlertDialog.Builder info = new AlertDialog.Builder(ObjetiveDetails.this);
                info.setTitle("Error");
                info.setMessage("Asegúrate tener conexión a internet");
                info.setCancelable(false);
                info.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertLogin = info.create();
                alertLogin.show();
            } else {
                adapter.notifyDataSetChanged();
            }
        }
    }

}
