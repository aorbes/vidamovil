package di;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 6/2/2018.
 */

public class leaderItemSingleton {
    private static  List<Integer> leaderitems = null;
    public leaderItemSingleton(){

    }
    public static List<Integer> getInstance()
    {
        if (leaderitems == null)
        {
            leaderitems = new ArrayList<>();
        }
        return leaderitems;
    }

    public static void setLeaderItems(List<Integer> ldi) {
        leaderitems = ldi;
    }

    public static  void emptyLeaderItems(){
        leaderitems=null;
    }
}
