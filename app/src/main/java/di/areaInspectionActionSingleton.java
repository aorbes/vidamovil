package di;

import com.movil.vida.dao.AreaInspectionAction;

/**
 * Created by Alexander on 17/2/2018.
 */

public class areaInspectionActionSingleton {
    public static AreaInspectionAction area_inspection_action = null;
    public static Long inspectionId = null;

    public areaInspectionActionSingleton() {

    }

    public static AreaInspectionAction getInstance() {
        if (area_inspection_action == null) {
            area_inspection_action = new AreaInspectionAction();
        }
        return area_inspection_action;
    }

    public static void setInspectionId(Long inspectionIdx) {
        inspectionId = inspectionIdx;
    }

    public static void empyInspectionId() {
        inspectionId = null;
    }

    public static void setArea_inspection_action(AreaInspectionAction area_inspection_action2) {
        area_inspection_action = area_inspection_action2;
    }

    public static void emptyAreainspectionAction() {
        area_inspection_action = null;
    }

}
