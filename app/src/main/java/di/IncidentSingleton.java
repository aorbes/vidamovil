package di;

import com.movil.vida.dao.Incidents;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 27/1/2018.
 */

public class IncidentSingleton {
   public static Incidents incidents=null;
    public static List<Integer> infractions;
   public IncidentSingleton(){

    }
    public static Incidents getInstance()
    {
        if (incidents == null)
        {
            incidents = new Incidents();
        }
        return incidents;
    }

    public static List<Integer> getRiskInstance() {
        if (infractions == null) {
            infractions = new ArrayList<>();
        }
        return infractions;
    }

    public static void setInfractions(List<Integer> list) {
        infractions = list;
    }

    public static void emptyInfractions() {
        infractions = null;
    }
    public static void setIncidents(Incidents ins) {
        incidents = ins;
    }

    public static  void empyIncident(){
        incidents=null;
    }
}
