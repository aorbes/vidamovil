package di;

import com.movil.vida.dao.AuditFieldRegistry;
import com.movil.vida.dao.AuditItemRegistry;
import com.movil.vida.dao.AuditRegistry;
import com.movil.vida.dao.GeneralAudit;

import java.util.ArrayList;
import java.util.List;

public class GeneralAuditSingleton {
    public static GeneralAudit generalAudit;
    public static AuditRegistry auditRegistry;
    public static List<AuditFieldRegistry> auditFieldRegistries;
    public static List<AuditItemRegistry> auditItemRegistries;

    public static GeneralAudit getInstance() {
        if (generalAudit == null) {
            generalAudit = new GeneralAudit();
        }
        return generalAudit;
    }

    public static AuditRegistry getAuditRegistryInstance() {
        if (auditRegistry == null) {
            auditRegistry = new AuditRegistry();
        }
        return auditRegistry;
    }

    public static List<AuditFieldRegistry> getAuditFieldRegistriesInstance() {
        if (auditFieldRegistries == null) {
            auditFieldRegistries = new ArrayList<>();
        }
        return auditFieldRegistries;
    }

    public static List<AuditItemRegistry> getAuditItemRegistriesInstance() {
        if (auditItemRegistries == null) {
            auditItemRegistries = new ArrayList<>();
        }
        return auditItemRegistries;
    }

    public static void setGeneralAudit(GeneralAudit generalAudit) {
        GeneralAuditSingleton.generalAudit = generalAudit;
    }

    public static void setAuditRegistry(AuditRegistry auditRegistry) {
        GeneralAuditSingleton.auditRegistry = auditRegistry;
    }

    public static void setAuditFieldRegistries(List<AuditFieldRegistry> auditFieldRegistries) {
        GeneralAuditSingleton.auditFieldRegistries = auditFieldRegistries;
    }

    public static void setAuditItemRegistries(List<AuditItemRegistry> auditItemRegistries) {
        GeneralAuditSingleton.auditItemRegistries = auditItemRegistries;
    }

    public static void emptyGeneralAudit() {
        generalAudit = null;
    }

    public static void emptyAuditResgistry() {
        auditRegistry = null;
    }

    public static void emptyAuditFieldRegistries() {
        auditFieldRegistries = null;
    }

    public static void emptyAuditItemRegistries() {
        auditItemRegistries = null;
    }
}
