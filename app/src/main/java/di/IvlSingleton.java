package di;

import com.movil.vida.dao.Ivl;

/**
 * Created by Alexander on 6/2/2018.
 */

public class IvlSingleton {
    public static Ivl ivl=null;
    public static Long ivl_action_id = null;
    public IvlSingleton(){

    }
    public static Ivl getInstance()
    {
        if (ivl == null)
        {
            ivl = new Ivl();
        }
        return ivl;
    }

    public static void setIvl(Ivl ivl2) {
        ivl = ivl2;
    }

    public static  void emptyIvl(){
        ivl=null;
    }

    public static void setIvl_action_id(Long id) {
        ivl_action_id = id;
    }

    public static void emptyIvlActionId() {
        ivl_action_id = null;
    }
}
