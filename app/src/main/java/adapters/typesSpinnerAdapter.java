package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.movil.vida.R;

import java.util.List;

import beans.TypesBean;

/**
 * Created by Alexander on 23/1/2018.
 */

public class typesSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context activity;
    private List<TypesBean> typesBeans;

    public typesSpinnerAdapter(Context activity, List<TypesBean> typesBeans) {
        this.activity = activity;
        this.typesBeans = typesBeans;
    }

    @Override
    public int getCount() {
        return typesBeans.size();
    }

    @Override
    public Object getItem(int i) {
        return typesBeans.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long)i;
    }

    public int getPosition(String type) {
        for (int i = 0; i < typesBeans.size(); i++) {
            if (typesBeans.get(i).getType().equals(type)) {
                return i;
            }
        }
        return 0;
        //return IntStream.range(0,typesBeans.size()).filter(i->typesBeans.get(i).getType().equals(type)).findFirst().getAsInt();
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_text_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(typesBeans.get(position).getDescription());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.LEFT);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_input_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_scroll_down_blue, 0);
        txt.setText(typesBeans.get(i).getDescription());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
}
