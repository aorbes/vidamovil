package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.dao.IvlResposible;

import java.util.List;

/**
 * Created by Alexander on 23/1/2018.
 */

public class responsibleSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context activity;
    private List<IvlResposible> ivlResposibles;

    public responsibleSpinnerAdapter(Context activity, List<IvlResposible> ivlResposibles) {
        this.activity = activity;
        this.ivlResposibles = ivlResposibles;
    }

    @Override
    public int getCount() {
        return ivlResposibles.size();
    }

    @Override
    public Object getItem(int i) {
            return ivlResposibles.get(i);
    }

    @Override
    public long getItemId(int i) {
            return (long)i;
    }

    public int getPosition(IvlResposible u) {
        return ivlResposibles.indexOf(u);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_text_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(ivlResposibles.get(position).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.LEFT);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_input_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_scroll_down_blue, 0);
        txt.setText(ivlResposibles.get(i).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
}
