package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.movil.vida.R;

import java.io.ByteArrayInputStream;
import java.util.List;

public class SlideAdapter extends PagerAdapter {
    public int position;
    Context context;
    LayoutInflater layoutInflater;
    private List<byte[]> list;
    ViewPager viewPager;

    public SlideAdapter(Context context, List<byte[]> list, ViewPager pager) {
        this.context = context;
        this.list = list;
        this.viewPager = pager;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.image_silder_item, container, false);
        ImageView imgslide = view.findViewById(R.id.imgImageItem);
        TextView txtposition = view.findViewById(R.id.txtPosition);
        txtposition.setText(position + 1 + "/" + list.size());
        ByteArrayInputStream imageStream = new ByteArrayInputStream(list.get(position));
        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
        imgslide.setImageBitmap(theImage);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    public void removeView() {
        //Log.i("Position to del++  ", viewPager.getCurrentItem() + "++");
        list.remove(viewPager.getCurrentItem());
        notifyDataSetChanged();
    }

}
