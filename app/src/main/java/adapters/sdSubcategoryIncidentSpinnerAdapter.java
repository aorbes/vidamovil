package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.dao.SdSubcategoryIncident;

import java.util.List;

/**
 * Created by Alexander on 11/4/2018.
 */

public class sdSubcategoryIncidentSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context activity;
    private List<SdSubcategoryIncident> sdSubcategoryIncidents;

    public sdSubcategoryIncidentSpinnerAdapter(Context activity, List<SdSubcategoryIncident> sdSubcategoryIncidents) {
        this.activity = activity;
        this.sdSubcategoryIncidents = sdSubcategoryIncidents;
    }

    @Override
    public int getCount() {
        return sdSubcategoryIncidents.size();
    }

    @Override
    public Object getItem(int i) {
        return sdSubcategoryIncidents.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long) i;
    }

    public int getPosition(SdSubcategoryIncident sdSubCat) {
        return sdSubcategoryIncidents.indexOf(sdSubCat);
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_text_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(sdSubcategoryIncidents.get(position).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.LEFT);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_input_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_scroll_down_blue, 0);
        txt.setText(sdSubcategoryIncidents.get(i).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return txt;
    }
}
