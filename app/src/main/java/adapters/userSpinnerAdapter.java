package adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.dao.User;

import java.util.List;

/**
 * Created by Alexander on 22/1/2018.
 */

public class userSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context activity;
    private List<User> asr;

    public userSpinnerAdapter(Context context,List<User> asr) {
        this.asr=asr;
        this.activity = context;
    }

    @Override
    public int getCount() {
       return asr.size();
    }

    @Override
    public User getItem(int i) {
        return asr.get(i);
    }

    @Override
    public long getItemId(int i) {
        return (long)i;
    }

    public int getPOsition(User u) {
        return asr.indexOf(u);
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_text_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(asr.get(position).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.LEFT);
        int pad = (int) activity.getResources().getDimension(R.dimen.dropdown_input_padding);
        txt.setPadding(pad, pad, pad, pad);
        txt.setTextSize(activity.getResources().getDimension(R.dimen.dropdown_text_size));
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_scroll_down_blue, 0);
        txt.setText(asr.get(i).getName());
        txt.setTextColor(Color.parseColor("#000000"));
        return  txt;
    }

}
