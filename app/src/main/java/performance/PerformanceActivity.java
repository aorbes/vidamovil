package performance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.movil.vida.R;
import com.movil.vida.Session;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import beans.PerformanceBean;
import beans.PerformanceItemsBean;

public class PerformanceActivity extends AppCompatActivity {
    private TextView txtPeriod;
    private TextView txtEmployeeName;
    private TextView txtcharge;
    private TextView txtBudget;
    private TextView txtRealPoints;
    private TextView txtVariation;
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView listPerformanceItems;
    private List<PerformanceItemsBean> list = new ArrayList<>();
    private PerformanceItemsAdapter itemsAdapter;
    private Toolbar toolbarPerformance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance);
        toolbarPerformance = (Toolbar) findViewById(R.id.toolbarPerformance);
        setSupportActionBar(toolbarPerformance);
        getSupportActionBar().setTitle("Mi desempeño");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtPeriod = (TextView) findViewById(R.id.txtPeriod);
        txtVariation = (TextView) findViewById(R.id.txtPeriod);
        txtEmployeeName = (TextView) findViewById(R.id.txtEmployee);
        txtcharge = (TextView) findViewById(R.id.txtCharge);
        txtBudget = (TextView) findViewById(R.id.txtBudget);
        txtRealPoints = (TextView) findViewById(R.id.txtRealPoints);
        txtVariation = (TextView) findViewById(R.id.txtVariation);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeLayout);
        swipeLayout.setOnRefreshListener(() -> {
            new DownloadData().execute();
            swipeLayout.setRefreshing(true);
        });

        listPerformanceItems = (RecyclerView) findViewById(R.id.listPerformanceItems);
        listPerformanceItems.setHasFixedSize(true);
        itemsAdapter = new PerformanceItemsAdapter(list, this);
        listPerformanceItems.setAdapter(itemsAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        listPerformanceItems.setLayoutManager(llm);

        new DownloadData().execute();
    }

    public class DownloadData extends AsyncTask<Void, Void, Void> {
        boolean conn = false;
        private String url;
        PerformanceBean performanceBean;

        public DownloadData() {
            url = Session.getBaseUrl(PerformanceActivity.this);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeLayout.setRefreshing(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
                restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
                ResponseEntity<PerformanceBean> performances = restTemplate.getForEntity(url + "/getindividualsecurityreport/" + Session.getEmployeeId(PerformanceActivity.this), PerformanceBean.class);
                performanceBean = performances.getBody();
                conn = true;
            } catch (Exception e) {
                Log.i("Error en la xync", "xxxxxxxxxxxxx");
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            if (!conn) {
                AlertDialog.Builder info = new AlertDialog.Builder(PerformanceActivity.this);
                info.setTitle("Alerta");
                info.setMessage("No se ha encontrado la información solicitada");
                info.setCancelable(false);
                info.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertLogin = info.create();
                alertLogin.show();
                swipeLayout.setRefreshing(false);
            } else {

                if (performanceBean != null) {
                    txtPeriod.setText(performanceBean.getPeriod());
                    txtEmployeeName.setText(performanceBean.getEmployee());
                    txtcharge.setText(performanceBean.getCharge());
                    txtBudget.setText(performanceBean.getBudget());
                    txtRealPoints.setText(performanceBean.getReal());
                    txtVariation.setText(performanceBean.getVariation() + "%");
                    list.clear();
                    for (PerformanceItemsBean item : performanceBean.getItems()) {
                        PerformanceItemsBean itemBean = new PerformanceItemsBean();
                        itemBean.setBudget(item.getBudget());
                        itemBean.setName(item.getName());
                        itemBean.setReal(item.getReal());
                        itemBean.setVariation(item.getVariation());
                        list.add(itemBean);
                    }
                    itemsAdapter.notifyDataSetChanged();
                }
            }


            swipeLayout.setRefreshing(false);
        }
    }
}

