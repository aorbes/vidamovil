package performance;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.movil.vida.R;

import java.util.List;

import beans.PerformanceItemsBean;

public class PerformanceItemsAdapter extends RecyclerView.Adapter<PerformanceItemsAdapter.MyViewHolder> {

    List<PerformanceItemsBean> items;
    private Context context;

    public PerformanceItemsAdapter(List<PerformanceItemsBean> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public PerformanceItemsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.performanceitem, parent, false);
        PerformanceItemsAdapter.MyViewHolder vh = new PerformanceItemsAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(PerformanceItemsAdapter.MyViewHolder holder, int position) {
        holder.txtDescription.setText(items.get(position).getName());
        holder.txtBudget.setText(items.get(position).getBudget());
        holder.txtPoints.setText(items.get(position).getReal());
        holder.txtVariation.setText(items.get(position).getVariation() + "%");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtDescription;
        TextView txtBudget;
        TextView txtPoints;
        TextView txtVariation;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtDescription = itemView.findViewById(R.id.txtDescription);
            txtBudget = itemView.findViewById(R.id.txtItemBudget);
            txtPoints = itemView.findViewById(R.id.txtItemRealPoints);
            txtVariation = itemView.findViewById(R.id.txtItemVariation);
        }
    }
}
