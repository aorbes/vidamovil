package beans;

import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.dao.GeneralAuditField;

public class SpinnerBean {
    private GeneralAuditField generalAuditField;
    private Spinner spinner;
    private TextView textView;

    public SpinnerBean(GeneralAuditField generalAuditField, Spinner spinner, TextView textView) {
        this.generalAuditField = generalAuditField;
        this.spinner = spinner;
        this.textView = textView;
    }


    public GeneralAuditField getGeneralAuditField() {
        return generalAuditField;
    }

    public void setGeneralAuditField(GeneralAuditField generalAuditField) {
        this.generalAuditField = generalAuditField;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSpinner(Spinner spinner) {
        this.spinner = spinner;
    }


    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
