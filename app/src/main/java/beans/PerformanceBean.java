package beans;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PerformanceBean {
    private String period;
    private String employee;
    private String charge;
    private String real;
    private String budget;
    private String variation;
    @JsonProperty("items")
    private List<PerformanceItemsBean> items;

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public List<PerformanceItemsBean> getItems() {
        return items;
    }

    public void setItems(List<PerformanceItemsBean> items) {
        this.items = items;
    }
}
