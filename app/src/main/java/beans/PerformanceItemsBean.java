package beans;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PerformanceItemsBean {
    @JsonProperty("name")
    private String name;
    @JsonProperty("budget")
    private String budget;
    @JsonProperty("real")
    private String real;
    @JsonProperty("variation")
    private String variation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getReal() {
        return real;
    }

    public void setReal(String real) {
        this.real = real;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }
}
