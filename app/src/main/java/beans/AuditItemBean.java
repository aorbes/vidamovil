package beans;

import android.widget.Spinner;
import android.widget.TextView;

import adapters.stringSpinnerAdapter;

public class AuditItemBean {

    private Long categoryId;
    private Long itemId;
    private String optionSelected;
    private Spinner spinner;
    private TextView textView;
    private stringSpinnerAdapter adapter;

    public AuditItemBean(Long categoryId, Long itemId, String optionSelected, Spinner spinner, TextView textView,
                         stringSpinnerAdapter stringSpinnerAdapter) {
        this.categoryId = categoryId;
        this.itemId = itemId;
        this.optionSelected = optionSelected;
        this.spinner = spinner;
        this.textView = textView;
        this.adapter = stringSpinnerAdapter;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public void setSpinner(Spinner spinner) {
        this.spinner = spinner;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOptionSelected() {
        return optionSelected;
    }

    public void setOptionSelected(String optionSelected) {
        this.optionSelected = optionSelected;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }

    public stringSpinnerAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(stringSpinnerAdapter adapter) {
        this.adapter = adapter;
    }
}
