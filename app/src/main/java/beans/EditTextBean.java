package beans;

import android.widget.EditText;

import com.movil.vida.dao.GeneralAuditField;

public class EditTextBean {
    private GeneralAuditField generalAuditField;
    private EditText editText;

    public EditTextBean(GeneralAuditField generalAuditField, EditText editText) {
        this.generalAuditField = generalAuditField;
        this.editText = editText;
    }

    public GeneralAuditField getGeneralAuditField() {
        return generalAuditField;
    }

    public void setGeneralAuditField(GeneralAuditField generalAuditField) {
        this.generalAuditField = generalAuditField;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }


}
