package beans;

public class AreaInspectionExecutingBean {
    private Long id;
    private String registrationDate;
    private String creatorName;
    private String inspectedArea;
    private String participants;

    public AreaInspectionExecutingBean() {
    }

    public AreaInspectionExecutingBean(Long id, String registrationDate, String creatorName, String inspectedArea, String participants) {
        this.id = id;
        this.registrationDate = registrationDate;
        this.creatorName = creatorName;
        this.inspectedArea = inspectedArea;
        this.participants = participants;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getInspectedArea() {
        return inspectedArea;
    }

    public void setInspectedArea(String inspectedArea) {
        this.inspectedArea = inspectedArea;
    }

    public String getParticipants() {
        return participants;
    }

    public void setParticipants(String participants) {
        this.participants = participants;
    }
}
