package experientialPlan;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.ExperientialPlan;
import com.movil.vida.dao.ExperientialPlanDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.stringSpinnerAdapter;
import adapters.userSpinnerAdapter;

public class AddExperientialPlanActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private Toolbar toolbar;
    private EditText txtExperientialPlanDate, txtExperientialPlanHour, txtExperientialPlanTime, txtExperientialPlanComment;
    private TextView lblExperientialPlanDate, lblExperientialPlanHour, lblExperientialPlanTime, lblExperientialPlanArea, lblComment, lblExperientialPLanInteractionType,
            lblExperientialPlanReportName, lblExperientialPlanCoach;
    private Spinner spExperientialPlanName, spExperientialPlanCoach, spExperientialPlanArea;
    private UserDao userDao;
    private userSpinnerAdapter userSpinnerAdapter;
    private ToggleButton toggleButton;
    private ExperientialPlan experiential_plan = new ExperientialPlan();
    private ExperientialPlanDao experiential_planDao;
    private CheckBox cbkUnacemPersonal, cbkFlotaPersonal;

    private Button btnSave;
    private ScrollView scroll;
    private ImageView backArrow;
    private TextView tittle;
    private TextView action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_experiential_plan);
        toolbar = (Toolbar) findViewById(R.id.add_experiential_plan_toolbar);
        setSupportActionBar(toolbar);
        tittle = toolbar.findViewById(R.id.toolbar_title);
        // tittle.setText("Nuevo Registro");
        action = toolbar.findViewById(R.id.toolbar_action);
        action.setText("Guardar");
        action.setOnClickListener(view -> save());
        backArrow = toolbar.findViewById(R.id.imgBackarrow);
        backArrow.setOnClickListener(view -> {
            onBackPressed();
        });
        //init dao
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        experiential_planDao = daoSession.getExperientialPlanDao();
        userDao = daoSession.getUserDao();

        txtExperientialPlanDate = (EditText) findViewById(R.id.txtExperientialPlanDate);
        lblExperientialPlanDate = (TextView) findViewById(R.id.lblExperientialPlanDate);
        txtExperientialPlanDate.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddExperientialPlanActivity.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setTitle("Selecciona la Fecha");
            datePickerDialog.show(getFragmentManager(), "");
        });
        txtExperientialPlanHour = (EditText) findViewById(R.id.txtExperientialPlanHour);
        lblExperientialPlanHour = (TextView) findViewById(R.id.lblExperientialPlanHour);
        txtExperientialPlanHour.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(AddExperientialPlanActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog.setTitle("Selecciona la Hora");
            timePickerDialog.show(getFragmentManager(), "");
        });
        lblExperientialPlanReportName = (TextView) findViewById(R.id.lblExperientialPlanReportName);
        spExperientialPlanName = (Spinner) findViewById(R.id.spExperientialPlanName);

        List<User> users = new ArrayList<>();
        User user = new User();
        user.setName(getResources().getString(R.string.select_option));
        users.add(user);
        users.addAll(userDao.loadAll());
        Collections.sort(users.subList(1, users.size()), (user1, user2) -> user1.getName().compareTo(user2.getName())
        );
        userSpinnerAdapter = new userSpinnerAdapter(getApplicationContext(), users);
        spExperientialPlanName.setAdapter(userSpinnerAdapter);

        lblExperientialPlanCoach = (TextView) findViewById(R.id.lblExperientialPlanCoach);
        spExperientialPlanCoach = (Spinner) findViewById(R.id.spExperientialPlanCoach);
        spExperientialPlanCoach.setAdapter(userSpinnerAdapter);
        txtExperientialPlanTime = (EditText) findViewById(R.id.txtExperientialPlanTime);
        lblExperientialPlanTime = (TextView) findViewById(R.id.lblExperientialPlanTime);
        txtExperientialPlanTime.setOnClickListener(view -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(AddExperientialPlanActivity.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog.setTitle("Selecciona el tiempo destinado");
            timePickerDialog.show(getFragmentManager(), "");
        });
        lblExperientialPlanArea = (TextView) findViewById(R.id.lblExperientialPlanArea);
        spExperientialPlanArea = (Spinner) findViewById(R.id.spExperientialPlanArea);
        List<String> areas = new ArrayList<>();
        areas.add("Seleccione el área");
        areas.add("ACC-Acompañamiento en ciclo de carga del cemento");
        areas.add("ARCA-Acompalamiento en ruta-caliza");
        areas.add("ARCE-Acompañamiento en ruta cemento-combustible");
        areas.add("DAF-Descarga(aceite,fundas,importaciones)");
        stringSpinnerAdapter stringSpinnerAdapterAreas = new stringSpinnerAdapter(getApplication(), areas);
        spExperientialPlanArea.setAdapter(stringSpinnerAdapterAreas);
        txtExperientialPlanComment = (EditText) findViewById(R.id.txtExperientialPlanComment);
        lblComment = (TextView) findViewById(R.id.lblComment);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        lblExperientialPLanInteractionType = (TextView) findViewById(R.id.lblExperientialPLanInteractionType);
        cbkUnacemPersonal = (CheckBox) findViewById(R.id.cbkUnacemPersonal);
        cbkUnacemPersonal.setOnCheckedChangeListener((compoundButton, b) -> {
            lblExperientialPLanInteractionType.setTextColor(Color.BLACK);
            lblExperientialPLanInteractionType.setError(null);
            lblExperientialPLanInteractionType.setText("Tipo de Personal con que tuvo interación");
        });
        cbkFlotaPersonal = (CheckBox) findViewById(R.id.cbkFlotaPersonal);
        cbkFlotaPersonal.setOnCheckedChangeListener((compoundButton, b) -> {
            lblExperientialPLanInteractionType.setTextColor(Color.BLACK);
            lblExperientialPLanInteractionType.setError(null);
            lblExperientialPLanInteractionType.setText("Tipo de Personal con que tuvo interación");
        });

        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(view -> {
            save();
        });
        scroll = (ScrollView) findViewById(R.id.scrollAddExpPLan);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            // tittle.setText("Editar Registro");
            action.setText("Guardar Cambios");
            experiential_plan = experiential_planDao.load(b.getLong("ExpPlanId"));
            String regDate[] = experiential_plan.getRegistrationDate().split(" ");
            txtExperientialPlanDate.setText(regDate[0]);
            txtExperientialPlanHour.setText(regDate[1]);
            spExperientialPlanName.setSelection(userSpinnerAdapter.getPOsition(userDao.load(experiential_plan.getName())));
            spExperientialPlanCoach.setSelection(userSpinnerAdapter.getPOsition(userDao.load(experiential_plan.getCoach())));
            txtExperientialPlanTime.setText(experiential_plan.getTime());
            spExperientialPlanArea.setSelection(stringSpinnerAdapterAreas.getPosition(experiential_plan.getArea()));
            txtExperientialPlanComment.setText(experiential_plan.getComment());
            toggleButton.setChecked(experiential_plan.getDesviation().contains("SI"));
            cbkUnacemPersonal.setChecked(experiential_plan.getPersonalType().contains("PU") || experiential_plan.getPersonalType().contains("BO"));
            cbkFlotaPersonal.setChecked(experiential_plan.getPersonalType().contains("CF") || experiential_plan.getPersonalType().contains("BO"));
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       getMenuInflater().inflate(R.menu.save_edit_menu, menu);
       return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                save();
            case R.id.edit:
                save();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (experiential_plan.getId() != null) {
            menu.findItem(R.id.save).setVisible(false);
        } else {
            menu.findItem(R.id.edit).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }*/

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtExperientialPlanDate.setTextColor(Color.BLACK);
        txtExperientialPlanDate.setError(null);
        txtExperientialPlanDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hour = "";
        String min = "";
        if (hourOfDay < 10) {
            hour = "0";
        }
        if (minute < 10) {
            min = "0";
        }
        if (view.getTitle().equals("Selecciona la Hora")) {
            txtExperientialPlanHour.setText(hour + hourOfDay + ":" + min + minute);
            txtExperientialPlanHour.setError(null);
            txtExperientialPlanHour.setTextColor(Color.BLACK);
        } else {
            txtExperientialPlanTime.setText(hour + hourOfDay + ":" + min + minute);
            txtExperientialPlanTime.setError(null);
            txtExperientialPlanTime.setTextColor(Color.BLACK);
        }
    }

    public boolean validate() {
        if (txtExperientialPlanDate.getText().toString().isEmpty() || txtExperientialPlanDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtExperientialPlanDate.setError(getString(R.string.error_field_required));
            txtExperientialPlanDate.setTextColor(Color.RED);
            txtExperientialPlanDate.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanDate.getTop());
            });
            return false;
        }
        if (txtExperientialPlanHour.getText().toString().isEmpty() || txtExperientialPlanHour.getText().toString().contains(getString(R.string.error_field_required))) {
            txtExperientialPlanHour.setError(getString(R.string.error_field_required));
            txtExperientialPlanHour.setTextColor(Color.RED);
            txtExperientialPlanHour.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanHour.getTop());
            });
            return false;
        }
        if (((User) spExperientialPlanName.getSelectedItem()).getName().equals(getResources().getString(R.string.select_option))) {
            TextView errorText = (TextView) spExperientialPlanName.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanReportName.getTop());
            });
            return false;
        }
        if (((User) spExperientialPlanCoach.getSelectedItem()).getName().equals(getResources().getString(R.string.select_option))) {
            TextView errorText = (TextView) spExperientialPlanCoach.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanCoach.getTop());
            });
            return false;
        }
        if (txtExperientialPlanTime.getText().toString().isEmpty() || txtExperientialPlanTime.getText().toString().contains(getString(R.string.error_field_required))) {
            txtExperientialPlanTime.setError(getString(R.string.error_field_required));
            txtExperientialPlanTime.setTextColor(Color.RED);
            txtExperientialPlanTime.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanTime.getTop());
            });
            return false;
        }
        if (spExperientialPlanArea.getSelectedItem().toString().equals("Seleccione el área")) {
            TextView errorText = (TextView) spExperientialPlanArea.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPlanArea.getTop());
            });
            return false;
        }
        if (txtExperientialPlanComment.getText().toString().isEmpty()) {
            txtExperientialPlanComment.setError("Este campo es obligatorio");
            lblComment.requestFocus();
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblComment.getTop());
            });
            return false;
        }
        if (!cbkFlotaPersonal.isChecked() && !cbkUnacemPersonal.isChecked()) {
            lblExperientialPLanInteractionType.requestFocus();
            lblExperientialPLanInteractionType.setError("Este campo es obligatorio");
            lblExperientialPLanInteractionType.setText("Este campo es obligatorio");
            lblExperientialPLanInteractionType.setTextColor(Color.RED);
            scroll.post(() -> {
                scroll.smoothScrollTo(0, lblExperientialPLanInteractionType.getTop());
            });
            return false;
        }
        return true;
    }

    public void save() {
        if (validate()) {
            experiential_plan.setRegistrationDate(txtExperientialPlanDate.getText().toString() + " " + txtExperientialPlanHour.getText().toString());
            experiential_plan.setName(((User) spExperientialPlanName.getSelectedItem()).getId());
            experiential_plan.setCoach(((User) spExperientialPlanCoach.getSelectedItem()).getId());
            experiential_plan.setTime(txtExperientialPlanTime.getText().toString());
            experiential_plan.setArea(spExperientialPlanArea.getSelectedItem().toString());
            experiential_plan.setComment(txtExperientialPlanComment.getText().toString());
            experiential_plan.setDesviation(toggleButton.getText().toString());
            if (cbkUnacemPersonal.isChecked()) {
                experiential_plan.setPersonalType("PU");
            }
            if (cbkFlotaPersonal.isChecked()) {
                experiential_plan.setPersonalType("CF");
            }

            if (cbkUnacemPersonal.isChecked() && cbkFlotaPersonal.isChecked()) {
                experiential_plan.setPersonalType("BO");
            }
            if (experiential_plan.getId() != null) {
                experiential_planDao.update(experiential_plan);
                Toast.makeText(AddExperientialPlanActivity.this, "Se ha actualizado el plan vivencial " + experiential_plan.getId(), Toast.LENGTH_LONG).show();
            } else {
                experiential_plan.setSyncState(0);
                Long id = experiential_planDao.insert(experiential_plan);
                Toast.makeText(this, "Se ha registrado el plan vivencial " + id, Toast.LENGTH_LONG).show();
            }

            startActivity(new Intent(getApplicationContext(), ExperientialPlanListActivity.class));
            finish();
        }
    }
}
