package experientialPlan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.ExperientialPlan;
import com.movil.vida.dao.ExperientialPlanDao;

import java.util.List;

/**
 * Created by Alexander on 19/2/2018.
 */

public class ExperientialPlanAdapter extends RecyclerView.Adapter<ExperientialPlanAdapter.MyViewHolder> {

    private List<ExperientialPlan> items;
    private Context mContext;
    private ExperientialPlanDao experientialPlanDao;

    public ExperientialPlanAdapter(List<ExperientialPlan> items, Context context) {
        this.items = items;
        this.mContext = context;
        DaoSession daoSession = ((DaoApp) context.getApplicationContext()).getDaoSession();
        experientialPlanDao = daoSession.getExperientialPlanDao();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.experiential_plan_item_card, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.lblId.setText(items.get(position).getId() + "");
        holder.txtExperientialPlanDate.setText(items.get(position).getRegistrationDate());
        holder.txtExperientialPlanTime.setText(items.get(position).getTime());
        holder.txtExperientialPlanArea.setText(items.get(position).getArea());
        if (items.get(position).getSyncState() == 0) {
            holder.imgSyncSatate.setImageResource(R.drawable.ic_sync_black_24dp);
        } else {
            holder.imgSyncSatate.setImageResource(R.drawable.ic_check_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtExperientialPlanDate;
        private TextView txtExperientialPlanArea;
        private TextView txtExperientialPlanTime, lblId;
        private Button btnEdit, btnDelete;
        private ImageView imgSyncSatate;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtExperientialPlanDate = itemView.findViewById(R.id.txtExperientialPlanDate);
            txtExperientialPlanArea = itemView.findViewById(R.id.txtExperientialPlanArea);
            txtExperientialPlanTime = itemView.findViewById(R.id.txtExperientialPlanTime);
            lblId = itemView.findViewById(R.id.lblId);
            imgSyncSatate = itemView.findViewById(R.id.imgSyncSatate);
            btnEdit = itemView.findViewById(R.id.btnEdit);
            btnEdit.setOnClickListener(view -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) view.getContext();
                    ExperientialPlan experientialPlan = items.get(getAdapterPosition());
                    args.putLong("ExpPlanId", experientialPlan.getId());
                    Intent intent = new Intent(view.getContext(), AddExperientialPlanActivity.class);
                    intent.putExtras(args);
                    activity.startActivity(intent);
                }
            });

            btnDelete = itemView.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(view -> {
                AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
                adb.setTitle("Eliminar el Plan?");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("Eliminar", (dialogInterface, i) -> {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        ExperientialPlan experientialPlan = items.get(pos);
                        experientialPlanDao.delete(experientialPlan);
                        items.remove(pos);
                        notifyItemRemoved(pos);
                    }

                });
                adb.setNegativeButton("Cancelar", (v1, v2) -> {
                });
                adb.show();
            });
        }
    }

}
