package experientialPlan;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.ExperientialPlan;
import com.movil.vida.dao.ExperientialPlanDao;

import java.util.List;

public class ExperientialPlanListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private List<ExperientialPlan> experiential_plans;
    private ExperientialPlanDao experiential_planDao;
    private RecyclerView recyclerView;
    private ExperientialPlanAdapter experientialPlanAdapter;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton btnAddExperientialPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiential_plan_list);
        toolbar = (Toolbar) findViewById(R.id.experientialPlan_listbar_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Planes vivenciales registrados");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        experiential_planDao = daoSession.getExperientialPlanDao();

        experiential_plans = experiential_planDao.loadAll();
        recyclerView = (RecyclerView) findViewById(R.id.list_experiential_plan);
        recyclerView.setHasFixedSize(true);
        experientialPlanAdapter = new ExperientialPlanAdapter(experiential_plans, ExperientialPlanListActivity.this);
        recyclerView.setAdapter(experientialPlanAdapter);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        btnAddExperientialPlan = (FloatingActionButton) findViewById(R.id.btnAddExperientialPlan);
        btnAddExperientialPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), AddExperientialPlanActivity.class));
                //startActivity(new Intent(getApplicationContext(),testAddExpPlanActivity.class));
            }
        });
    }
}
