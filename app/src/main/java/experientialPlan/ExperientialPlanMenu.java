package experientialPlan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.ExperientialPlan;
import com.movil.vida.dao.ExperientialPlanDao;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class ExperientialPlanMenu extends AppCompatActivity {

    String url;
    private ConstraintLayout lytListExperientialPlan, lytAddExperientialPlan;
    private Toolbar toolbarExplan;
    private Button btnUploadExperientialPlan;
    private TextView lblUploaded, lblNotUploaded;
    private ExperientialPlanDao experientialPlanDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiential_plan_menu);
        url = Session.getBaseUrl(ExperientialPlanMenu.this) + "/uploadexpplan";
        toolbarExplan = (Toolbar) findViewById(R.id.toolbarExplan);
        setSupportActionBar(toolbarExplan);
        getSupportActionBar().setTitle("Menú Plan Vivencial");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        experientialPlanDao = daoSession.getExperientialPlanDao();

        lytAddExperientialPlan = (ConstraintLayout) findViewById(R.id.lytAddExperientialPlan);
        lytListExperientialPlan = (ConstraintLayout) findViewById(R.id.lytListExperientialPlan);
        lytAddExperientialPlan.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), AddExperientialPlanActivity.class));
        });
        lytListExperientialPlan.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), ExperientialPlanListActivity.class));
        });
        btnUploadExperientialPlan = (Button) findViewById(R.id.btnUploadExperientialPlan);
        btnUploadExperientialPlan.setOnClickListener(view -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(ExperientialPlanMenu.this);
            adb.setTitle("Inciar la subida de registros a vida?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("OK", (dialogInterface, i) -> {
                if (experientialPlanDao.queryBuilder().where(ExperientialPlanDao.Properties.SyncState.eq(0)).list().size() > 0) {
                    new HttpRequestTask(ExperientialPlanMenu.this).execute();
                } else if (experientialPlanDao.loadAll().size() < 1) {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(ExperientialPlanMenu.this);
                    goLogin.setMessage("No hay registros!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("OK", (dialogInterface1, i1) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                } else {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(ExperientialPlanMenu.this);
                    goLogin.setMessage("Todos los registros se han subido a vida!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("OK", (dialogInterface2, i2) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                }
            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();
        });
        lblUploaded = (TextView) findViewById(R.id.txtUploaded);
        lblNotUploaded = (TextView) findViewById(R.id.txtNotUploaded);
        updateUploadeds();
    }

    private void updateUploadeds() {
        Long uploaded = experientialPlanDao.queryBuilder().where(ExperientialPlanDao.Properties.SyncState.eq(1)).count();
        lblUploaded.setText(uploaded + "");
        Long notUploaded = experientialPlanDao.queryBuilder().where(ExperientialPlanDao.Properties.SyncState.eq(0)).count();
        lblNotUploaded.setText(notUploaded + "");
    }

    private class HttpRequestTask extends AsyncTask<Void, String, Void> {
        public int total;
        public int uploaded = 0;
        public boolean conn = false;
        ProgressDialog progDailog;
        Context context;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();

        public HttpRequestTask(Context context) {
            this.context = context;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            total = experientialPlanDao.queryBuilder().where(ExperientialPlanDao.Properties.SyncState.eq(0)).list().size();
            progDailog = new ProgressDialog(ExperientialPlanMenu.this);
            progDailog.setTitle("Subiendo Planes Vivenciales");
            progDailog.setMessage("Espere mientras se sube los registros..." + uploaded + "/" + total);
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                for (ExperientialPlan experientialPlan : experientialPlanDao.queryBuilder().where(ExperientialPlanDao.Properties.SyncState.eq(0)).list()) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("registrationDate", experientialPlan.getRegistrationDate());
                    jsonObject.put("name", experientialPlan.getName());
                    jsonObject.put("coach", experientialPlan.getCoach());
                    jsonObject.put("area", experientialPlan.getArea());
                    jsonObject.put("time", experientialPlan.getTime());
                    jsonObject.put("comment", experientialPlan.getComment());
                    jsonObject.put("desviation", experientialPlan.getDesviation());
                    jsonObject.put("personalType", experientialPlan.getPersonalType());
                    HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
                    Log.i("Tyring to upload", entity.getBody());
                    ResponseEntity<String> response =
                            restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

                    // ResponseEntity<ExperientialPlan> responseEntity = restTemplate.postForEntity(url,  experientialPlan, ExperientialPlan.class);
                    conn = true;
                    Log.i("Uploaded", experientialPlan.getId() + "--------------------------------------");
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Espere mientras se sube los registros..." + uploaded++ + "/" + total);
                        experientialPlan.setSyncState(1);
                        experientialPlanDao.update(experientialPlan);
                    } else {
                        conn = false;
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                updateUploadeds();
                android.support.v7.app.AlertDialog.Builder goLogin = new android.support.v7.app.AlertDialog.Builder(context);
                goLogin.setMessage("Se subió los planes vivenciales al  servidor correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("OK", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                android.support.v7.app.AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }
}
