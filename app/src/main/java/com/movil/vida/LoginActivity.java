package com.movil.vida;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.Result;
import com.movil.vida.dao.User;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.Arrays;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {


    private Button mconfigButton, synButton;

    private ZXingScannerView zXingScannerView;
    private String url;
    private String android_id;
    String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
    private EditText ip;
    private EditText port;
    private Button btnSaveIpPort;
    private Button btnCancelDialog;
    //private Button btn_test_login;
    private AlertDialog dialog;
    private boolean scann = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initViews();
        System.out.println("Log 1.4");
    }

    void initViews() {
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        url = Session.getBaseUrl(LoginActivity.this);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.configdialog, null);
        mBuilder.setView(mView);
        dialog = mBuilder.create();
        ip = mView.findViewById(R.id.vidaserverip);
        ip.setText(Session.getIp(LoginActivity.this));
        port = mView.findViewById(R.id.vidaserverport);
        port.setText(Session.getPort(LoginActivity.this));
        btnSaveIpPort = mView.findViewById(R.id.btnSaveIpPort);
        btnSaveIpPort.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateDialog()) {
                    Session.setIpPort(LoginActivity.this, ip.getText().toString(), port.getText().toString());
                    if (port.getText().toString().trim().isEmpty()) {
                        Session.setBaseUrl(LoginActivity.this, "http://" + Session.getIp(LoginActivity.this) + "/SafeTeam/api/movildata");
                        url = Session.getBaseUrl(LoginActivity.this);
                    } else {
                        Session.setBaseUrl(LoginActivity.this, "http://" + Session.getIp(LoginActivity.this) + ":" + Session.getPort(LoginActivity.this) + "/SafeTeam/api/movildata");
                        url = Session.getBaseUrl(LoginActivity.this);
                    }
                    Toast.makeText(getApplicationContext(), "Se ha actualizado la dirección del servidor correctamente", Toast.LENGTH_LONG).show();
                    dialog.cancel();
                }

            }
        });
        btnCancelDialog = mView.findViewById(R.id.btnCancelDialog);
        btnCancelDialog.setOnClickListener(view -> {
            dialog.cancel();
        });
        mconfigButton = (Button) findViewById(R.id.btn_config);
        mconfigButton.setOnClickListener(view -> {
            dialog.show();
        });
      /*  btn_test_login= (Button) findViewById(R.id.btn_test_login);
        btn_test_login.setOnClickListener(view ->  {
                Session.setUserName(getApplicationContext(),"Usuario de Prueba Tesis","Email de Prueba Tesis","0");
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
               finish();
        });*/
        synButton = (Button) findViewById(R.id.scanqrcode);
        synButton.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                            5);
                }
            } else {
                scanQRCode(view);
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scanQRCode(synButton);
            }
        }
    }

    public boolean validateDialog() {
        if (ip.getText().toString().isEmpty()) {
            ip.setError("La ip/url es obligatoria");
            ip.requestFocus();
            return false;
        }
        /*if(!Patterns.IP_ADDRESS.matcher(ip.getText().toString()).matches()){
            ip.setError("Ingrese una ip válida");
            ip.requestFocus();
            return false;
        }*/
      /*  if(port.getText().toString().isEmpty()){
            port.setError("El puerto es obligatorio");
            port.requestFocus();
            return false;
        }*/

        return true;
    }

    public void scanQRCode(View v) {
        zXingScannerView = new ZXingScannerView(this);
        setContentView(zXingScannerView);
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
        scann = true;
    }



    @Override
    public void handleResult(Result result) {
        Log.i("Entró al handler", "+++++++");
        System.out.println("El código QR " + result.getText());
        Log.v("HandlerResult", result.getText());
        scann = false;
        String res[] = result.getText().split(",");
        setContentView(R.layout.activity_login);
        try {
            new doLogin(res[0], res[1], "QR", this).execute();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Escanee el código QR generado en Vida", Toast.LENGTH_LONG).show();
        }
        zXingScannerView.removeAllViews();
        zXingScannerView.stopCamera();
        initViews();
        // setContentView(R.layout.activity_login);
        //  zXingScannerView.resumeCameraPreview(this);
    }

    @Override
    public void onBackPressed() {
        Log.i("Entró al backpresed", "+++++++");
        if (scann) {
            setContentView(R.layout.activity_login);
            zXingScannerView.removeAllViews();
            zXingScannerView.stopCamera();
            initViews();
        } else {
            finishAffinity();
        }
        scann = false;
    }



    public class doLogin extends AsyncTask<Void, Void, Void> {
        Context context;
        String login, password, type;

        ProgressDialog progDailog;
        ResponseEntity<User> response;

        public doLogin(String login, String password, String type, Context context) {
            this.login = login;
            this.password = password;
            this.context = context;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(LoginActivity.this);
            progDailog.setMessage("Iniciando Sesion...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("Test Login 1.3");
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            JSONObject obj = new JSONObject();

            try {
                obj.put("login", login);
                obj.put("password", password);
                obj.put("devicekey", android_id);
                obj.put("devicename", deviceName);
                HttpEntity<String> httpEntity = new HttpEntity<String>(obj.toString(), headers);
                System.out.println("Esto se  va a enviar " + httpEntity.toString());
                response = restTemplate.exchange(url + "/login", HttpMethod.POST, httpEntity, User.class);
                //this.response = response.getBody();
                // System.out.println("Esta es la respuesta entity " + response.getBody());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                progDailog.dismiss();
                if (this.response != null) {
                    Session.setUserName(context, this.response.getBody().getName(), this.response.getBody().getEmail(), this.response.getBody().getEmployeeId());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                    goLogin.setTitle("Alerta!!");
                    goLogin.setMessage("Ocurrió un error al intentar loguearse!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                        dialog.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}

