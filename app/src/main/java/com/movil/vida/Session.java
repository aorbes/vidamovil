package com.movil.vida;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Alexander on 26/11/2017.
 */

public class Session {

    static final String USER_NAME= "username";
    static final String EMAIL= "email";
    static final String IP= "ip";
    static final String PORT= "port";
    static final String BASE_URL= "base_url";
    static final String SYNC = "sync_state";
    static final String EMPLOYEE_ID = "employee_id";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setUserName(Context ctx, String userName, String email, String employeeId)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_NAME, userName);
        editor.putString(EMAIL,email);
        editor.putString(EMPLOYEE_ID, employeeId);
        editor.commit();
    }
    public static void setIpPort(Context ctx, String ip,String port) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(IP, ip);
        editor.putString(PORT,port);
        editor.commit();
    }

    public static void setBaseUrl(Context ctx, String base_url) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(BASE_URL,base_url);
        editor.commit();
    }
    public static String getBaseUrl(Context ctx) {
        return getSharedPreferences(ctx).getString(BASE_URL, "");
    }

    public static String getUserName(Context ctx) {
        return getSharedPreferences(ctx).getString(USER_NAME, "");
    }
    public static String getEmail(Context ctx) {
        return getSharedPreferences(ctx).getString(EMAIL, "");
    }

    public static String getEmployeeId(Context context) {
        return getSharedPreferences(context).getString(EMPLOYEE_ID, "");
    }
    public static String getIp(Context ctx) {
        return getSharedPreferences(ctx).getString(IP, "");
    }
    public static String getPort(Context ctx) {
        return getSharedPreferences(ctx).getString(PORT, "");
    }

    public static void setSync(Context context, String state) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(SYNC, state);
        editor.commit();
    }

    public static String getSync(Context context) {
        return getSharedPreferences(context).getString(SYNC, "");
    }
    public  static void logout(Context ctx){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(USER_NAME, null);
        editor.putString(EMAIL,null);
        editor.putString(EMPLOYEE_ID, null);
     //   editor.clear();
        editor.commit();
    }
}
