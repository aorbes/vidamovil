package com.movil.vida;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView = null;
    Toolbar toolbar = null;
    Button sincronize;
    private String url;
    ProgressBar progressBar;
    private TextView session_username, session_email;
    private android.app.AlertDialog dialog;
    private EditText ip;
    private EditText port;
    private Button btnSaveIpPort;
    private Button btnCancelDialog;
    private View mView;
    private android.app.AlertDialog.Builder mBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        if (Session.getUserName(MainActivity.this).length() == 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            // Stay at the current activity.
        }

        //if (Session.getIp(MainActivity.this).length() == 0 && Session.getPort(MainActivity.this).length() == 0) {
        //Session.setIpPort(MainActivity.this, "10.52.0.36", "");
        //Session.setIpPort(MainActivity.this, "c8800050.ngrok.io", "");

        //}

       // if (Session.getBaseUrl(MainActivity.this).length() == 0) {
            Session.setBaseUrl(MainActivity.this, "http://" + Session.getIp(MainActivity.this) + "/SafeTeam/api/movildata");
        //}
        url = Session.getBaseUrl(MainActivity.this);
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new MainFragment()).commit();
        }
        toolbar = (Toolbar) findViewById(R.id.add_experiential_plan_toolbar);
        setSupportActionBar(toolbar);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        session_username = header.findViewById(R.id.profile_username);
        session_email = header.findViewById(R.id.profile_email);

        session_username.setText(Session.getUserName(this));
        session_email.setText(Session.getEmail(this));

        mBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        mView = getLayoutInflater().inflate(R.layout.configdialog, null);
        mBuilder.setView(mView);
        dialog = mBuilder.create();
        ip = mView.findViewById(R.id.vidaserverip);
        ip.setText(Session.getIp(MainActivity.this));
        port = mView.findViewById(R.id.vidaserverport);
        port.setText(Session.getPort(MainActivity.this));
        btnSaveIpPort = mView.findViewById(R.id.btnSaveIpPort);
        btnSaveIpPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateDialog()) {
                    Session.setIpPort(MainActivity.this, ip.getText().toString(), port.getText().toString());
                    if (port.getText().toString().trim().isEmpty()) {
                        Session.setBaseUrl(MainActivity.this, "http://" + Session.getIp(MainActivity.this) + "/SafeTeam/api/movildata");
                    } else {
                        Session.setBaseUrl(MainActivity.this, "http://" + Session.getIp(MainActivity.this) + ":" + Session.getPort(MainActivity.this) + "/SafeTeam/api/movildata");
                    }
                    dialog.cancel();
                    mBuilder.setView(null);
                }

            }
        });

        btnCancelDialog = mView.findViewById(R.id.btnCancelDialog);
        btnCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                mBuilder.setView(null);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

   /* Database db=((DaoApp) getApplication()).getDaoSession().getDatabase();
        DaoMaster.createAllTables(db,true);*/


    }

    public boolean validateDialog() {
        if (ip.getText().toString().isEmpty()) {
            ip.setError("La ip/url es obligatoria");
            ip.requestFocus();
            return false;
        }
      /*  if (!Patterns.IP_ADDRESS.matcher(ip.getText().toString()).matches()) {
            ip.setError("Ingrese una ip válida");
            ip.requestFocus();
            return false;
        }
        if (port.getText().toString().isEmpty()) {
            port.setError("El puerto es obligatorio");
            port.requestFocus();
            return false;
        }*/

        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_init_sync) {
            System.out.println("Se inica la sincronización-----");
            new DownloadData(this).execute();
            //   new HttpRequestTask(this).execute();
        } else if (id == R.id.nav_dashoboard) {
            MainFragment mainFragment = new MainFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, mainFragment).commit();

        } /*else if (id == R.id.nav_myobjetives) {
            startActivity(new Intent(getApplicationContext(), MyObjetives.class));
        } else if (id == R.id.nav_mypoints) {
            startActivity(new Intent(getApplicationContext(), PerformanceActivity.class));
        } */ else if (id == R.id.nav_config) {
            dialog.show();


        } else if (id == R.id.nav_logout) {
            Session.logout(this);
            ProgressDialog progDailog = new ProgressDialog(MainActivity.this);
            progDailog.setMessage("Cerrando Sesion...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
            new Handler().postDelayed(() -> {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                progDailog.dismiss();
            }, 2000);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
