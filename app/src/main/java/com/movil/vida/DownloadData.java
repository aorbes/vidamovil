package com.movil.vida;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.movil.vida.dao.Area;
import com.movil.vida.dao.AreaDao;
import com.movil.vida.dao.Contractor;
import com.movil.vida.dao.ContractorDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Deparmet;
import com.movil.vida.dao.DeparmetDao;
import com.movil.vida.dao.Driver;
import com.movil.vida.dao.DriverDao;
import com.movil.vida.dao.GeneralAudit;
import com.movil.vida.dao.GeneralAuditCategory;
import com.movil.vida.dao.GeneralAuditCategoryDao;
import com.movil.vida.dao.GeneralAuditDao;
import com.movil.vida.dao.GeneralAuditField;
import com.movil.vida.dao.GeneralAuditFieldDao;
import com.movil.vida.dao.GeneralAuditItem;
import com.movil.vida.dao.GeneralAuditItemDao;
import com.movil.vida.dao.IvlCatalog;
import com.movil.vida.dao.IvlCatalogDao;
import com.movil.vida.dao.IvlResposible;
import com.movil.vida.dao.IvlResposibleDao;
import com.movil.vida.dao.Risk;
import com.movil.vida.dao.RiskDao;
import com.movil.vida.dao.SdCategoryIncident;
import com.movil.vida.dao.SdCategoryIncidentDao;
import com.movil.vida.dao.SdSubcategoryIncident;
import com.movil.vida.dao.SdSubcategoryIncidentDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.movil.vida.dao.Vehicle;
import com.movil.vida.dao.VehicleDao;
import com.movil.vida.dao.VehicleDriver;
import com.movil.vida.dao.VehicleDriverDao;
import com.movil.vida.dao.rule;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

public class DownloadData extends AsyncTask<Void, String, Void> {
    UserDao userDao;
    DeparmetDao deparmetDao;
    AreaDao areaDao;
    com.movil.vida.dao.ruleDao ruleDao;
    ContractorDao contractorDao;
    IvlCatalogDao ivl_catalogDao;
    IvlResposibleDao ivl_resposibleDao;
    VehicleDao vehicleDao;
    DriverDao driverDao;
    VehicleDriverDao vehicleDriverDao;
    SdCategoryIncidentDao sdCategoryIncidentDao;
    SdSubcategoryIncidentDao sdSubcategoryIncidentDao;
    RiskDao riskDao;
    GeneralAuditDao generalAuditDao;
    GeneralAuditFieldDao generalAuditFieldDao;
    GeneralAuditCategoryDao generalAuditCategoryDao;
    GeneralAuditItemDao generalAuditItemDao;
    ProgressDialog progDailog;
    Context context;
    boolean conn = false;
    private String url;


    public DownloadData(Context mContext) {
        url = Session.getBaseUrl(mContext);
        this.context = mContext;
        DaoSession daoSession = ((DaoApp) mContext.getApplicationContext()).getDaoSession();
        userDao = daoSession.getUserDao();
        deparmetDao = daoSession.getDeparmetDao();
        areaDao = daoSession.getAreaDao();
        ruleDao = daoSession.getRuleDao();
        contractorDao = daoSession.getContractorDao();
        ivl_catalogDao = daoSession.getIvlCatalogDao();
        ivl_resposibleDao = daoSession.getIvlResposibleDao();
        vehicleDao = daoSession.getVehicleDao();
        driverDao = daoSession.getDriverDao();
        vehicleDriverDao = daoSession.getVehicleDriverDao();
        sdCategoryIncidentDao = daoSession.getSdCategoryIncidentDao();
        sdSubcategoryIncidentDao = daoSession.getSdSubcategoryIncidentDao();
        riskDao = daoSession.getRiskDao();
        generalAuditDao = daoSession.getGeneralAuditDao();
        generalAuditFieldDao = daoSession.getGeneralAuditFieldDao();
        generalAuditCategoryDao = daoSession.getGeneralAuditCategoryDao();
        generalAuditItemDao = daoSession.getGeneralAuditItemDao();
    }


    @Override
    protected void onPreExecute() {
        System.out.println("Se está preparando la sincronización");
        super.onPreExecute();
        progDailog = new ProgressDialog(context);
        progDailog.setTitle("Sincronización");
        progDailog.setMessage("Descargando datos de vida...");
        //progDailog.setIndeterminate(false);
        progDailog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progDailog.setCancelable(false);
        progDailog.setMax(100);
        progDailog.show();

    }

    @Override
    protected Void doInBackground(Void... params) {
        try {


            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            //load users
            System.out.println("Se inica la sincronización de usuarios");


            this.publishProgress(String.valueOf(5), "Descargando usuarios...");
            ResponseEntity<User[]> responseEntity = restTemplate.getForEntity(url + "/employees", User[].class);
            User[] users = responseEntity.getBody();
            if (users.length > 0) {
                userDao.deleteAll();
                for (int i = 0; i < users.length; i++) {
                    userDao.insert(users[i]);
                }

            }
            System.out.println("Usuarios cargados-- " + userDao.loadAll().size());
            //load areas
            System.out.println("Se inica la sincronización de areas");
            this.publishProgress(String.valueOf(10), "Descargando áreas...");
            ResponseEntity<Area[]> area = restTemplate.getForEntity(url + "/areas", Area[].class);
            Area[] areas = area.getBody();
            if (areas.length > 0) {
                areaDao.deleteAll();
                for (int i = 0; i < areas.length; i++) {
                    areaDao.insert(areas[i]);
                }
            }
            System.out.println("Areas cargadas-- " + areaDao.loadAll().size());
            //load asocrules
            System.out.println("Se inica la sincronización de reglas asociadas");
            this.publishProgress(String.valueOf(15), "Descargando reglas asociadas...");
            ResponseEntity<rule[]> response = restTemplate.getForEntity(url + "/relatetype", rule[].class);
            rule[] rules = response.getBody();
            if (rules.length > 0) {
                ruleDao.deleteAll();
                for (int i = 0; i < rules.length; i++) {
                    ruleDao.insert(rules[i]);
                }
            }
            System.out.println("Reglas asociadas cargadas-- " + ruleDao.loadAll().size());

//load departments
            System.out.println("Se inica la sincronización de departamentos");
            this.publishProgress(String.valueOf(20), "Descargando departamentos...");
            ResponseEntity<Deparmet[]> responseEntityDeparment = restTemplate.getForEntity(url + "/departments", Deparmet[].class);
            Deparmet[] deparments = responseEntityDeparment.getBody();
            if (deparments.length > 0) {
                deparmetDao.deleteAll();
                for (int y = 0; y < deparments.length; y++) {
                    deparmetDao.insert(deparments[y]);
                }
            }
            System.out.println("Departamentos cargados-- " + deparmetDao.loadAll().size());
//load ivl catalogs
            System.out.println("Se inica la sincronización de catálogo de ivl");
            this.publishProgress(String.valueOf(25), "Descargando catálogos de ivl...");
            ResponseEntity<IvlCatalog[]> responseEntityivlCatalogs = restTemplate.getForEntity(url + "/ivlcatalog", IvlCatalog[].class);
            IvlCatalog[] ivlCatalogs = responseEntityivlCatalogs.getBody();
            if (ivlCatalogs.length > 0) {
                ivl_catalogDao.deleteAll();
                for (int y = 0; y < ivlCatalogs.length; y++) {
                    ivl_catalogDao.insert(ivlCatalogs[y]);
                }
            }
            System.out.println("Catálogos de ivl cargados-- " + ivl_catalogDao.loadAll().size());
//load ivl responsibles
            System.out.println("Se inica la sincronización de responsables de ivl");
            this.publishProgress(String.valueOf(30), "Descargando responsables de ivl...");
            ResponseEntity<IvlResposible[]> responseEntityivlResponsibles = restTemplate.getForEntity(url + "/ivlresponsibles", IvlResposible[].class);
            IvlResposible[] ivlResponsibleCatalogs = responseEntityivlResponsibles.getBody();
            System.out.println("Responsables de ivl recibidos-- " + ivlResponsibleCatalogs.length);
            if (ivlResponsibleCatalogs.length > 0) {
                ivl_resposibleDao.deleteAll();
                for (int y = 0; y < ivlResponsibleCatalogs.length; y++) {
                    ivl_resposibleDao.insert(ivlResponsibleCatalogs[y]);
                }
            }
            System.out.println("Responsables de ivl cargados-- " + ivl_resposibleDao.loadAll().size());
//load contractors
            System.out.println("Se inica la sincronización de contratistas");
            this.publishProgress(String.valueOf(40), "Descargando contratistas...");
            ResponseEntity<Contractor[]> responseEntityContractors = restTemplate.getForEntity(url + "/contractorcatalog", Contractor[].class);
            Contractor[] contractors = responseEntityContractors.getBody();
            if (contractors.length > 0) {
                contractorDao.deleteAll();
                for (int y = 0; y < contractors.length; y++) {
                    contractorDao.insert(contractors[y]);
                }
            }
            System.out.println("Contratistas  cargados-- " + contractorDao.loadAll().size());
            //load vehicles
            System.out.println("Se inica la sincronización de vehículos");
            this.publishProgress(String.valueOf(50), "Descargando vehículos...");
            ResponseEntity<Vehicle[]> responseEntityVehicles = restTemplate.getForEntity(url + "/findvehicles", Vehicle[].class);
            Vehicle[] vehicles = responseEntityVehicles.getBody();
            if (vehicles.length > 0) {
                vehicleDao.deleteAll();
                for (int y = 0; y < vehicles.length; y++) {
                    vehicleDao.insert(vehicles[y]);
                }
            }
            System.out.println("Vehículos cargados-- " + vehicleDao.loadAll().size());
            //load drivers
            System.out.println("Se inica la sincronización de conductores");
            this.publishProgress(String.valueOf(60), "Descargando conductores...");
            ResponseEntity<Driver[]> responseEntityDrivers = restTemplate.getForEntity(url + "/finddrivers", Driver[].class);
            Driver[] drivers = responseEntityDrivers.getBody();
            if (drivers.length > 0) {
                driverDao.deleteAll();
                for (int y = 0; y < drivers.length; y++) {
                    driverDao.insert(drivers[y]);
                }
            }
            System.out.println("Conductores cargados-- " + driverDao.loadAll().size());
            //load vehicledrivers
            System.out.println("Se inica la sincronización de vehículosconductres");
            this.publishProgress(String.valueOf(70), "Descargando vehículos asignados...");
            ResponseEntity<VehicleDriver[]> responseEntityVeDrivers = restTemplate.getForEntity(url + "/findvehicledrivers", VehicleDriver[].class);
            VehicleDriver[] vehicleDrivers = responseEntityVeDrivers.getBody();
            if (vehicleDrivers.length > 0) {
                vehicleDriverDao.deleteAll();
                for (int y = 0; y < vehicleDrivers.length; y++) {
                    vehicleDriverDao.insert(vehicleDrivers[y]);
                }
            }
            System.out.println("VehículosConductores cargados-- " + vehicleDriverDao.loadAll().size());
            //load Categpry incidents
            System.out.println("Se inica la sincronización de categorías incidente");
            this.publishProgress(String.valueOf(75), "Descargando cat incidentes...");
            ResponseEntity<SdCategoryIncident[]> responseEntityCatIncident = restTemplate.getForEntity(url + "/findcategoryincidents", SdCategoryIncident[].class);
            SdCategoryIncident[] sdCategoryIncidents = responseEntityCatIncident.getBody();
            if (sdCategoryIncidents.length > 0) {
                sdCategoryIncidentDao.deleteAll();
                for (int y = 0; y < sdCategoryIncidents.length; y++) {
                    sdCategoryIncidentDao.insert(sdCategoryIncidents[y]);
                }
            }
            System.out.println("Categorías incidentes cargados-- " + sdCategoryIncidentDao.loadAll().size());
            //load subcategoryincidents
            System.out.println("Se inica la sincronización de subcategorías incidente");
            this.publishProgress(String.valueOf(80), "Descargando subcat incidentes...");
            ResponseEntity<SdSubcategoryIncident[]> responseEntitySubCatIncident = restTemplate.getForEntity(url + "/findsubcategoryincidents", SdSubcategoryIncident[].class);
            SdSubcategoryIncident[] sdSubcategoryIncidents = responseEntitySubCatIncident.getBody();
            if (sdSubcategoryIncidents.length > 0) {
                sdSubcategoryIncidentDao.deleteAll();
                for (int y = 0; y < sdSubcategoryIncidents.length; y++) {
                    sdSubcategoryIncidentDao.insert(sdSubcategoryIncidents[y]);
                }
            }
            System.out.println("Subcategorías incidentes cargados-- " + sdSubcategoryIncidentDao.loadAll().size());

            //load risks
            System.out.println("Se inica la sincronización de risklist");
            this.publishProgress(String.valueOf(85), "Descargando riesgos...");
            ResponseEntity<Risk[]> responseEntityRisk = restTemplate.getForEntity(url + "/getallrisklist", Risk[].class);
            Risk[] risks = responseEntityRisk.getBody();
            if (risks.length > 0) {
                riskDao.deleteAll();
                for (int y = 0; y < risks.length; y++) {
                    riskDao.insert(risks[y]);
                }
            }
            System.out.println("Risklist cargados-- " + riskDao.loadAll().size());
            //load general audits
            System.out.println("Se inica la sincronización de inspecciones estándar");
            this.publishProgress(String.valueOf(86), "Descargando inspecciones estándar...");
            ResponseEntity<GeneralAudit[]> responseEntityGenralAudit = restTemplate.getForEntity(url + "/getgeneralaudits", GeneralAudit[].class);
            GeneralAudit[] generalAudits = responseEntityGenralAudit.getBody();
            if (generalAudits.length > 0) {
                generalAuditDao.deleteAll();
                for (int y = 0; y < generalAudits.length; y++) {
                    generalAuditDao.insert(generalAudits[y]);
                }
            }
            System.out.println("Inspecciones estándar cargados-- " + generalAuditDao.loadAll().size());

            //load general audits fields
            System.out.println("Se inica la sincronización de campos inspecciones estándar");
            this.publishProgress(String.valueOf(87), "Descargando campos ins estándar...");
            ResponseEntity<GeneralAuditField[]> responseEntityGenralAuditField = restTemplate.getForEntity(url + "/getgeneralauditfields", GeneralAuditField[].class);
            GeneralAuditField[] generalAuditFields = responseEntityGenralAuditField.getBody();
            if (generalAuditFields.length > 0) {
                generalAuditFieldDao.deleteAll();
                for (int y = 0; y < generalAuditFields.length; y++) {
                    generalAuditFieldDao.insert(generalAuditFields[y]);
                }
            }
            System.out.println("Campos de inspecciones estándar cargados-- " + generalAuditFieldDao.loadAll().size());
            //load general audits category
            System.out.println("Se inica la sincronización de categorias inspecciones estándar");
            this.publishProgress(String.valueOf(88), "Descargando categorías ins estándar...");
            ResponseEntity<GeneralAuditCategory[]> responseEntityGenralAuditCategory = restTemplate.getForEntity(url + "/getgenauditcategories", GeneralAuditCategory[].class);
            GeneralAuditCategory[] generalAuditcategories = responseEntityGenralAuditCategory.getBody();
            if (generalAuditcategories.length > 0) {
                generalAuditCategoryDao.deleteAll();
                for (int y = 0; y < generalAuditcategories.length; y++) {
                    generalAuditCategoryDao.insert(generalAuditcategories[y]);
                }
            }
            System.out.println("Categorias de inspecciones estándar cargados-- " + generalAuditCategoryDao.loadAll().size());

            //load general audits category
            System.out.println("Se inica la sincronización de Items inspecciones estándar");
            this.publishProgress(String.valueOf(89), "Descargando Items ins estándar...");
            ResponseEntity<GeneralAuditItem[]> responseEntityGenralAuditItem = restTemplate.getForEntity(url + "/getgenaudititems", GeneralAuditItem[].class);
            GeneralAuditItem[] generalAuditItems = responseEntityGenralAuditItem.getBody();
            if (generalAuditItems.length > 0) {
                generalAuditItemDao.deleteAll();
                for (int y = 0; y < generalAuditItems.length; y++) {
                    generalAuditItemDao.insert(generalAuditItems[y]);
                }
            }
            System.out.println("Items de inspecciones estándar cargados-- " + generalAuditItemDao.loadAll().size());


            this.publishProgress(String.valueOf(90), "Descarga Completa...");
            conn = true;
        } catch (Exception e) {
            Log.i("Error en la xync", "xxxxxxxxxxxxx");
            e.printStackTrace();
        }
       /* } else {
        }*/
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        int progress = Integer.parseInt(values[0]);
        String message = values[1];
        progDailog.setProgress(progress);
        progDailog.setMessage(message);
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        super.onPostExecute(aVoid);
        progDailog.dismiss();
        if (!conn) {
            AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
            goLogin.setMessage("Ocurrió un error al intentar conectarse con el servidor");
            goLogin.setCancelable(false);
            goLogin.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            AlertDialog alertLogin = goLogin.create();
            alertLogin.show();
        } else {
            AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
            goLogin.setMessage("Se descargó los datos del servidor correctamente!!");
            goLogin.setCancelable(false);
            goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                ((Activity) context).finish();
                context.startActivity(((Activity) context).getIntent());
                dialogInterface.cancel();
            });
            AlertDialog alertLogin = goLogin.create();
            alertLogin.show();
            Session.setSync(context, "true");
        }
    }
}
