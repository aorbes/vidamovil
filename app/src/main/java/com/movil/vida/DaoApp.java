package com.movil.vida;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.movil.vida.dao.DaoMaster;
import com.movil.vida.dao.DaoSession;

/**
 * Created by alex_ on 13/06/2017.
 */

public class DaoApp  extends Application {
    private DaoSession daoSession;
    /*
    static IncidentsDao postDao;
    static IvlDao ivlDao;
    static UserDao userDao;
    static DeparmetDao deparmetDao;
    static AreaDao areaDao;
    static ruleDao ruleDao;
    static  IncidentsDao incidentsDao;
    static IncidentImagesDao incidentImagesDao;
    static ContractorDao contractorDao;
    static IvlResposibleDao ivlResposibleDao;
    static IvlCatalogDao ivlCatalogDao;
    static IvlImagesDao ivlImagesDao;
    static IvlCatalogRegDao ivlCatalogRegDao;
    static AreaInspectionsDao areaInspectionsDao;
    static AreaInspectionActionDao areaInspectionActionDao;
    static AreaInsActionImagesDao areaInsActionImagesDao;
    static ExperientialPlanDao experientialPlanDao;
    static IvlActionDao ivlActionDao;
    static VehicleDao vehicleDao;
    static DriverDao driverDao;
    static VehicleDriverDao vehicleDriverDao;
    static SdCategoryIncidentDao sdCategoryIncidentDao;
    static SdSubcategoryIncidentDao sdSubcategoryIncidentDao;
    static RiskDao riskDao;
    static RiskIncidentDao riskIncidentDao;
    static GeneralAuditDao generalAuditDao;
    static GeneralAuditFieldDao generalAuditFieldDao;
    static GeneralAuditCategoryDao generalAuditCategoryDao;
    static GeneralAuditItemDao generalAuditItemDao;
    static AppPhotosDao appPhotosDao;
    static AuditFieldRegistryDao auditFieldRegistryDao;
    static AuditItemRegistryDao auditItemRegistryDao;
    static AuditRegistryDao auditRegistryDao;*/

/*
    public static void insertTestaData(SQLiteDatabase db) {
    db.execSQL("insert into AREA values(1,'Regla Asociada de prueba tesis1');");
    db.execSQL("insert into AREA values(2,'Regla Asociada de prueba tesis2');");
    db.execSQL("insert into AREA values(3,'Regla Asociada de prueba tesis3');");
    db.execSQL("insert into AREA values(4,'Regla Asociada de prueba tesis4');");
    db.execSQL("insert into AREA values(5,'Regla Asociada de prueba tesis5');");
    db.execSQL("insert into CONTRACTOR values(1,'Contratista de prueba tesis1','11212212');");
    db.execSQL("insert into CONTRACTOR values(2,'Contratista de prueba tesis2','11212212');");
    db.execSQL("insert into CONTRACTOR values(3,'Contratista de prueba tesis3','11212212');");
    db.execSQL("insert into CONTRACTOR values(4,'Contratista de prueba tesis4','11212212');");
    db.execSQL("insert into DEPARMET values(1,'Departamento de prueba tesis1');");
    db.execSQL("insert into DEPARMET values(2,'Departamento de prueba tesis2');");
    db.execSQL("insert into DEPARMET values(3,'Departamento de prueba tesis3');");
    db.execSQL("insert into DEPARMET values(4,'Departamento de prueba tesis4');");
    db.execSQL("insert into DEPARMET values(5,'Departamento de prueba tesis5');");
    db.execSQL("insert into IVL_CATALOG values(1,'Ivl Catalog de prueba tesis1','L');");
    db.execSQL("insert into IVL_CATALOG values(2,'Ivl Catalog de prueba tesis2','L');");
    db.execSQL("insert into IVL_CATALOG values(3,'Ivl Catalog de prueba tesis3','L');");
    db.execSQL("insert into IVL_CATALOG values(4,'Ivl Catalog de prueba tesis4','L');");
    db.execSQL("insert into IVL_CATALOG values(5,'Ivl Catalog de prueba tesis1','E');");
    db.execSQL("insert into IVL_CATALOG values(6,'Ivl Catalog de prueba tesis2','E');");
    db.execSQL("insert into IVL_CATALOG values(7,'Ivl Catalog de prueba tesis3','E');");
    db.execSQL("insert into IVL_CATALOG values(8,'Ivl Catalog de prueba tesis4','E');");

    db.execSQL("insert into IVL_RESPOSIBLE values(1,'Responsable IVL de prueba tesis1');");
    db.execSQL("insert into IVL_RESPOSIBLE values(2,'Responsable IVL de prueba tesis2');");
    db.execSQL("insert into IVL_RESPOSIBLE values(3,'Responsable IVL de prueba tesis3');");
    db.execSQL("insert into IVL_RESPOSIBLE values(4,'Responsable IVL de prueba tesis4');");

    db.execSQL("insert into RULE values(1,'Regla de prueba tesis1');");
    db.execSQL("insert into RULE values(2,'Regla de prueba tesis2');");
    db.execSQL("insert into RULE values(3,'Regla de prueba tesis3');");
    db.execSQL("insert into RULE values(4,'Regla de prueba tesis4');");

    db.execSQL("insert into USER values(1,'Usuario  de prueba tesis1','alex_251093@hotmail.com');");
    db.execSQL("insert into USER values(2,'Usuario  de prueba tesis2','alex_251093@hotmail.com');");
    db.execSQL("insert into USER values(3,'Usuario  de prueba tesis3','alex_251093@hotmail.com');");
    db.execSQL("insert into USER values(4,'Usuario  de prueba tesis4','alex_251093@hotmail.com');");


}*/

  /*  public static IncidentImagesDao getIncidentImagesDao() {
        return incidentImagesDao;
    }

    public static IncidentsDao getPostDao() {
        return postDao;
    }
    public  static IvlDao getIvlDao(){return ivlDao;}
    public static  UserDao getUserDao(){return  userDao;}
    public  static DeparmetDao getDeparmetDao(){return  deparmetDao;}
    public static  AreaDao getAreaDao(){return  areaDao;}
    public  static  ruleDao getRuleDao(){return  ruleDao;}
    public static  IncidentsDao getIncidentsDao(){return  incidentsDao;}

    public static IvlResposibleDao getIvlResposibleDao() {
        return ivlResposibleDao;
    }

    public static  ContractorDao getContractorDao(){return  contractorDao;}

    public static IvlCatalogDao getIvlCatalogDao() {
        return ivlCatalogDao;
    }

    public static IvlImagesDao getIvlImagesDao() {
        return ivlImagesDao;
    }

    public static IvlCatalogRegDao getIvlCatalogRegDao() {
        return ivlCatalogRegDao;
    }

    public static AreaInspectionsDao getAreaInspectionsDao() {
        return areaInspectionsDao;
    }

    public static AreaInspectionActionDao getAreaInspectionActionDao() {
        return areaInspectionActionDao;
    }

    public static AreaInsActionImagesDao getAreaInsActionImagesDao() {
        return areaInsActionImagesDao;
    }

    public static ExperientialPlanDao getExperientialPlanDao() {
        return experientialPlanDao;
    }

    public static IvlActionDao getIvlActionDao() {
        return ivlActionDao;
    }

    public static VehicleDao getVehicleDao() {
        return vehicleDao;
    }

    public static DriverDao getDriverDao() {
        return driverDao;
    }

    public static VehicleDriverDao getVehicleDriverDao() {
        return vehicleDriverDao;
    }

    public static SdCategoryIncidentDao getSdCategoryIncidentDao() {
        return sdCategoryIncidentDao;
    }

    public static SdSubcategoryIncidentDao getSdSubcategoryIncidentDao() {
        return sdSubcategoryIncidentDao;
    }

    public static RiskDao getRiskDao() {
        return riskDao;
    }

    public  static RiskIncidentDao getRiskIncidentDao(){
        return  riskIncidentDao;
    }

    public  static  GeneralAuditDao getGeneralAuditDao(){
        return  generalAuditDao;
    }
    public  static  GeneralAuditFieldDao getGeneralAuditFieldDao(){
        return  generalAuditFieldDao;
    }

    public static GeneralAuditCategoryDao getGeneralAuditCategoryDao() {
        return generalAuditCategoryDao;
    }

    public static AppPhotosDao getAppPhotosDao() {
        return appPhotosDao;
    }

    public static AuditFieldRegistryDao getAuditFieldRegistryDao() {
        return auditFieldRegistryDao;
    }

    public static AuditItemRegistryDao getAuditItemRegistryDao() {
        return auditItemRegistryDao;
    }

    public static AuditRegistryDao getAuditRegistryDao() {
        return auditRegistryDao;
    }

    public static GeneralAuditItemDao getGeneralAuditItemDao() {
        return generalAuditItemDao;
    }*/

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "com.movil.vida.db", null);
        SQLiteDatabase db = helper.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
       /* postDao = daoSession.getIncidentsDao();
        ivlDao = daoSession.getIvlDao();
        ivlActionDao = daoSession.getIvlActionDao();
        userDao = daoSession.getUserDao();
        deparmetDao = daoSession.getDeparmetDao();
        areaDao = daoSession.getAreaDao();

        ruleDao = daoSession.getRuleDao();
        incidentsDao = daoSession.getIncidentsDao();
        incidentImagesDao = daoSession.getIncidentImagesDao();
        contractorDao = daoSession.getContractorDao();
        ivlResposibleDao = daoSession.getIvlResposibleDao();
        ivlCatalogDao = daoSession.getIvlCatalogDao();
        ivlImagesDao = daoSession.getIvlImagesDao();
        ivlCatalogRegDao = daoSession.getIvlCatalogRegDao();
        areaInspectionsDao = daoSession.getAreaInspectionsDao();
        areaInspectionActionDao = daoSession.getAreaInspectionActionDao();
        areaInsActionImagesDao = daoSession.getAreaInsActionImagesDao();
        experientialPlanDao = daoSession.getExperientialPlanDao();
        vehicleDao = daoSession.getVehicleDao();
        driverDao = daoSession.getDriverDao();
        vehicleDriverDao = daoSession.getVehicleDriverDao();
        sdCategoryIncidentDao = daoSession.getSdCategoryIncidentDao();
        sdSubcategoryIncidentDao = daoSession.getSdSubcategoryIncidentDao();
        riskDao=daoSession.getRiskDao();
        riskIncidentDao=daoSession.getRiskIncidentDao();
        generalAuditDao=daoSession.getGeneralAuditDao();
        generalAuditFieldDao=daoSession.getGeneralAuditFieldDao();
        generalAuditCategoryDao=daoSession.getGeneralAuditCategoryDao();
        generalAuditItemDao=daoSession.getGeneralAuditItemDao();
        appPhotosDao=daoSession.getAppPhotosDao();
        auditFieldRegistryDao=daoSession.getAuditFieldRegistryDao();
        auditItemRegistryDao=daoSession.getAuditItemRegistryDao();
        auditRegistryDao=daoSession.getAuditRegistryDao();*/
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}
