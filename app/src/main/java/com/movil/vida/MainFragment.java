package com.movil.vida;


import android.accounts.Account;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IvlResposible;
import com.movil.vida.dao.IvlResposibleDao;

import experientialPlan.ExperientialPlanMenu;
import incidents.IncidentsMenuActivity;
import inspeccions.AddInspection.AreaInspectionsListActivity;
import inspeccionst.MenuInspectionStActivity;
import ivl.MenuIVLActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.example.android.datasync.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "example.com";
    // The account name
    public static final String ACCOUNT = "dummyaccount";

    // Instance fields
    Account mAccount;
    float rainfall[] = {98f, 554f, 5454f, 5454f, 57857f, 545f, 323f, 545f, 878f, 212f, 224f, 545f};
    String monthNames[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Spetiembre", "Octubre", "Noviembre", "Diciembre"};
    private BottomNavigationView mBottomNav;

    private CardView ivincidentCard, inspectionStandarCard;
    private CardView ivlCard, inspectionAreaCard, experientialPlanCard;
    private LinearLayout layoutScanQrCode, layoutGeneralAudit, layoutInspectionAreaStandar, layoutIncidentIvl, layoutExerientialPlan;
    private Button btnInitSync;
    IvlResposibleDao ivlResposibleDao;
    public MainFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        Log.i("Está sincronizado", Session.getSync(getContext()) + "");
        // Session.setSync(getContext(),"");
        if (Session.getSync(getContext()).isEmpty()) {
            view = inflater.inflate(R.layout.need_sync, container, false);
            // getActivity().setTitle("Home");
            btnInitSync = view.findViewById(R.id.btnInitSync);
            btnInitSync.setOnClickListener(view1 -> {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(getContext());
                goLogin.setTitle("Confirmación");
                goLogin.setMessage("Iniciar descarga de datos?");
                goLogin.setCancelable(true);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    new DownloadData(getContext()).execute();

                });
                goLogin.setNegativeButton("Cancelar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            });
        } else {
            view = inflater.inflate(R.layout.fragment_main, container, false);
            getActivity().setTitle("Home");


            ivincidentCard = view.findViewById(R.id.incidentCard);
            ivincidentCard.setOnClickListener(view1 -> {
                startActivity(new Intent(getContext(), IncidentsMenuActivity.class));
            });
            ivlCard = view.findViewById(R.id.ivlCard);
            ivlCard.setOnClickListener(view1 -> {
               /* DaoSession daoSession = ((DaoApp) getActivity().getApplicationContext()).getDaoSession();
                ivlResposibleDao=daoSession.getIvlResposibleDao();
                for(IvlResposible ivlResposible:ivlResposibleDao.loadAll()){
                    Log.i("Responsible",ivlResposible.getName()+"-- id--> "+ivlResposible.getId());
                }
                IvlResposible ivlResposible=ivlResposibleDao.queryBuilder().where(IvlResposibleDao.Properties.Id.eq(Session.getEmployeeId(getContext()))).unique();
                */        // Long.valueOf(Session.getEmployeeId(getContext())));
                //Log.i("Id employee",ivlResposible.getId()+"");
                //Log.i("Id employee",Session.getEmployeeId(getContext())+"");
               // Log.i("Responsible logued",Session.getUserName(getContext())+"");
                /*if(ivlResposible!=null&&ivlResposible.getId().equals(Session.getEmployeeId(getContext()))){*/
                    startActivity(new Intent(getContext(), MenuIVLActivity.class));
             /*   }else{
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage("Usted no está autorizado para registrar un IVL!!");
                dialog.setCancelable(false);
                dialog.setPositiveButton("Aceptar", (dialogInterface1, i1) -> {
                });
                AlertDialog alert = dialog.create();
                alert.show();
            }*/

            });
            inspectionAreaCard = view.findViewById(R.id.inspectionsAreaCard);
            inspectionAreaCard.setOnClickListener(view1 -> {
                startActivity(new Intent(getContext(), AreaInspectionsListActivity.class));
            });
            experientialPlanCard = view.findViewById(R.id.cardExperientialPlan);
            experientialPlanCard.setOnClickListener(view1 -> {
                startActivity(new Intent(getContext(), ExperientialPlanMenu.class));
            });
            inspectionStandarCard = view.findViewById(R.id.inspectionStandarCard);
            inspectionStandarCard.setOnClickListener(view1 -> {
                startActivity(new Intent(getContext(), MenuInspectionStActivity.class));
            });



      /*  pieChart = (PieChart) view.findViewById(R.id.piechart);
        pieChart.setElevation(10);
        pieChart.setCenterText("VIDA");
        Description description = new Description();
        description.setText("Resumen de Actividades");
        pieChart.setDescription(description);
        pieChart.setData(data);
        pieChart.invalidate(); // refresh*/
        }
        return view;
    }




   /*

    public class doLogin extends AsyncTask<Void, Void, Void> {
        Context context;
        String login, password, type;

        ProgressDialog progDailog;
        ResponseEntity<User> response;

        public doLogin(String login, String password, String type, Context context) {
            this.login = login;
            this.password = password;
            this.context = context;
            this.type = type;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDailog = new ProgressDialog(this.context);
            progDailog.setMessage("Iniciando Sesion...");
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            System.out.println("Test Login 1.3");
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters()
                    .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            JSONObject obj = new JSONObject();

            try {
                obj.put("login", login);
                obj.put("password", password);
                obj.put("devicekey", android_id);
                obj.put("devicename", deviceName);
                HttpEntity<String> httpEntity = new HttpEntity<String>(obj.toString(), headers);
                System.out.println("Esto se  va a enviar " + httpEntity.toString());
                response = restTemplate.exchange(url + "/login", HttpMethod.POST, httpEntity, User.class);
                //this.response = response.getBody();
                System.out.println("Esta es la respuesta entity " + response.getBody());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();
            if (this.response.getBody() != null) {
                Session.setUserName(context, this.response.getBody().getName(), this.response.getBody().getEmail());
                *//*Intent intent = new Intent(this.context, MainActivity.class);
                startActivity(intent);*//*
                updateSession();
            } else {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setTitle("Mensaje!!");
                goLogin.setMessage("Error al intentar iniciar sesión!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }

        }
    }*/
}
