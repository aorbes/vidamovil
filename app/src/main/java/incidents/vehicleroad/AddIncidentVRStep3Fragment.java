package incidents.vehicleroad;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Risk;
import com.movil.vida.dao.RiskDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.List;

import di.IncidentSingleton;


public class AddIncidentVRStep3Fragment extends Fragment implements Step, BlockingStep {
    private android.support.v7.widget.GridLayout gridLayoutInfractions;
    private RiskDao riskDao;
    private List<Integer> infractions = IncidentSingleton.getRiskInstance();

    public AddIncidentVRStep3Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_incident_vrstep3, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        riskDao = daoSession.getRiskDao();

        gridLayoutInfractions = view.findViewById(R.id.tabLayoutinfractions);
        gridLayoutInfractions.removeAllViews();

        List<Risk> riskList = riskDao.loadAll();
        int column = 2;
        int row = riskList.size();
        gridLayoutInfractions.setColumnCount(column);
        gridLayoutInfractions.setRowCount(row + 1);
        for (int i = 0, c = 0, r = 0; i < row; i++, c++) {
            if (c == column) {
                c = 0;
                r++;
            }
            boolean exist = false;
            for (int k = 0; k < infractions.size(); k++) {
                if (riskList.get(i).getId().intValue() == infractions.get(k).intValue()) {
                    exist = true;
                }
            }//boolean exist= leaderitems.stream().anyMatch(x->x.equals(listIvlCatalog.get(finalI).getId().intValue()));
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setId(riskList.get(i).getId().intValue());
            if (exist) {
                checkBox.setChecked(true);
            }
            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    infractions.add(compoundButton.getId());
                    Log.i("Item slected", compoundButton.getId() + "");
                } else {
                    int index = infractions.indexOf(compoundButton.getId());
                    infractions.remove(index);
                }
            });

            TextView textView = new TextView(getContext());
            textView.setText(riskList.get(i).getName());

            GridLayout.Spec rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            GridLayout.Spec colspan = GridLayout.spec(GridLayout.UNDEFINED, 1);
            if (r == 0 && c == 0) {
                colspan = GridLayout.spec(GridLayout.UNDEFINED, 2);
                rowSpan = GridLayout.spec(GridLayout.UNDEFINED, 2);
            }
            GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams(
                    rowSpan, colspan);
            gridLayoutInfractions.addView(checkBox, gridParam);
            gridLayoutInfractions.addView(textView, gridParam);

        }
        return view;
    }


    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        IncidentSingleton.setInfractions(infractions);
        callback.goToNextStep();
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

}
