package incidents.vehicleroad;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.AreaDao;
import com.movil.vida.dao.Contractor;
import com.movil.vida.dao.ContractorDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Deparmet;
import com.movil.vida.dao.DeparmetDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.areaSpinnerAdapter;
import adapters.contractorSpinnerAdapter;
import adapters.departmentSpinnerAdapter;
import adapters.ruleSpinnerAdapter;
import adapters.stringSpinnerAdapter;
import adapters.userSpinnerAdapter;
import di.IncidentSingleton;
import incidents.IncidentsMenuActivity;
import incidents.ListIncidentsActivity;


public class AddIncidentVRStep2Fragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, Step, BlockingStep {
    private EditText txtIncidentDateNM;
    private Spinner spIncidentPlace;
    private Spinner spType;
    private TextView lblspBodyPart;
    private Spinner spBodyPart;
    private TextView lblspInjuryType;
    private Spinner spInjuryType;
    private Spinner spReportBy;
    private TextView lblspReporterName;
    private Spinner spReporterName;
    private TextView lblOtherReporter;
    private EditText txtOtherReporter;
    private Spinner spReportDepartment;
    private Spinner spResponsibleDpartment;
    private Spinner spActivityType;
    private Spinner spInicentCause;
    private Spinner spInvolved;
    private TextView lblContractor;
    private Spinner spIncidentContractor;
    private TextView lbltxtVisitor;
    private EditText txtVisitor;
    private TextView lbltxtCostumer;
    private EditText txtCostumer;
    private Spinner spLocation;
    private Spinner spArea;
    private Spinner spRules;
    private EditText txtRelate;
    private ScrollView scrollViewAddIncident;
    private EditText txtPlace;
    private EditText txtInmediateAcion;
    private Switch propertyDamage;
    UserDao userDao;
    DeparmetDao deparmetDao;
    AreaDao areaDao;
    private EditText txtIncidentHourNM;
    com.movil.vida.dao.ruleDao ruleDao;
    Incidents incidents = IncidentSingleton.getInstance();
    private ContractorDao contractorDao;
    private MultiSpinner spEmailNotifications;
    private MultiSpinner.MultiSpinnerListener onSelectedListener;
    private String emails = "";

    public AddIncidentVRStep2Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_incident_vrstep2, container, false);
        DaoSession daoSession = ((DaoApp) getContext()).getDaoSession();
        userDao = daoSession.getUserDao();
        deparmetDao = daoSession.getDeparmetDao();
        areaDao = daoSession.getAreaDao();
        ruleDao = daoSession.getRuleDao();
        contractorDao = daoSession.getContractorDao();

        spIncidentPlace = view.findViewById(R.id.spIncidentPlace);
        List<String> iPlace = new ArrayList<>();
        iPlace.add("Unacem");
        iPlace.add("Cantyvol");
        stringSpinnerAdapter stringSpinnerAdapterIplace = new stringSpinnerAdapter(getContext(), iPlace);
        spIncidentPlace.setAdapter(stringSpinnerAdapterIplace);


        txtIncidentDateNM = view.findViewById(R.id.txtIncidentDateNM);
        txtIncidentDateNM.setOnClickListener(w -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddIncidentVRStep2Fragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setTitle("Selecciona la Fecha");
            datePickerDialog.show(getActivity().getFragmentManager(), "");
        });

        txtIncidentHourNM = view.findViewById(R.id.txtIncidentHourNM);
        txtIncidentHourNM.setOnClickListener(w -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(AddIncidentVRStep2Fragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog.setTitle("Selecciona la Hora");
            timePickerDialog.show(getActivity().getFragmentManager(), "");
        });
        spType = view.findViewById(R.id.spType);
        List<String> listType = new ArrayList<>();
        listType.add("Desviación");
        listType.add("Casi Accidente");
        listType.add("Primeros Auxilios");
        stringSpinnerAdapter adapatertype = new stringSpinnerAdapter(getContext(), listType);
        spType.setAdapter(adapatertype);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    String selected = parent.getItemAtPosition(position).toString();
                    if (selected.equals("Primeros Auxilios")) {
                        lblspBodyPart.setVisibility(View.VISIBLE);
                        spBodyPart.setVisibility(View.VISIBLE);
                        lblspInjuryType.setVisibility(View.VISIBLE);
                        spInjuryType.setVisibility(View.VISIBLE);
                    } else {
                        lblspBodyPart.setVisibility(View.GONE);
                        spBodyPart.setVisibility(View.GONE);
                        lblspInjuryType.setVisibility(View.GONE);
                        spInjuryType.setVisibility(View.GONE);
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lblspBodyPart = view.findViewById(R.id.lblspBodyPart);
        lblspBodyPart.setVisibility(View.GONE);
        spBodyPart = view.findViewById(R.id.spBodyPart);
        List<String> listBodyPart = new ArrayList<>();
        listBodyPart.add("Ninguna");
        listBodyPart.add("Brazo");
        listBodyPart.add("Cabeza");
        listBodyPart.add("Cara");
        listBodyPart.add("Cuello");
        listBodyPart.add("Dedo");
        listBodyPart.add("Espalda");
        listBodyPart.add("Labio");
        listBodyPart.add("Oídos");
        listBodyPart.add("Ojos");
        listBodyPart.add("Oreja");
        listBodyPart.add("Pie");
        listBodyPart.add("Pierna");
        listBodyPart.add("Torso");
        stringSpinnerAdapter adapterBodyPart = new stringSpinnerAdapter(getContext(), listBodyPart);
        spBodyPart.setAdapter(adapterBodyPart);
        spBodyPart.setVisibility(View.GONE);

        lblspInjuryType = view.findViewById(R.id.lblspInjuryType);
        lblspInjuryType.setVisibility(View.GONE);
        spInjuryType = view.findViewById(R.id.spInjuryType);
        List<String> listInjuryType = new ArrayList<>();
        listInjuryType.add("Ninguna");
        listInjuryType.add("Atrapamiento");
        listInjuryType.add("Corte");
        listInjuryType.add("Golpe");
        listInjuryType.add("Irritación");
        listInjuryType.add("Quemadura Física");
        listInjuryType.add("Quemadura Química");
        stringSpinnerAdapter adapterInjuryType = new stringSpinnerAdapter(getContext(), listInjuryType);
        spInjuryType.setAdapter(adapterInjuryType);
        spInjuryType.setVisibility(View.GONE);

        spReportBy = view.findViewById(R.id.spReportBy);
        List<String> listReporBy = new ArrayList<>();
        listReporBy.add("Empleado de Unacem");
        listReporBy.add("Otro");
        stringSpinnerAdapter adapterReporBy = new stringSpinnerAdapter(getContext(), listReporBy);
        spReportBy.setAdapter(adapterReporBy);
        spReportBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    String selected = parent.getItemAtPosition(position).toString();
                    if (selected.equals("Empleado de Unacem")) {
                        lblspReporterName.setVisibility(View.VISIBLE);
                        spReporterName.setVisibility(View.VISIBLE);
                        lblOtherReporter.setVisibility(View.GONE);
                        txtOtherReporter.setVisibility(View.GONE);
                    } else if (selected.equals("Otro")) {
                        lblspReporterName.setVisibility(View.GONE);
                        spReporterName.setVisibility(View.GONE);
                        lblOtherReporter.setVisibility(View.VISIBLE);
                        txtOtherReporter.setVisibility(View.VISIBLE);
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lblOtherReporter = view.findViewById(R.id.lbltxtOtherReporter);
        lblOtherReporter.setVisibility(View.GONE);
        txtOtherReporter = view.findViewById(R.id.txtOtherReporter);
        txtOtherReporter.setVisibility(View.GONE);

        lblspReporterName = view.findViewById(R.id.lblspReporterName);
        spReporterName = view.findViewById(R.id.spReporterName);
        final List<User> listUser = userDao.loadAll();

        // listUser.add(new User(9999l, "Seleccione un usuario", "select@email.com"));
        Collections.sort(listUser, (user1, user2) -> {
            return user1.getName().compareTo(user2.getName());
        });

        final userSpinnerAdapter userSpinnerAdapter = new userSpinnerAdapter(getContext(), listUser);
        spReporterName.post(new Runnable() {

            @Override
            public void run() {
                int indexUser = -1;
                for (int i = 0; i < listUser.size(); i++) {
                    if (listUser.get(i).getName().equals(Session.getUserName(getContext()))) {
                        indexUser = i;
                        break;
                    }
                }
                if (indexUser > 0) {
                    spReporterName.setSelection(indexUser);
                }
              /*  TextView errorText = (TextView) customSpinner.getSelectedView();
                errorText.setError("anything here, just to add the icon");*/
            }
        });
        spReporterName.setAdapter(userSpinnerAdapter);
       /* spReporterName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    User item = (User) parent.getItemAtPosition(position);
                    incidents.setReportPerson(item.getName());
                    // Toast.makeText(parent.getContext(), "Android Custom Spinner Example Output..." + item.getName(), Toast.LENGTH_LONG).show();
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

       /* List<String> users=new ArrayList<>();
        for (User user:userDao.loadAll()) {
            users.add(user.getName());
        }
        Collections.sort(users);
        ArrayAdapter<CharSequence> adapterReporterName = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_dropdown_item);
        adapterReporterName.addAll(users);
        spReporterName.setAdapter(adapterReporterName);*/
        //spReporterName.setVisibility(View.GONE);


        spActivityType = view.findViewById(R.id.spActivityType);
        List<String> listActivityType = new ArrayList<>();
        listActivityType.add("Trabajo Rutinario");
        listActivityType.add("Trabajo Programado");
        listActivityType.add("Trabajo Mayor (Paradas)");
        listActivityType.add("Trabajo Emergente");
        listActivityType.add("Proyecto");
        listActivityType.add("Sitio de Clientes");
        stringSpinnerAdapter adapterActivityTye = new stringSpinnerAdapter(getContext(), listActivityType);
        spActivityType.setAdapter(adapterActivityTye);


        spInicentCause = view.findViewById(R.id.spCause);
        List<String> listCause = new ArrayList<>();
        listCause.add("Acto Inseguro");
        listCause.add("Condición Insegura");
        stringSpinnerAdapter adapterCause = new stringSpinnerAdapter(getContext(), listCause);
        spInicentCause.setAdapter(adapterCause);


        spInvolved = view.findViewById(R.id.spInvolved);
        List<String> listInvolved = new ArrayList<>();
        listInvolved.add("No Definido");
        listInvolved.add("Unacem");
        listInvolved.add("Contratista");
        listInvolved.add("Visitante");
        listInvolved.add("Clientes");
        stringSpinnerAdapter adapterInvolved = new stringSpinnerAdapter(getContext(), listInvolved);
        spInvolved.setAdapter(adapterInvolved);
        spInvolved.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    String selected = parent.getItemAtPosition(position).toString();
                    if (selected.equals("No Definido") || (selected.equals("Unacem"))) {
                        lblContractor.setVisibility(View.GONE);
                        spIncidentContractor.setVisibility(View.GONE);
                        lbltxtVisitor.setVisibility(View.GONE);
                        txtVisitor.setVisibility(View.GONE);
                        lbltxtCostumer.setVisibility(View.GONE);
                        txtCostumer.setVisibility(View.GONE);
                        txtCostumer.setText(null);
                        txtVisitor.setText(null);
                    } else if (selected.equals("Contratista")) {
                        lblContractor.setVisibility(View.VISIBLE);
                        spIncidentContractor.setVisibility(View.VISIBLE);
                        lbltxtVisitor.setVisibility(View.GONE);
                        txtVisitor.setVisibility(View.GONE);
                        lbltxtCostumer.setVisibility(View.GONE);
                        txtCostumer.setVisibility(View.GONE);
                        txtCostumer.setText(null);
                        txtVisitor.setText(null);
                    } else if (selected.equals("Visitante")) {
                        lblContractor.setVisibility(View.GONE);
                        spIncidentContractor.setVisibility(View.GONE);
                        lbltxtVisitor.setVisibility(View.VISIBLE);
                        txtVisitor.setVisibility(View.VISIBLE);
                        lbltxtCostumer.setVisibility(View.GONE);
                        txtCostumer.setVisibility(View.GONE);
                        txtCostumer.setText(null);

                    } else if (selected.equals("Clientes")) {
                        lblContractor.setVisibility(View.GONE);
                        spIncidentContractor.setVisibility(View.GONE);
                        lbltxtVisitor.setVisibility(View.GONE);
                        txtVisitor.setVisibility(View.GONE);
                        lbltxtCostumer.setVisibility(View.VISIBLE);
                        txtCostumer.setVisibility(View.VISIBLE);
                        txtVisitor.setText(null);
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        lblContractor = view.findViewById(R.id.lblspIncidentContractor);
        lblContractor.setVisibility(View.GONE);
        spIncidentContractor = view.findViewById(R.id.spIncidentContractor);
        final List<Contractor> contractorList = contractorDao.loadAll();
        contractorList.add(new Contractor(9999L, "Seleccione el Contratista", "99999999"));
        spIncidentContractor.post(new Runnable() {
            @Override
            public void run() {
                spIncidentContractor.setSelection(contractorList.size() - 1);
            }
        });
        contractorSpinnerAdapter adapterContractor = new contractorSpinnerAdapter(getContext(), contractorList);
        spIncidentContractor.setAdapter(adapterContractor);
        spIncidentContractor.setVisibility(View.GONE);
 /*       spIncidentContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (count >= 1) {
                    Contractor contractor = (Contractor) parent.getSelectedItem();
                    if (!contractor.getName().equals("Seleccione el Contratista")) {
                        incidents.setContractor_id(contractor.getId());
                    }
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        spReportDepartment = view.findViewById(R.id.spReportDepartment);
        departmentSpinnerAdapter adapterReporterDeparment = new departmentSpinnerAdapter(getContext(), deparmetDao.loadAll());
        spReportDepartment.setAdapter(adapterReporterDeparment);

        spResponsibleDpartment = view.findViewById(R.id.spResponsibleDepartment);
        spResponsibleDpartment.setAdapter(adapterReporterDeparment);

        spLocation = view.findViewById(R.id.spLocation);
        List<String> listLocation = new ArrayList<>();
        listLocation.add("Planta");
        listLocation.add("Oficinas Quito");
        listLocation.add("Canteras");
        listLocation.add("Otros");
        stringSpinnerAdapter adapterLocation = new stringSpinnerAdapter(getContext(), listLocation);
        spLocation.setAdapter(adapterLocation);

        spArea = view.findViewById(R.id.spArea);
        areaSpinnerAdapter adapterArea = new areaSpinnerAdapter(getContext(), areaDao.loadAll());
        spArea.setAdapter(adapterArea);
        //spArea.setSelection(areaDao.loadAll().indexOf("No Definido") + 1);


        spRules = view.findViewById(R.id.spAssocRules);
        ruleSpinnerAdapter adapterRules = new ruleSpinnerAdapter(getContext(), ruleDao.loadAll());
        spRules.setAdapter(adapterRules);
        //spRules.setSelection(rules.indexOf("No definido") + 1);


        lbltxtVisitor = view.findViewById(R.id.lbltxtVisitor);
        txtVisitor = view.findViewById(R.id.txtVisitor);
        lbltxtVisitor.setVisibility(View.GONE);
        txtVisitor.setVisibility(View.GONE);


        lbltxtCostumer = view.findViewById(R.id.lbltxtCostumer);
        txtCostumer = view.findViewById(R.id.txtCostumer);
        lbltxtCostumer.setVisibility(View.GONE);
        txtCostumer.setVisibility(View.GONE);


        txtRelate = view.findViewById(R.id.txtRelate);
        propertyDamage = view.findViewById(R.id.swPropetyDamage);
        txtPlace = view.findViewById(R.id.txtPlace);
        txtInmediateAcion = view.findViewById(R.id.txtInmediateAction);


        scrollViewAddIncident = view.findViewById(R.id.scrollViewAddIncident);

        spEmailNotifications = view.findViewById(R.id.spEmailNotifications);

        final ArrayAdapter<User> adapterUser = new ArrayAdapter<User>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listUser);


        onSelectedListener = new MultiSpinner.MultiSpinnerListener() {
            public void onItemsSelected(boolean[] selected) {
                emails = "";
                for (int i = 0; i < selected.length; i++) {
                    if (selected[i]) {
                        emails += adapterUser.getItem(i).getId() + ",";
                    }
                }
            }

        };
        spEmailNotifications.setAdapter(adapterUser, false, onSelectedListener);

        //If args

        if (incidents.getId() != null) {
            spIncidentPlace.setSelection(stringSpinnerAdapterIplace.getPosition(incidents.getMainPlace()));
            txtIncidentDateNM.setText(incidents.getReporterDate().split(" ")[0]);
            txtIncidentHourNM.setText(incidents.getReporterDate().split(" ")[1]);
            spType.setSelection(adapatertype.getPosition(incidents.getIncidentType()));
            spReportBy.setSelection(adapterReporBy.getPosition(incidents.getReportType()));
            if (incidents.getReportType().equals("Empleado de Unacem")) {
                // spReporterName.setSelection(adapterReporterName.getPosition(incidents.getReportPerson())+1);
            } else {
                txtOtherReporter.setText(incidents.getReportPerson());
            }

            spActivityType.setSelection(adapterActivityTye.getPosition(incidents.getActivityType()));
            spInicentCause.setSelection(adapterCause.getPosition(incidents.getCause()));
            spInvolved.setSelection(adapterInvolved.getPosition(incidents.getInvolvedType()));
            //spReportDepartment.setSelection(adapterReporterDeparment.getPosition(incidents.getReportDepartmentName()) + 1);
            // spResponsibleDpartment.setSelection(adapterReporterDeparment.getPosition(incidents.getResponsibleDepartmentName()) + 1);
            spLocation.setSelection(adapterLocation.getPosition(incidents.getUbicationType()));
            // spArea.setSelection(adapterArea.getPosition(incidents.getAreaName()) + 1);
            txtRelate.setText(incidents.getRelate());
            txtPlace.setText(incidents.getSpecificArea());
            if (incidents.getPropertyDamages().equals("Y")) {
                propertyDamage.setChecked(true);
            } else {
                propertyDamage.setChecked(false);
            }
            //  spRules.setSelection(adapterRules.getPosition(incidents.getRelateType()) + 1);
            txtInmediateAcion.setText(incidents.getActionName());
            boolean[] selectedItems = new boolean[adapterUser.getCount()];
            if (!incidents.getEmails().equals("")) {
                System.out.println("Hay emails");
                String[] listemails = incidents.getEmails().split(",");
                System.out.println("Número de emailss " + listemails.length);
                for (int i = 0; i < listemails.length; i++) {
                    Long idu = Long.parseLong(listemails[i]);
                    System.out.println("El id del usuario " + idu);
                    User user = userDao.load(idu);
                    int pos = adapterUser.getPosition(user);
                    System.out.println("Posición del usuario " + pos);
                    selectedItems[pos] = true;
                }
                spEmailNotifications.setSelected(selectedItems);
            }
            System.out.println("Test Edit Incident 1.0");
        }
        return view;
    }

    public boolean Validate() {
        if (spIncidentPlace.getSelectedItem().equals("Seleccione el Lugar del Incidente")) {
            // spIncidentPlace.setError("Este campo es obligatorio");
            spIncidentPlace.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spIncidentPlace.getTop() - 150);
                }
            });
            return false;
        }
        if (txtIncidentDateNM.getText().toString().isEmpty() || txtIncidentDateNM.getText().toString().contains(getString(R.string.error_field_required))) {
            txtIncidentDateNM.requestFocus();
            txtIncidentDateNM.setError(getString(R.string.error_field_required));
            txtIncidentDateNM.setTextColor(Color.RED);
            txtIncidentDateNM.setText(getString(R.string.error_field_required));
            scrollViewAddIncident.post(() -> {
                scrollViewAddIncident.smoothScrollTo(0, txtIncidentDateNM.getTop());
            });
            return false;
        }
        if (txtIncidentHourNM.getText().toString().isEmpty() || txtIncidentHourNM.getText().toString().contains(getString(R.string.error_field_required))) {
            txtIncidentHourNM.requestFocus();
            txtIncidentHourNM.setError(getString(R.string.error_field_required));
            txtIncidentHourNM.setTextColor(Color.RED);
            txtIncidentHourNM.setText(getString(R.string.error_field_required));
            scrollViewAddIncident.post(() -> {
                scrollViewAddIncident.smoothScrollTo(0, txtIncidentHourNM.getTop());
            });
            return false;
        }
        if (spType.getSelectedItem().equals("Seleccionar el Tipo de Incidente")) {
            // spType.setError("Este campo es obligatorio");
            spType.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spType.getTop() - 150);
                }
            });
            return false;
        }
        if (spReportBy.getSelectedItem().equals("Seleccione quién lo Reporta")) {
            spReportBy.requestFocus();
            //  spReportBy.setError("Este campo es obligatorio");
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spReportBy.getTop() - 150);
                }
            });
            return false;
        }
        if (spReporterName.getSelectedItem().equals("Seleccione el Nombre")) {
            spReporterName.requestFocus();
            // spReporterName.setError("Este campo es obligatorio");
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spReporterName.getTop() - 150);
                }
            });
            return false;
        }
        if (spActivityType.getSelectedItem().equals("Seleccione el Tipo de Actividad")) {
            spActivityType.requestFocus();
            scrollViewAddIncident.post(() -> {


                scrollViewAddIncident.smoothScrollTo(0, spActivityType.getTop() - 150);

            });
            return false;
        }
        if (spInicentCause.getSelectedItem().equals("Seleccione la Causa")) {
            spInicentCause.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spInicentCause.getTop() - 150);
                }
            });
            return false;
        }
        if (spInvolved.getSelectedItem().equals("eleccione el Involucrado")) {
            spInvolved.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spInvolved.getTop() - 150);
                }
            });
            return false;
        }
        if (spReportDepartment.getSelectedItem().equals("Seleccione el Departamento que Reporta")) {
            spReportDepartment.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spReporterName.getTop() - 150);
                }
            });
            return false;
        }
        if (spResponsibleDpartment.getSelectedItem().equals("Seleccione el Departamento responsable")) {
            spResponsibleDpartment.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spResponsibleDpartment.getTop() - 150);
                }
            });
            return false;
        }
        if (spLocation.getSelectedItem().equals("Seleccione la Ubicación")) {
            spLocation.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spLocation.getTop() - 150);
                }
            });
            return false;
        }
        if (spArea.getSelectedItem().equals("Seleccione el Área")) {
            spArea.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spArea.getTop() - 150);
                }
            });
            return false;
        }
        if (txtRelate.getText().toString().equals("")) {
            txtRelate.requestFocus();
            txtRelate.setError("Este campo es obligatorio");
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, txtRelate.getTop() - 150);
                }
            });
            return false;
        }
        if (txtPlace.getText().toString().equals("")) {
            txtPlace.requestFocus();
            txtPlace.setError("Este campo es obligatorio");
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, txtPlace.getTop() - 150);
                }
            });
            return false;
        }
        if (spRules.getSelectedItem().equals("Seleccione una Regla Asociada")) {
            spRules.requestFocus();
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, spRules.getTop() - 150);
                }
            });
            return false;
        }
        if (txtInmediateAcion.getText().toString().equals("")) {
            txtInmediateAcion.requestFocus();
            txtInmediateAcion.setError("Este campo es obligatorio");
            scrollViewAddIncident.post(new Runnable() {
                @Override
                public void run() {
                    scrollViewAddIncident.smoothScrollTo(0, txtInmediateAcion.getTop() - 150);
                }
            });
            return false;
        }

        return true;
    }

    private void addIncident() {

        incidents.setMainPlace(spIncidentPlace.getSelectedItem().toString());
        incidents.setReporterDate(txtIncidentDateNM.getText().toString() + " " + txtIncidentHourNM.getText().toString());
        incidents.setIncidentType(spType.getSelectedItem().toString());
        incidents.setReportType(spReportBy.getSelectedItem().toString());
        if (spReportBy.getSelectedItem().toString().equals("Empleado de Unacem")) {
            incidents.setReportPerson(((User) spReporterName.getSelectedItem()).getName());
        } else {
            incidents.setReportPerson(txtOtherReporter.getText().toString());
        }
        incidents.setActivityType(spActivityType.getSelectedItem().toString());
        incidents.setCause(spInicentCause.getSelectedItem().toString());
        incidents.setInvolvedType(spInvolved.getSelectedItem().toString());

        if (spInvolved.getSelectedItem().toString().equals("Contratista")) {
            incidents.setContractorId(((Contractor) spIncidentContractor.getSelectedItem()).getId());
        }
        if (!txtCostumer.getText().toString().equals("")) {
            incidents.setInvolvedName(txtCostumer.getText().toString());
        }
        if (!txtVisitor.getText().toString().equals("")) {
            incidents.setInvolvedName(txtVisitor.getText().toString());
        }
        incidents.setReportDepartmentName(((Deparmet) spReportDepartment.getSelectedItem()).getName());
        incidents.setResponsibleDepartmentName(((Deparmet) spResponsibleDpartment.getSelectedItem()).getName());
        incidents.setUbicationType(spLocation.getSelectedItem().toString());
        incidents.setAreaName(spArea.getSelectedItem().toString());
        incidents.setRelate(txtRelate.getText().toString());
        incidents.setSpecificArea(txtPlace.getText().toString());

        if (propertyDamage.isChecked()) {
            incidents.setPropertyDamages("Y");
        } else {
            incidents.setPropertyDamages("N");
        }
        incidents.setRelateType(spRules.getSelectedItem().toString());
        incidents.setActionName(txtInmediateAcion.getText().toString());
        incidents.setEmails(emails);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtIncidentDateNM.setTextColor(Color.BLACK);
        txtIncidentDateNM.setError(null);
        txtIncidentDateNM.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hour = "";
        String min = "";
        if (hourOfDay < 10) {
            hour = "0";
        }
        if (minute < 10) {
            min = "0";
        }
        txtIncidentHourNM.setTextColor(Color.BLACK);
        txtIncidentHourNM.setError(null);
        txtIncidentHourNM.setText(hour + hourOfDay + ":" + min + minute);
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (Validate()) {
            try {
                addIncident();
                callback.goToNextStep();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        IncidentSingleton.setIncidents(incidents);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        IncidentSingleton.empyIncident();
        if (incidents.getId() != null) {
            startActivity(new Intent(getContext(), ListIncidentsActivity.class));
        } else {
            startActivity(new Intent(getContext(), IncidentsMenuActivity.class));
        }
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
