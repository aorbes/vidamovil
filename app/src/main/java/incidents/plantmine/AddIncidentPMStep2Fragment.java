package incidents.plantmine;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.Area;
import com.movil.vida.dao.AreaDao;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Deparmet;
import com.movil.vida.dao.DeparmetDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.SdCategoryIncident;
import com.movil.vida.dao.SdCategoryIncidentDao;
import com.movil.vida.dao.SdSubcategoryIncident;
import com.movil.vida.dao.SdSubcategoryIncidentDao;
import com.movil.vida.dao.User;
import com.movil.vida.dao.UserDao;
import com.movil.vida.dao.rule;
import com.movil.vida.dao.ruleDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.thomashaertel.widget.MultiSpinner;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import adapters.areaSpinnerAdapter;
import adapters.departmentSpinnerAdapter;
import adapters.ruleSpinnerAdapter;
import adapters.sdCategoryIncidentSpinnerAdapter;
import adapters.sdSubcategoryIncidentSpinnerAdapter;
import adapters.stringSpinnerAdapter;
import adapters.userSpinnerAdapter;
import di.IncidentSingleton;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddIncidentPMStep2Fragment extends Fragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, Step, BlockingStep {


    UserDao userDao;
    DeparmetDao deparmetDao;
    AreaDao areaDao;
    ruleDao ruleDao;
    SdCategoryIncidentDao sdCategoryIncidentDao;
    SdSubcategoryIncidentDao sdSubcategoryIncidentDao;
    sdSubcategoryIncidentSpinnerAdapter sdSubcategoryIncidentSpinnerAdapter;
    Incidents incidents = IncidentSingleton.getInstance();
    private Spinner spIncidentPMPlace;
    private EditText txtIncidentPMDate;
    private EditText txtIncidentPMHour;
    private Spinner spIncidentPMType;
    private TextView lblIncidentPMBodyPart;
    private TextView lblIncidentPMInjuryType;
    private Spinner spIncidentPMInjury;
    private Spinner spIncidentPMBodyPart;
    private TextView lblIncidentPMReportName1;
    private Spinner spIncidentPMReportBy;
    private Spinner spIncidentPMReportName;
    private TextView lblIncidentPMReportName;
    private EditText txtIncidentPMReportName;
    private Spinner spIncidentPMLogisticPerson;
    private Spinner spIncidentPMAcType;
    private Spinner spIncidentPMCause;
    private Spinner spIncidentPMInvloved;
    private TextView lblIncidentnPMVisitor;
    private EditText txtIncidentPMVisitor;
    private TextView lblIncidentPMCustomer;
    private EditText txtIncidentPMCustomer;
    private Spinner spIncidentPMCategoy;
    private Spinner spIncidentPMIncidence;
    private Spinner spIncidentPMDepartment;
    private Spinner spIncidentPMDepResp;
    private Spinner spIncidentPMUbication;
    private Spinner spIncidentPMArea;
    private EditText txtIncidentPMSpecPlace;
    private Spinner spIncidentPMRule;
    private EditText txtIncidentPMRelate;
    private EditText txtIncidentPMAction;
    private MultiSpinner spIncidentPMNotifications;
    private MultiSpinner.MultiSpinnerListener onSelectedListener;
    private String emails = "";
    private ScrollView scrollView;

    public AddIncidentPMStep2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_incident_pmstep2, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        userDao = daoSession.getUserDao();
        deparmetDao = daoSession.getDeparmetDao();
        areaDao = daoSession.getAreaDao();
        ruleDao = daoSession.getRuleDao();
        sdCategoryIncidentDao = daoSession.getSdCategoryIncidentDao();
        sdSubcategoryIncidentDao = daoSession.getSdSubcategoryIncidentDao();


        spIncidentPMPlace = v.findViewById(R.id.spIncidentPMPlace);
        List<String> places = new ArrayList<>();
        places.add("Unacem");
        places.add("Cantyvol");
        stringSpinnerAdapter spinnerAdapterPLaces = new stringSpinnerAdapter(getContext(), places);
        spIncidentPMPlace.setAdapter(spinnerAdapterPLaces);

        txtIncidentPMDate = v.findViewById(R.id.txtIncidentPMDate);
        txtIncidentPMDate.setOnClickListener(w -> {
            Calendar now = Calendar.getInstance();
            DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(AddIncidentPMStep2Fragment.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.setTitle("Selecciona la Fecha");
            datePickerDialog.show(getActivity().getFragmentManager(), "");
        });

        txtIncidentPMHour = v.findViewById(R.id.txtIncidentPMHour);
        txtIncidentPMHour.setOnClickListener(w -> {
            Calendar now = Calendar.getInstance();
            TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(AddIncidentPMStep2Fragment.this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE), true);
            timePickerDialog.setTitle("Selecciona la Hora");
            timePickerDialog.show(getActivity().getFragmentManager(), "");
        });

        spIncidentPMType = v.findViewById(R.id.spIncidentPMType);
        List<String> listType = new ArrayList<>();
        listType.add("Desviación");
        listType.add("Casi Accidente");
        listType.add("Primeros Auxilios");
        stringSpinnerAdapter adapatertype = new stringSpinnerAdapter(getContext(), listType);
        spIncidentPMType.setAdapter(adapatertype);
        spIncidentPMType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (count > 0 && adapterView.getItemAtPosition(i).equals("Primeros Auxilios")) {
                    lblIncidentPMBodyPart.setVisibility(View.VISIBLE);
                    spIncidentPMBodyPart.setVisibility(View.VISIBLE);
                    lblIncidentPMInjuryType.setVisibility(View.VISIBLE);
                    spIncidentPMInjury.setVisibility(View.VISIBLE);

                } else {
                    lblIncidentPMBodyPart.setVisibility(View.GONE);
                    spIncidentPMBodyPart.setVisibility(View.GONE);
                    spIncidentPMBodyPart.setSelected(false);
                    lblIncidentPMInjuryType.setVisibility(View.GONE);
                    spIncidentPMInjury.setVisibility(View.GONE);
                    spIncidentPMInjury.setSelected(false);
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lblIncidentPMBodyPart = v.findViewById(R.id.lblIncidentPMBodyPart);
        lblIncidentPMBodyPart.setVisibility(View.GONE);
        spIncidentPMBodyPart = v.findViewById(R.id.spIncidentPMBodyPart);
        List<String> listBodyPart = new ArrayList<>();
        listBodyPart.add("Ninguna");
        listBodyPart.add("Brazo");
        listBodyPart.add("Cabeza");
        listBodyPart.add("Cara");
        listBodyPart.add("Cuello");
        listBodyPart.add("Dedo");
        listBodyPart.add("Espalda");
        listBodyPart.add("Labio");
        listBodyPart.add("Oídos");
        listBodyPart.add("Ojos");
        listBodyPart.add("Oreja");
        listBodyPart.add("Pie");
        listBodyPart.add("Pierna");
        listBodyPart.add("Torso");
        stringSpinnerAdapter adapterBodyPart = new stringSpinnerAdapter(getContext(), listBodyPart);
        spIncidentPMBodyPart.setAdapter(adapterBodyPart);
        spIncidentPMBodyPart.setVisibility(View.GONE);

        lblIncidentPMInjuryType = v.findViewById(R.id.lblIncidentPMInjuryType);
        lblIncidentPMInjuryType.setVisibility(View.GONE);
        spIncidentPMInjury = v.findViewById(R.id.spIncidentPMInjury);
        List<String> listInjuryType = new ArrayList<>();
        listInjuryType.add("Ninguna");
        listInjuryType.add("Atrapamiento");
        listInjuryType.add("Corte");
        listInjuryType.add("Golpe");
        listInjuryType.add("Irritación");
        listInjuryType.add("Quemadura Física");
        listInjuryType.add("Quemadura Química");
        stringSpinnerAdapter adapterInjuryType = new stringSpinnerAdapter(getContext(), listInjuryType);
        spIncidentPMInjury.setAdapter(adapterInjuryType);
        spIncidentPMInjury.setVisibility(View.GONE);


        spIncidentPMReportBy = v.findViewById(R.id.spIncidentPMReportBy);
        List<String> listReporBy = new ArrayList<>();
        listReporBy.add("Empleado de Unacem");
        listReporBy.add("Otro");
        stringSpinnerAdapter adapterReporBy = new stringSpinnerAdapter(getContext(), listReporBy);
        spIncidentPMReportBy.setAdapter(adapterReporBy);
        spIncidentPMReportBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (count > 0 && adapterView.getItemAtPosition(i).equals("Otro")) {
                    lblIncidentPMReportName.setVisibility(View.VISIBLE);
                    txtIncidentPMReportName.setVisibility(View.VISIBLE);
                    lblIncidentPMReportName1.setVisibility(View.GONE);
                    spIncidentPMReportName.setVisibility(View.GONE);
                } else {
                    lblIncidentPMReportName.setVisibility(View.GONE);
                    txtIncidentPMReportName.setVisibility(View.GONE);
                    txtIncidentPMReportName.setText("");
                    lblIncidentPMReportName1.setVisibility(View.VISIBLE);
                    spIncidentPMReportName.setVisibility(View.VISIBLE);
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        lblIncidentPMReportName1 = v.findViewById(R.id.lblIncidentPMReportName1);
        spIncidentPMReportName = v.findViewById(R.id.spIncidentPMReportName);
        final List<User> listUser = userDao.loadAll();
        Collections.sort(listUser, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        userSpinnerAdapter userSpinnerAdapter = new userSpinnerAdapter(getContext(), listUser);
        spIncidentPMReportName.setAdapter(userSpinnerAdapter);
        spIncidentPMReportName.post(() -> {
            int indexUser = -1;
            for (int i = 0; i < listUser.size(); i++) {
                if (listUser.get(i).getName().equals(Session.getUserName(getContext()))) {
                    indexUser = i;
                    break;
                }
            }
            if (indexUser > 0) {
                spIncidentPMReportName.setSelection(indexUser);
            }
        });
        lblIncidentPMReportName = v.findViewById(R.id.lblIncidentPMReportName);
        lblIncidentPMReportName.setVisibility(View.GONE);
        txtIncidentPMReportName = v.findViewById(R.id.txtIncidentPMReportName);
        txtIncidentPMReportName.setVisibility(View.GONE);

        spIncidentPMLogisticPerson = v.findViewById(R.id.spIncidentPMLogisticPerson);
        spIncidentPMLogisticPerson.setAdapter(userSpinnerAdapter);

        spIncidentPMAcType = v.findViewById(R.id.spIncidentPMAcType);
        List<String> listActivityType = new ArrayList<>();
        listActivityType.add("Trabajo Rutinario");
        listActivityType.add("Trabajo Programado");
        listActivityType.add("Trabajo Mayor (Paradas)");
        listActivityType.add("Trabajo Emergente");
        listActivityType.add("Proyecto");
        listActivityType.add("Sitio de Clientes");
        stringSpinnerAdapter adapterActivityTye = new stringSpinnerAdapter(getContext(), listActivityType);
        spIncidentPMAcType.setAdapter(adapterActivityTye);

        spIncidentPMCause = v.findViewById(R.id.spIncidentPMCause);
        List<String> listCause = new ArrayList<>();
        listCause.add("Acto Inseguro");
        listCause.add("Condición Insegura");
        stringSpinnerAdapter adapterCause = new stringSpinnerAdapter(getContext(), listCause);
        spIncidentPMCause.setAdapter(adapterCause);

        spIncidentPMInvloved = v.findViewById(R.id.spIncidentPMInvloved);
        List<String> listInvolved = new ArrayList<>();
        listInvolved.add("No Definido");
        listInvolved.add("Unacem");
        listInvolved.add("Contratista");
        listInvolved.add("Visitante");
        listInvolved.add("Clientes");
        stringSpinnerAdapter adapterInvolved = new stringSpinnerAdapter(getContext(), listInvolved);
        spIncidentPMInvloved.setAdapter(adapterInvolved);
        spIncidentPMInvloved.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (count > 0 && adapterView.getItemAtPosition(i).equals("Visitante")) {
                    lblIncidentnPMVisitor.setVisibility(View.VISIBLE);
                    txtIncidentPMVisitor.setVisibility(View.VISIBLE);
                    lblIncidentPMCustomer.setVisibility(View.GONE);
                    txtIncidentPMCustomer.setVisibility(View.GONE);
                } else if (count > 0 && adapterView.getItemAtPosition(i).equals("Clientes")) {
                    lblIncidentPMCustomer.setVisibility(View.VISIBLE);
                    txtIncidentPMCustomer.setVisibility(View.VISIBLE);
                    lblIncidentnPMVisitor.setVisibility(View.GONE);
                    txtIncidentPMVisitor.setVisibility(View.GONE);
                } else {
                    lblIncidentnPMVisitor.setVisibility(View.GONE);
                    txtIncidentPMVisitor.setVisibility(View.GONE);
                    lblIncidentPMCustomer.setVisibility(View.GONE);
                    txtIncidentPMCustomer.setVisibility(View.GONE);
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        lblIncidentnPMVisitor = v.findViewById(R.id.lblIncidentnPMVisitor);
        lblIncidentnPMVisitor.setVisibility(View.GONE);
        txtIncidentPMVisitor = v.findViewById(R.id.txtIncidentPMVisitor);
        txtIncidentPMVisitor.setVisibility(View.GONE);
        lblIncidentPMCustomer = v.findViewById(R.id.lblIncidentPMCustomer);
        lblIncidentPMCustomer.setVisibility(View.GONE);
        txtIncidentPMCustomer = v.findViewById(R.id.txtIncidentPMCustomer);
        txtIncidentPMCustomer.setVisibility(View.GONE);

        spIncidentPMCategoy = v.findViewById(R.id.spIncidentPMCategoy);
        List<SdCategoryIncident> cat = new ArrayList<>();
        SdCategoryIncident sdCategoryIncident = new SdCategoryIncident();
        sdCategoryIncident.setName("Seleccione una categoría");
        cat.add(sdCategoryIncident);
        cat.addAll(sdCategoryIncidentDao.loadAll());
        sdCategoryIncidentSpinnerAdapter sdCapSpinner = new sdCategoryIncidentSpinnerAdapter(getContext(), cat);
        spIncidentPMCategoy.setAdapter(sdCapSpinner);
        // final sdSubcategoryIncidentSpinnerAdapter[] sdSubcategoryIncidentSpinnerAdapter = {null};
        spIncidentPMCategoy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String itemSelected = ((SdCategoryIncident) adapterView.getItemAtPosition(i)).getName();
                Log.i("Item Seleccionado", itemSelected);
                if (count > 0 && !itemSelected.equals("Seleccione una categoría")) {
                    Log.i("Entra al if", "true");
                    SdCategoryIncident sdCat = (SdCategoryIncident) adapterView.getItemAtPosition(i);
                    List<SdSubcategoryIncident> sdSubcategoryIncidents = new ArrayList<>();
                    SdSubcategoryIncident sdSubcategoryIncident = new SdSubcategoryIncident();
                    sdSubcategoryIncident.setName("Selecione una subcategoría");
                    sdSubcategoryIncidents.add(sdSubcategoryIncident);
                    List<SdSubcategoryIncident> result = sdSubcategoryIncidentDao.queryBuilder().where(SdSubcategoryIncidentDao.Properties.CategoryId.eq(sdCat.getId())).list();
                    sdSubcategoryIncidents.addAll(result);
                    sdSubcategoryIncidentSpinnerAdapter = new sdSubcategoryIncidentSpinnerAdapter(getContext(), sdSubcategoryIncidents);
                    spIncidentPMIncidence.setAdapter(sdSubcategoryIncidentSpinnerAdapter);
                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spIncidentPMIncidence = v.findViewById(R.id.spIncidentPMIncidence);
        // spIncidentPMIncidence.setAdapter(new sdSubcategoryIncidentSpinnerAdapter(getContext(), sdSubcategoryIncidentDao.loadAll()));


        spIncidentPMDepartment = v.findViewById(R.id.spIncidentPMDepartment);
        departmentSpinnerAdapter adapterReporterDeparment = new departmentSpinnerAdapter(getContext(), deparmetDao.loadAll());
        spIncidentPMDepartment.setAdapter(adapterReporterDeparment);

        spIncidentPMDepResp = v.findViewById(R.id.spIncidentPMDepResp);
        spIncidentPMDepResp.setAdapter(adapterReporterDeparment);

        spIncidentPMUbication = v.findViewById(R.id.spIncidentPMUbication);
        List<String> listLocation = new ArrayList<>();
        listLocation.add("Planta");
        listLocation.add("Oficinas Quito");
        listLocation.add("Canteras");
        listLocation.add("Otros");
        stringSpinnerAdapter adapterLocation = new stringSpinnerAdapter(getContext(), listLocation);
        spIncidentPMUbication.setAdapter(adapterLocation);

        spIncidentPMArea = v.findViewById(R.id.spIncidentPMArea);
        areaSpinnerAdapter adapterArea = new areaSpinnerAdapter(getContext(), areaDao.loadAll());
        spIncidentPMArea.setAdapter(adapterArea);

        txtIncidentPMSpecPlace = v.findViewById(R.id.txtIncidentPMSpecPlace);

        spIncidentPMRule = v.findViewById(R.id.spIncidentPMRule);
        ruleSpinnerAdapter adapterRules = new ruleSpinnerAdapter(getContext(), ruleDao.loadAll());
        spIncidentPMRule.setAdapter(adapterRules);

        txtIncidentPMRelate = v.findViewById(R.id.txtIncidentPMRelate);
        txtIncidentPMAction = v.findViewById(R.id.txtIncidentPMAction);
        spIncidentPMNotifications = v.findViewById(R.id.spIncidentPMNotifications);

        final ArrayAdapter<User> adapterUser = new ArrayAdapter<User>(getActivity(), android.R.layout.simple_spinner_dropdown_item, listUser);
        onSelectedListener = (selected) -> {
            emails = "";
            for (int i = 0; i < selected.length; i++) {
                if (selected[i]) {
                    emails += adapterUser.getItem(i).getId() + ",";
                }
            }
        };
        spIncidentPMNotifications.setAdapter(adapterUser, false, onSelectedListener);
        scrollView = v.findViewById(R.id.scrollViewIncidentsPM);

        if (incidents.getId() != null) {
            spIncidentPMPlace.setSelection(spinnerAdapterPLaces.getPosition(incidents.getMainPlace()));
            txtIncidentPMDate.setText(incidents.getReporterDate().split(" ")[0]);
            txtIncidentPMHour.setText(incidents.getReporterDate().split(" ")[1]);
            spIncidentPMType.setSelection(adapatertype.getPosition(incidents.getIncidentType()));
            spIncidentPMReportBy.setSelection(adapterReporBy.getPosition(incidents.getReportType()));
            if (incidents.getReportType().contains("Otros")) {
                spIncidentPMReportName.setVisibility(View.GONE);
                txtIncidentPMReportName.setVisibility(View.VISIBLE);
                txtIncidentPMReportName.setText(incidents.getReportPerson());
            } else {
                User user = userDao.queryBuilder().where(UserDao.Properties.Name.eq(incidents.getReportPerson())).unique();
                spIncidentPMReportName.setSelection(userSpinnerAdapter.getPOsition(user));
            }
            spIncidentPMLogisticPerson.setSelection(userSpinnerAdapter.getPOsition(userDao.queryBuilder().where(UserDao.Properties.Name.eq(incidents.getLogPersonVerify())).unique()));
            spIncidentPMAcType.setSelection(adapterActivityTye.getPosition(incidents.getActivityType()));
            spIncidentPMCause.setSelection(adapterCause.getPosition(incidents.getCause()));
            spIncidentPMInvloved.setSelection(adapterInvolved.getPosition(incidents.getInvolvedType()));
            if (incidents.getInvolvedType().contains("Visitante")) {
                txtIncidentPMVisitor.setVisibility(View.VISIBLE);
                txtIncidentPMVisitor.setText(incidents.getInvolvedName());
            } else if (incidents.getInvolvedType().contains("Clientes")) {
                txtIncidentPMCustomer.setVisibility(View.VISIBLE);
                txtIncidentPMCustomer.setText(incidents.getInvolvedName());
            }
            spIncidentPMCategoy.setSelection(sdCapSpinner.getPosition(sdCategoryIncidentDao.queryBuilder().where(SdCategoryIncidentDao.Properties.Id.eq(incidents.getSdCategory())).unique()));
            adapters.sdSubcategoryIncidentSpinnerAdapter editSubCat = new sdSubcategoryIncidentSpinnerAdapter(getContext(), sdSubcategoryIncidentDao.queryBuilder().where(SdSubcategoryIncidentDao.Properties.CategoryId.eq(incidents.getSdCategory())).list());
            spIncidentPMIncidence.setAdapter(editSubCat);
            spIncidentPMIncidence.setSelection(editSubCat.getPosition(sdSubcategoryIncidentDao.queryBuilder().where(SdSubcategoryIncidentDao.Properties.Id.eq(incidents.getSdSubcategory())).unique()));
            spIncidentPMDepartment.setSelection(adapterReporterDeparment.getPosition(deparmetDao.queryBuilder().where(DeparmetDao.Properties.Name.eq(incidents.getReportDepartmentName())).unique()));
            spIncidentPMDepResp.setSelection(adapterReporterDeparment.getPosition(deparmetDao.queryBuilder().where(DeparmetDao.Properties.Name.eq(incidents.getResponsibleDepartmentName())).unique()));
            spIncidentPMUbication.setSelection(adapterLocation.getPosition(incidents.getUbicationType()));
            spIncidentPMArea.setSelection(adapterArea.getPosition(areaDao.queryBuilder().where(AreaDao.Properties.Name.eq(incidents.getAreaName())).unique()));
            txtIncidentPMSpecPlace.setText(incidents.getSpecificArea());
            spIncidentPMRule.setSelection(adapterRules.getPosition(ruleDao.queryBuilder().where(com.movil.vida.dao.ruleDao.Properties.Name.eq(incidents.getRelateType())).unique()));
            txtIncidentPMRelate.setText(incidents.getRelate());
            txtIncidentPMAction.setText(incidents.getActionName());
            List<User> toMail = null;
            String[] m = incidents.getEmails().split(",");
            if (m.length > 0) {
                for (String userid : m) {
                    Log.i("User to email", userid);
//                    toMail.add(userDao.queryBuilder().where(UserDao.Properties.Id.eq(Long.parseLong(userid))).unique());
                }
            }


        }


        return v;
    }

    public boolean validate() {
        if (txtIncidentPMDate.getText().toString().isEmpty() || txtIncidentPMDate.getText().toString().contains(getString(R.string.error_field_required))) {
            txtIncidentPMDate.requestFocus();
            txtIncidentPMDate.setError(getString(R.string.error_field_required));
            txtIncidentPMDate.setTextColor(Color.RED);
            txtIncidentPMDate.setText(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, txtIncidentPMDate.getTop());
            });
            return false;
        }
        if (txtIncidentPMHour.getText().toString().isEmpty() || txtIncidentPMHour.getText().toString().contains(getString(R.string.error_field_required))) {
            txtIncidentPMHour.requestFocus();
            txtIncidentPMHour.setError(getString(R.string.error_field_required));
            txtIncidentPMHour.setTextColor(Color.RED);
            txtIncidentPMHour.setText(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, txtIncidentPMHour.getTop());
            });
            return false;
        }
        if (((SdCategoryIncident) spIncidentPMCategoy.getSelectedItem()).getName().contains("Seleccione una categoría")) {
            spIncidentPMCategoy.requestFocus();
            TextView errorText = (TextView) spIncidentPMCategoy.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, spIncidentPMCategoy.getTop());
            });
            return false;
        }
        if (((SdSubcategoryIncident) spIncidentPMIncidence.getSelectedItem()).getName().contains("Selecione una subcategoría")) {
            spIncidentPMIncidence.requestFocus();
            TextView errorText = (TextView) spIncidentPMIncidence.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, spIncidentPMIncidence.getTop());
            });
            return false;
        }
        if (txtIncidentPMSpecPlace.getText().toString().isEmpty()) {
            txtIncidentPMSpecPlace.requestFocus();
            txtIncidentPMSpecPlace.setError(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, txtIncidentPMSpecPlace.getBottom());
            });
            return false;
        }
        if (txtIncidentPMRelate.getText().toString().isEmpty()) {
            txtIncidentPMRelate.requestFocus();
            txtIncidentPMRelate.setError(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, txtIncidentPMRelate.getBottom());
            });
            return false;
        }
        if (txtIncidentPMAction.getText().toString().isEmpty()) {
            txtIncidentPMAction.requestFocus();
            txtIncidentPMAction.setError(getString(R.string.error_field_required));
            scrollView.post(() -> {
                scrollView.smoothScrollTo(0, txtIncidentPMAction.getBottom());
            });
            return false;
        }
        return true;
    }

    public void save() {
        incidents.setMainPlace(spIncidentPMPlace.getSelectedItem().toString());
        incidents.setReporterDate(txtIncidentPMDate.getText().toString() + " " + txtIncidentPMHour.getText().toString());
        incidents.setIncidentType(spIncidentPMType.getSelectedItem().toString());
        if (spIncidentPMType.getSelectedItem().toString().contains("Primeros Auxilios")) {
            incidents.setBodyPart(spIncidentPMBodyPart.getSelectedItem().toString());
            incidents.setInjuryType(spIncidentPMInjury.getSelectedItem().toString());
        }
        incidents.setReportType(spIncidentPMReportBy.getSelectedItem().toString());
        if (spIncidentPMReportBy.getSelectedItem().toString().contains("Otro")) {
            incidents.setReportPerson(txtIncidentPMReportName.getText().toString());
        }
        incidents.setReportPerson(((User) spIncidentPMReportName.getSelectedItem()).getName());
        incidents.setLogPersonVerify(((User) spIncidentPMLogisticPerson.getSelectedItem()).getName());
        incidents.setActivityType(spIncidentPMAcType.getSelectedItem().toString());
        incidents.setCause(spIncidentPMCause.getSelectedItem().toString());
        incidents.setInvolvedType(spIncidentPMInvloved.getSelectedItem().toString());
        if (!txtIncidentPMCustomer.getText().toString().isEmpty()) {
            incidents.setInvolvedName(txtIncidentPMCustomer.getText().toString());
        }
        if (!txtIncidentPMVisitor.getText().toString().isEmpty()) {
            incidents.setInvolvedName(txtIncidentPMVisitor.getText().toString());
        }
        incidents.setSdCategory(((SdCategoryIncident) spIncidentPMCategoy.getSelectedItem()).getId());
        incidents.setSdSubcategory(((SdSubcategoryIncident) spIncidentPMIncidence.getSelectedItem()).getId());
        incidents.setReportDepartmentName(((Deparmet) spIncidentPMDepartment.getSelectedItem()).getName());
        incidents.setResponsibleDepartmentName(((Deparmet) spIncidentPMDepartment.getSelectedItem()).getName());
        incidents.setUbicationType(spIncidentPMUbication.getSelectedItem().toString());
        incidents.setAreaName(((Area) spIncidentPMArea.getSelectedItem()).getName());
        incidents.setSpecificArea(txtIncidentPMSpecPlace.getText().toString());
        incidents.setRelateType(((rule) spIncidentPMRule.getSelectedItem()).getName());
        incidents.setRelate(txtIncidentPMRelate.getText().toString());
        incidents.setActionName(txtIncidentPMAction.getText().toString());
        incidents.setEmails(emails);

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            save();
            IncidentSingleton.setIncidents(incidents);
            callback.goToNextStep();
        }

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        //startActivity( new Intent(getContext(), IncidentsMenuActivity.class));
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String x = "";
        String y = "";
        if (dayOfMonth < 10) {
            x = "0";
        }
        if (monthOfYear < 10) {
            y = "0";
        }
        txtIncidentPMDate.setTextColor(Color.BLACK);
        txtIncidentPMDate.setError(null);
        txtIncidentPMDate.setText(x + dayOfMonth + "/" + y + (monthOfYear + 1) + "/" + year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        txtIncidentPMHour.setTextColor(Color.BLACK);
        txtIncidentPMHour.setError(null);
        txtIncidentPMHour.setText(hourOfDay + ":" + minute);
    }
}
