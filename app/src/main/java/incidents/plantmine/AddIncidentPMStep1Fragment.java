package incidents.plantmine;


import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Driver;
import com.movil.vida.dao.DriverDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.Vehicle;
import com.movil.vida.dao.VehicleDao;
import com.movil.vida.dao.VehicleDriver;
import com.movil.vida.dao.VehicleDriverDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import adapters.vehicleSpinnerAdapter;
import di.IncidentSingleton;
import incidents.IncidentsMenuActivity;
import incidents.ListIncidentsActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddIncidentPMStep1Fragment extends Fragment implements Step, BlockingStep {
    Incidents incidents = IncidentSingleton.getInstance();
    private Spinner spIncidentPMVehicleId;
    private TextView lblDriverCedula;
    private TextView lblDriverName;
    private TextView lblDriverBirthDay;
    private ImageView imgDriver;

    public AddIncidentPMStep1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_incident_pmstep1, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        VehicleDao vehicleDao = daoSession.getVehicleDao();
        DriverDao driverDao = daoSession.getDriverDao();
        VehicleDriverDao vehicleDriverDao = daoSession.getVehicleDriverDao();
        spIncidentPMVehicleId = v.findViewById(R.id.spIncidentPMVehicleId);
        List<Vehicle> listVehicles = new ArrayList<>();
        Vehicle ve = new Vehicle();
        ve.setInternalId("Seleccione un Vehículo");
        listVehicles.add(ve);
        listVehicles.addAll(vehicleDao.loadAll());
        Collections.sort(listVehicles.subList(1, listVehicles.size()), (o1, o2) -> o1.getInternalId().compareTo(o2.getInternalId()));
        vehicleSpinnerAdapter vehicleSpinnerAdapter = new vehicleSpinnerAdapter(getContext(), listVehicles);
        spIncidentPMVehicleId.setAdapter(vehicleSpinnerAdapter);
        lblDriverCedula = v.findViewById(R.id.lblDriverCedula);
        lblDriverName = v.findViewById(R.id.lblDriverName);
        lblDriverBirthDay = v.findViewById(R.id.lblDriverBirthDay);
        imgDriver = v.findViewById(R.id.imgDriver);
        spIncidentPMVehicleId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int count = 0;

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (count >= 1) {
                    Vehicle item = (Vehicle) adapterView.getItemAtPosition(i);
                    if (!item.getInternalId().equals("Seleccione un Vehículo")) {
                        //   Log.i("Vehicle Id",item.getId()+"");
                        // Log.i("Driver Id",vehicleDriverDao.load(item.getId())+"");
                        List<VehicleDriver> driverId = vehicleDriverDao.queryBuilder().where(VehicleDriverDao.Properties.VehicleId.eq(item.getId())).list();
                        if (driverId.size() > 0) {
                            Driver driver = driverDao.load(driverId.get(0).getDriverId());
                            lblDriverCedula.setText(driver.getCi());
                            lblDriverName.setText(driver.getFirstName() + " " + driver.getLastName());
                            lblDriverBirthDay.setText(driver.getBirthDate());

                            if (driver.getPhoto() != null) {
                                byte[] decodedString = Base64.decode(driver.getPhoto(), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                imgDriver.setImageBitmap(decodedByte);
                            }

                        }
                    } else {
                        imgDriver.setImageResource(R.drawable.list_selector_background_transition_holo_dark);
                        lblDriverBirthDay.setText("---");
                        lblDriverName.setText("---");
                        lblDriverCedula.setText("---");
                    }

                }
                count++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        if (incidents.getId() != null) {
            spIncidentPMVehicleId.setSelection(vehicleSpinnerAdapter.getPosition(vehicleDao.load(incidents.getVehicleId())));
            List<VehicleDriver> driverId = vehicleDriverDao.queryBuilder().where(VehicleDriverDao.Properties.VehicleId.eq(incidents.getVehicleId())).list();
            if (driverId.size() > 0) {
                Driver driver = driverDao.load(driverId.get(0).getDriverId());
                lblDriverCedula.setText(driver.getCi());
                lblDriverName.setText(driver.getFirstName() + " " + driver.getLastName());
                lblDriverBirthDay.setText(driver.getBirthDate());

                if (driver.getPhoto() != null) {
                    byte[] decodedString = Base64.decode(driver.getPhoto(), Base64.DEFAULT);
                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                    imgDriver.setImageBitmap(decodedByte);
                }
            }
        } else {
            imgDriver.setImageResource(R.drawable.list_selector_background_transition_holo_dark);
            lblDriverBirthDay.setText("---");
            lblDriverName.setText("---");
            lblDriverCedula.setText("---");
        }
        return v;
    }

    void save() {
        incidents.setVehicleId(((Vehicle) spIncidentPMVehicleId.getSelectedItem()).getId());
    }

    boolean validate() {
        if (((Vehicle) spIncidentPMVehicleId.getSelectedItem()).getInternalId().contains("Seleccione un Vehículo")) {
            spIncidentPMVehicleId.requestFocus();
            TextView errorText = (TextView) spIncidentPMVehicleId.getSelectedView();
            errorText.setError(getString(R.string.error_field_required));
            errorText.setTextColor(Color.RED);
            errorText.setText(getString(R.string.error_field_required));
            return false;
        }
        return true;
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        if (validate()) {
            save();
            IncidentSingleton.setIncidents(incidents);
            callback.goToNextStep();
        }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

        AlertDialog.Builder confirm = new AlertDialog.Builder(getContext());
        confirm.setTitle("Confirmación");
        confirm.setMessage("Salir sin guardar cambios?");
        confirm.setCancelable(true);
        confirm.setPositiveButton("Aceptar", (dialogInterface, i) -> {
            IncidentSingleton.emptyInfractions();
            IncidentSingleton.empyIncident();
            if (incidents.getId() != null) {
                startActivity(new Intent(getContext(), ListIncidentsActivity.class));
            } else {
                startActivity(new Intent(getContext(), IncidentsMenuActivity.class));
            }
        });
        confirm.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            dialogInterface.cancel();
        });
        AlertDialog alertConfirm = confirm.create();
        alertConfirm.show();
    }

}
