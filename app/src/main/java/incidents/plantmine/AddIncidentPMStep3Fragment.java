package incidents.plantmine;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IncidentImages;
import com.movil.vida.dao.IncidentImagesDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.IncidentsDao;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import adapters.SlideAdapter;
import di.IncidentSingleton;
import incidents.ListIncidentsActivity;
import me.relex.circleindicator.CircleIndicator;

import static android.app.Activity.RESULT_OK;


public class AddIncidentPMStep3Fragment extends Fragment implements Step, BlockingStep {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    ArrayList<byte[]> list_incident_images = new ArrayList<>();
    Incidents incidents = IncidentSingleton.getInstance();
    IncidentsDao incidentsDao;
    IncidentImagesDao incident_imagesDao;
    CircleIndicator indicator;
    private ConstraintLayout lytSliderPhotos;
    private FloatingActionButton btnAddImage;
    private FloatingActionButton btnSlideDeleteImage;
    private ViewPager viewPager;
    private SlideAdapter myadapter;

    public AddIncidentPMStep3Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_incident_pmstep3, container, false);

        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        incidentsDao = daoSession.getIncidentsDao();
        incident_imagesDao = daoSession.getIncidentImagesDao();

        lytSliderPhotos = v.findViewById(R.id.lytSliderPhotos);
        btnAddImage = lytSliderPhotos.findViewById(R.id.btnSlideAddImage);
        btnSlideDeleteImage = lytSliderPhotos.findViewById(R.id.btnSlideDeleteImage);
        btnAddImage.setOnClickListener(v1 -> {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                            5);
                }
            } else {
                dispatchTakePictureIntent();
            }
        });

        btnSlideDeleteImage.setOnClickListener(v2 -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
            adb.setTitle("Eliminar Imagen?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("Eliminar", (dialogInterface, i) -> {
                try {
                    /*int pageIndex = myadapter.removeView(viewPager, SlideAdapter.currentPosition);
                    if (pageIndex == myadapter.getCount())
                        pageIndex--;
                    viewPager.setCurrentItem(pageIndex);
                    indicator.setViewPager(viewPager);
                    hideDeleteButton(list_incident_images.size());*/
                } catch (Exception e) {
                    Log.i("Error index", e.getMessage());
                }
            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();
        });


        if (incidents.getId() != null) {
            for (IncidentImages images : incident_imagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).list()) {
                list_incident_images.add(images.getImage());
            }
            //  list_incident_images.addAll(incident_imagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).list().stream().map(IncidentImages::getImage).collect(Collectors.toList()));
        }

        viewPager = v.findViewById(R.id.viewPagerImages);
        myadapter = new SlideAdapter(getContext(), list_incident_images, viewPager);
        viewPager.setAdapter(myadapter);
        indicator = v.findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        myadapter.registerDataSetObserver(indicator.getDataSetObserver());
        hideDeleteButton(list_incident_images.size());
        return v;
    }

    public void hideDeleteButton(int size) {
        if (size > 0) {
            btnSlideDeleteImage.setVisibility(View.VISIBLE);
        } else {
            btnSlideDeleteImage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                // Your app will not have this permission. Turn off all functions
                // that require this permission or it will force close like your
                // original question
            }
        }
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            list_incident_images.add(byteArray);
            myadapter.notifyDataSetChanged();
            viewPager.setCurrentItem(myadapter.getCount());
            myadapter.getCount();
            viewPager.getCurrentItem();
            hideDeleteButton(list_incident_images.size());
        }
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        Long incidentId;
        if (incidents.getId() != null) {
            incidentsDao.update(incidents);
            incidentId = incidents.getId();
            incident_imagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
        } else {
            incidents.setSyncState(0);
            //   incidents.setExternalType("P");
            incidentId = incidentsDao.insert(incidents);
        }

        if (list_incident_images.size() > 0) {
            for (byte[] photo : list_incident_images) {
                IncidentImages incidentImages = new IncidentImages();
                incidentImages.setImage(photo);
                incidentImages.setIncidentId(incidentId);
                incident_imagesDao.insert(incidentImages);
            }
        }
        IncidentSingleton.empyIncident();
        startActivity(new Intent(getContext(), ListIncidentsActivity.class));
        getActivity().finish();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
