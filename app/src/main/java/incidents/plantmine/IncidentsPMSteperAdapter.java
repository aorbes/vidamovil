package incidents.plantmine;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.movil.vida.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import incidents.normal.AddIncidentImagesFragment;

/**
 * Created by Alexander on 11/4/2018.
 */

public class IncidentsPMSteperAdapter extends AbstractFragmentStepAdapter {

    public IncidentsPMSteperAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position) {
            case 0:
                return new AddIncidentPMStep1Fragment();
            case 1:
                return new AddIncidentPMStep2Fragment();
            case 2:
                return new AddIncidentImagesFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle(null);
        switch (position) {
            case 0:
                builder.setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Cancelar")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setTitle("Datos del Vehículo");
                break;
            case 1:
                builder.setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Anterior")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Datos del Incidente");
                break;
            case 2:
                builder.setEndButtonLabel("Guardar")
                        .setBackButtonLabel("Anterior")
                        .setNextButtonEndDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Fotos del Incidente");
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}
