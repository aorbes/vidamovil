package incidents;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.IncidentsDao;

import java.util.ArrayList;

public class ListIncidentsActivity extends AppCompatActivity {
    IncidentsDao incidentsDao;
    IncidentsAdapter adapter;
    Toolbar toolbar=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_incidents);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        incidentsDao = daoSession.getIncidentsDao();


        // incidentsDao.deleteAll();
        toolbar = (Toolbar) findViewById(R.id.incidentlistbar_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Incidentes Registrados");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ArrayList<Incidents> arrayList = new ArrayList<>();
        arrayList.addAll(incidentsDao.loadAll());
        //    Log.i("Size of list incidents",incidentsDao.loadAll().size()+"");
        RecyclerView rv = (RecyclerView) findViewById(R.id.listincidents);
        rv.setHasFixedSize(true);
        adapter = new IncidentsAdapter(arrayList,ListIncidentsActivity.this);
        rv.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);

    }
}
