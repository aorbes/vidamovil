package incidents;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.Session;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IncidentImages;
import com.movil.vida.dao.IncidentImagesDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.IncidentsDao;
import com.movil.vida.dao.RiskIncident;
import com.movil.vida.dao.RiskIncidentDao;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

import di.IncidentSingleton;
import incidents.normal.WizardAddIncident;
import incidents.plantmine.WizardAddIncidentPM;
import incidents.vehicleroad.WizardAddIncidentVR;

public class IncidentsMenuActivity extends AppCompatActivity {
    Toolbar toolbar = null;
    LinearLayout layout_add_incident;
    LinearLayout layout_list_incident;
    Button btnUploadIncidents;
    TextView lblIncidentsUploaded;
    TextView lblIncidentsNotUploaded;
    IncidentsDao incidentsDao;
    IncidentImagesDao incidentImagesDao;
    RiskIncidentDao riskIncidentDao;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incidents_menu);
        toolbar = (Toolbar) findViewById(R.id.incident_toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setTitle("Menú incidentes");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        incidentsDao = daoSession.getIncidentsDao();
        incidentImagesDao = daoSession.getIncidentImagesDao();
        riskIncidentDao = daoSession.getRiskIncidentDao();

        // getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        url = Session.getBaseUrl(IncidentsMenuActivity.this) + "/uploadincidents";
        layout_add_incident = (LinearLayout) findViewById(R.id.layout_add_incident);
        layout_add_incident.setOnClickListener(v -> {
            registerForContextMenu(layout_add_incident);
            openContextMenu(layout_add_incident);
        });
        layout_list_incident = (LinearLayout) findViewById(R.id.layout_list_incident);
        layout_list_incident.setOnClickListener(a -> {
            startActivity(new Intent(getApplicationContext(), ListIncidentsActivity.class));
        });

        lblIncidentsUploaded = (TextView) findViewById(R.id.lblUploaded);
        lblIncidentsNotUploaded = (TextView) findViewById(R.id.lblIncidentsNotUploaded);
        updateUploadeds();
        btnUploadIncidents = (Button) findViewById(R.id.btnUploadIncidents);
        btnUploadIncidents.setOnClickListener(a -> {
            AlertDialog.Builder adb = new AlertDialog.Builder(IncidentsMenuActivity.this);
            adb.setTitle("¿Inciar la subida de incidentes a VIDA?");
            adb.setIcon(android.R.drawable.ic_dialog_alert);
            adb.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                if (incidentsDao.queryBuilder().where(IncidentsDao.Properties.SyncState.eq(0)).list().size() > 0) {
                    new HttpRequestTask(IncidentsMenuActivity.this).execute();
                } else if (incidentsDao.loadAll().size() < 1) {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(IncidentsMenuActivity.this);
                    goLogin.setMessage("No hay registros de incidentes!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("OK", (dialogInterface1, i1) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                } else {
                    AlertDialog.Builder goLogin = new AlertDialog.Builder(IncidentsMenuActivity.this);
                    goLogin.setMessage("Todos los incidentes se han subido al servidor!!");
                    goLogin.setCancelable(false);
                    goLogin.setPositiveButton("OK", (dialogInterface2, i2) -> {
                        dialogInterface.cancel();
                    });
                    AlertDialog alertLogin = goLogin.create();
                    alertLogin.show();
                }
            });
            adb.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            });
            adb.show();
        });
    }

    private void updateUploadeds() {
        Long incidentUploaded = incidentsDao.queryBuilder().where(IncidentsDao.Properties.SyncState.eq(1)).count();
        lblIncidentsUploaded.setText(incidentUploaded + "");
        Long incidentNotUploaded = incidentsDao.queryBuilder().where(IncidentsDao.Properties.SyncState.eq(0)).count();
        lblIncidentsNotUploaded.setText(incidentNotUploaded + "");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Tipos de Incidente");
        menu.setHeaderIcon(R.drawable.ic_incident_image);
        menu.add(0, 1, 0, "Actividades Planta Mina");
        menu.add(0, 2, 0, "Vehículo de Logística Planta/Mina");
        menu.add(0, 3, 0, "Vehículo de Logística Carretera");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            IncidentSingleton.getInstance().setExternalType("N");
            startActivity(new Intent(getApplicationContext(), WizardAddIncident.class));
        }
        if (item.getItemId() == 2) {
            IncidentSingleton.getInstance().setExternalType("P");
            startActivity(new Intent(getApplicationContext(), WizardAddIncidentPM.class));
        }
        if (item.getItemId() == 3) {
            IncidentSingleton.getInstance().setExternalType("C");
            startActivity(new Intent(getApplicationContext(), WizardAddIncidentVR.class));
        }
        return super.onContextItemSelected(item);
    }

    //MainApplication.getInstance().getDaoSession().getDatabase()
    private class HttpRequestTask extends AsyncTask<Void, String, Void> {
        ProgressDialog progDailog;
        Context context;
        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        boolean conn = false;
        int toUpload = 0;
        int Uploaded = 0;

        public HttpRequestTask(Context mContext) {
            this.context = mContext;
            headers.setContentType(MediaType.APPLICATION_JSON);
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            toUpload = incidentsDao.queryBuilder().where(IncidentsDao.Properties.SyncState.eq(0)).list().size();
            progDailog = new ProgressDialog(IncidentsMenuActivity.this);
            progDailog.setTitle("Uploading");
            progDailog.setMessage("Subiendo Incidentes a VIDA..." + Uploaded + "/" + toUpload);
            progDailog.setIndeterminate(false);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            System.out.println("Test Rest 1.5");

            try {
                JSONObject jsonObject;
                for (Incidents incidents : incidentsDao.queryBuilder().where(IncidentsDao.Properties.SyncState.eq(0)).list()) {
                    jsonObject = new JSONObject();
                    jsonObject.put("reporterDate", incidents.getReporterDate());
                    if (incidents.getIncidentType().equals("Desviación")) {
                        jsonObject.put("incidentType", "D");
                    }
                    if (incidents.getIncidentType().equals("Casi Accidente")) {
                        jsonObject.put("incidentType", "N");
                    }
                    if (incidents.getIncidentType().equals("Primeros Auxilios")) {
                        jsonObject.put("incidentType", "F");
                    }
                    jsonObject.accumulate("reportDepartmentName", incidents.getReportDepartmentName());
                    if (incidents.getReportType().equals("Empleado de Unacem")) {
                        jsonObject.put("reportType", "L");
                    }
                    if (incidents.getReportType().equals("Otro")) {
                        jsonObject.put("reportType", "O");
                    }
                    jsonObject.put("reportPerson", incidents.getReportPerson());
                    jsonObject.put("responsibleDepartmentName", incidents.getResponsibleDepartmentName());

                    if (incidents.getInvolvedType().equals("No Definido")) {
                        jsonObject.put("involvedType", "N");
                    }
                    if (incidents.getInvolvedType().equals("Unacem")) {
                        jsonObject.put("involvedType", "L");
                    }
                    if (incidents.getInvolvedType().equals("Contratista") && incidents.getContractor() != null) {
                        jsonObject.put("involvedType", "C");
                        jsonObject.put("involvedName", incidents.getContractor().getName());
                        jsonObject.put("contractorNumber", incidents.getContractor().getContractornumber());
                    }
                    if (incidents.getInvolvedType().equals("Visitante")) {
                        jsonObject.put("involvedType", "V");
                        jsonObject.put("involvedName", incidents.getInvolvedName());
                    }
                    if (incidents.getInvolvedType().equals("Clientes")) {
                        jsonObject.put("involvedType", "T");
                        jsonObject.put("involvedName", incidents.getInvolvedName());
                    }
                    if (incidents.getUbicationType().equals("Planta")) {
                        jsonObject.put("ubicationType", "P");
                    }
                    if (incidents.getUbicationType().equals("Oficinas Quito")) {
                        jsonObject.put("ubicationType", "Q");
                    }
                    if (incidents.getUbicationType().equals("Canteras")) {
                        jsonObject.put("ubicationType", "C");
                    }
                    if (incidents.getUbicationType().equals("Otros")) {
                        jsonObject.put("ubicationType", "O");
                    }
                    jsonObject.put("areaName", incidents.getAreaName());
                    jsonObject.put("relate", incidents.getRelate());
                    jsonObject.put("propertyDamages", incidents.getPropertyDamages());
                    jsonObject.put("specificArea", incidents.getSpecificArea());
                    jsonObject.put("relateType", incidents.getRelateType());
                    jsonObject.put("actionName", incidents.getActionName());
                    jsonObject.put("mainPlace", incidents.getMainPlace());
                    Log.i("Emails--", incidents.getEmails());
                    System.out.println("Emails ---" + incidents.getEmails());
                    if (!incidents.getEmails().startsWith(",") && !incidents.getEmails().isEmpty()) {
                        jsonObject.put("emails", incidents.getEmails().substring(0, incidents.getEmails().length() - 1));
                    } else {
                        jsonObject.put("emails", "");
                    }
                    jsonObject.put("externalType", incidents.getExternalType() + "");
                    jsonObject.put("sdCategory", incidents.getSdCategory());
                    jsonObject.put("sdSubcategory", incidents.getSdSubcategory());
                    jsonObject.put("vehicleId", incidents.getVehicleId());
                    List<IncidentImages> lstimages = incidentImagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).list();
                    if (lstimages.size() > 0) {
                        JSONArray images = new JSONArray();
                        for (IncidentImages incident_images : lstimages) {
                            String imgString = Base64.encodeToString(incident_images.getImage(),
                                    Base64.DEFAULT);
                            images.put(imgString);
                        }
                        jsonObject.put("images", images);
                    }
                    List<RiskIncident> infractions = riskIncidentDao.queryBuilder().where(RiskIncidentDao.Properties.IncidentId.eq(incidents.getId())).list();
                    if (infractions.size() > 0) {
                        JSONArray riskinfraction = new JSONArray();
                        for (RiskIncident riskIncident : infractions) {
                            System.out.println("Infraction id to send---" + riskIncident.getRiskId());
                            riskinfraction.put(riskIncident.getRiskId());
                        }
                        jsonObject.put("infractions", riskinfraction);
                    }
                    Log.i("JSON", jsonObject.toString());

                    HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
                    ResponseEntity<String> response =
                            restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
                    if (response.getStatusCode() == HttpStatus.CREATED) {
                        this.publishProgress("Subiendo Incidentes a vida..." + Uploaded++ + "/" + toUpload);
                        conn = true;
                        incidents.setSyncState(1);
                        incidentsDao.update(incidents);
                        System.out.println("Se creó el incidente" + response.getBody());
                    } else {
                        break;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progDailog.setMessage(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progDailog.dismiss();

            if (!conn) {
                AlertDialog.Builder goLogin = new AlertDialog.Builder(context);
                goLogin.setTitle("Alerta");
                goLogin.setMessage("Ocurrió un error al sincronizar con VIDA");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            } else {
                updateUploadeds();
                android.support.v7.app.AlertDialog.Builder goLogin = new android.support.v7.app.AlertDialog.Builder(context);
                goLogin.setMessage("Se subió los incidentes a VIDA correctamente!!");
                goLogin.setCancelable(false);
                goLogin.setPositiveButton("Aceptar", (dialogInterface, i) -> {
                    dialogInterface.cancel();
                });
                android.support.v7.app.AlertDialog alertLogin = goLogin.create();
                alertLogin.show();
            }
        }
    }
}
