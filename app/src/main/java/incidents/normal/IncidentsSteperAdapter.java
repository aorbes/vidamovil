package incidents.normal;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.movil.vida.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by Alexander on 26/1/2018.
 */

public class IncidentsSteperAdapter extends AbstractFragmentStepAdapter {

    public IncidentsSteperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
 return new AddIncidentFragment();
            case  1:
                return new AddIncidentImagesFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        StepViewModel.Builder builder = new StepViewModel.Builder(context)
                .setTitle(null);
        switch (position) {
            case 0:
                builder
                        .setEndButtonLabel("Siguiente")
                        .setBackButtonLabel("Cancelar")
                        .setNextButtonEndDrawableResId(R.drawable.ms_ic_chevron_right)
                        .setBackButtonStartDrawableResId(StepViewModel.NULL_DRAWABLE)
                        .setTitle("Datos Obligatorios");
                break;
            case 1:
                builder.setEndButtonLabel("Guardar Incidente")
                        .setBackButtonLabel("Anterior")
                        .setBackButtonStartDrawableResId(R.drawable.ms_ic_chevron_start)
                        .setTitle("Agregar Fotos");
                break;
            default:
                throw new IllegalArgumentException("Unsupported position: " + position);
        }
        return builder.create();
    }
}
