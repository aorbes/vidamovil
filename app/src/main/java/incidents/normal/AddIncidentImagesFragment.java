package incidents.normal;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.movil.vida.BuildConfig;
import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IncidentImages;
import com.movil.vida.dao.IncidentImagesDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.IncidentsDao;
import com.movil.vida.dao.RiskIncident;
import com.movil.vida.dao.RiskIncidentDao;
import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import adapters.SlideAdapter;
import di.IncidentSingleton;
import incidents.ListIncidentsActivity;
import me.relex.circleindicator.CircleIndicator;
import utils.FileUtil;
import utils.ImageUtils;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddIncidentImagesFragment extends Fragment implements Step,BlockingStep{

    ArrayList<byte[]> list_incident_images = new ArrayList<>();
    Incidents incidents = IncidentSingleton.getInstance();
    IncidentsDao incidentsDao;
    RiskIncidentDao riskIncidentDao;
    IncidentImagesDao incident_imagesDao;
    CircleIndicator indicator;
    private List<Integer> infractions = IncidentSingleton.getRiskInstance();
    private ConstraintLayout lytSliderPhotos;
    private FloatingActionButton btnAddImage;
    private FloatingActionButton btnSlideDeleteImage;
    private ViewPager viewPager;
    private SlideAdapter myadapter;
    private static final int IMAGE_GALLERY_REQUEST = 25;
    private static final int REQUEST_TAKE_PHOTO = 20;

    private AlertDialog dialog;
    private Button btnGallery;
    private Button btnCammera;
    private AlertDialog dialogDelete;
    private Button deleteImage;
    private Button cancelDelete;
    public AddIncidentImagesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_incident_images, container, false);
        DaoSession daoSession = ((DaoApp) getContext().getApplicationContext()).getDaoSession();
        incidentsDao = daoSession.getIncidentsDao();
        riskIncidentDao = daoSession.getRiskIncidentDao();
        incident_imagesDao = daoSession.getIncidentImagesDao();

        lytSliderPhotos = v.findViewById(R.id.lytSliderPhotosIncNormal);
        btnAddImage = lytSliderPhotos.findViewById(R.id.btnSlideAddImage);
        btnSlideDeleteImage = lytSliderPhotos.findViewById(R.id.btnSlideDeleteImage);
        btnAddImage.setOnClickListener(v1 -> {
            dialog.show();
        });

        btnSlideDeleteImage.setOnClickListener(v2 -> {
            dialogDelete.show();
        });


        if (incidents.getId() != null) {
            list_incident_images = new ArrayList<>();
            for (IncidentImages images : incident_imagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).list()) {
                list_incident_images.add(images.getImage());
            }
        }


//Add DeleteImage
        AlertDialog.Builder mBuilderDelImage = new AlertDialog.Builder(getContext());
        View mViewDelImage = inflater.inflate(R.layout.delete_layout, null);
        mBuilderDelImage.setView(mViewDelImage);
        dialogDelete = mBuilderDelImage.create();
        deleteImage = mViewDelImage.findViewById(R.id.btnDelete);
        deleteImage.setOnClickListener(view1 -> {
            try {
                myadapter.removeView();
                indicator.setViewPager(viewPager);
                hideDeleteButton(myadapter.getCount());
                dialogDelete.cancel();
            } catch (Exception e) {
                dialogDelete.cancel();
                Toast.makeText(getContext(), "Error al eliminar imagen", Toast.LENGTH_LONG).show();
            }
        });
        cancelDelete = mViewDelImage.findViewById(R.id.btnCanlcel);
        cancelDelete.setOnClickListener(view1 -> {
            dialogDelete.cancel();
        });
        // init dialogsAddImage
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View mView = getActivity().getLayoutInflater().inflate(R.layout.cammera_or_gallery, null);
        mBuilder.setView(mView);
        dialog = mBuilder.create();
        btnGallery = mView.findViewById(R.id.btnGallery);
        btnCammera = mView.findViewById(R.id.btnCammera);
        btnGallery.setOnClickListener(view -> {
            onImageGalleryClicked();
        });
        btnCammera.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{android.Manifest.permission.CAMERA},
                            5);
                }
            } else {
                dispatchTakePictureIntent();
            }
        });

        viewPager = v.findViewById(R.id.viewPagerImages);
        indicator = v.findViewById(R.id.indicator);
        myadapter = new SlideAdapter(getContext(), list_incident_images, viewPager);
        viewPager.setAdapter(myadapter);
        indicator.setViewPager(viewPager);
        myadapter.registerDataSetObserver(indicator.getDataSetObserver());
        ViewPagerArrowIndicator viewPagerArrowIndicator = v.findViewById(R.id.viewPagerArrowIndicator);
        viewPagerArrowIndicator.bind(viewPager);
        hideDeleteButton(list_incident_images.size());
        return v;
    }


    public void onImageGalleryClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();
        Uri data = Uri.parse(pictureDirectoryPath);
        photoPickerIntent.setDataAndType(data, "image/*");
        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }
    public void hideDeleteButton(int size) {
        if (size > 0) {
            btnSlideDeleteImage.setVisibility(View.VISIBLE);
        } else {
            btnSlideDeleteImage.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            }
        }
    }

    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile(getContext());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        dialog.cancel();
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                Bitmap myBitmap = ImageUtils.compressImage(new File(ImageUtils.currentImagePath), getContext());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                list_incident_images.add(byteArray);
                myadapter.notifyDataSetChanged();
                viewPager.setCurrentItem(myadapter.getCount());
                hideDeleteButton(myadapter.getCount());
            }
            if (requestCode == IMAGE_GALLERY_REQUEST) {
                try {
                    Bitmap image = ImageUtils.compressImage(FileUtil.from(getContext(), data.getData()), getContext());
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    list_incident_images.add(byteArray);
                    myadapter.notifyDataSetChanged();
                    viewPager.setCurrentItem(myadapter.getCount());
                    hideDeleteButton(myadapter.getCount());

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "No se puede abrir la imagen", Toast.LENGTH_LONG).show();
                }

            }
        }
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        Long incidentId;
        if (incidents.getId() != null) {
            incidentsDao.update(incidents);
            incidentId = incidents.getId();
            incident_imagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(incidents.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
            riskIncidentDao.queryBuilder().where(RiskIncidentDao.Properties.IncidentId.eq(incidents.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
        } else {
            incidents.setSyncState(0);
            // incidents.setExternalType("N");
            incidentId = incidentsDao.insert(incidents);
        }

        if (list_incident_images.size() > 0) {
            for (byte[] photo : list_incident_images) {
                IncidentImages incidentImages = new IncidentImages();
                incidentImages.setImage(photo);
                incidentImages.setIncidentId(incidentId);
                incident_imagesDao.insert(incidentImages);
            }
        }
        if (infractions.size() > 0) {
            for (Integer id : infractions) {
                RiskIncident riskIncident = new RiskIncident();
                riskIncident.setIncidentId(incidentId);
                riskIncident.setRiskId(id);
                riskIncidentDao.save(riskIncident);
            }
        }
        IncidentSingleton.empyIncident();
        IncidentSingleton.emptyInfractions();
        startActivity(new Intent(getContext(), ListIncidentsActivity.class));
        getActivity().finish();

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
}
