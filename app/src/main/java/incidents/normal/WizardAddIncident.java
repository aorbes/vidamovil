package incidents.normal;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IncidentsDao;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import di.IncidentSingleton;
import incidents.IncidentsMenuActivity;
import incidents.ListIncidentsActivity;

public class WizardAddIncident extends AppCompatActivity implements StepperLayout.StepperListener {
    private StepperLayout mStepperLayout;
    IncidentsDao incidentsDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_add_incident);

        DaoSession daoSession = ((DaoApp) getApplication()).getDaoSession();
        incidentsDao = daoSession.getIncidentsDao();
        Bundle b = getIntent().getExtras();
        if (b != null) {
            IncidentSingleton.getInstance();
            IncidentSingleton.setIncidents(incidentsDao.load(b.getLong("IncidentNID")));
        }
        mStepperLayout = (StepperLayout) findViewById(R.id.stepperLayout);
        mStepperLayout.setAdapter(new IncidentsSteperAdapter(getSupportFragmentManager(), this));
        mStepperLayout.setListener(this);
    }


    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {
     /*   Intent intent = new Intent(this, IncidentsMenuActivity.class);
        startActivity(intent);*/
    }

    @Override
    public void onBackPressed() {
        Log.i("Click On back", "----------");
        AlertDialog.Builder confirm = new AlertDialog.Builder(this);
        confirm.setTitle("Confirmación");
        confirm.setMessage("Salir sin guardar cambios?");
        confirm.setCancelable(true);
        confirm.setPositiveButton("Aceptar", (dialogInterface, i) -> {
            IncidentSingleton.empyIncident();
            if (IncidentSingleton.getInstance().getId() != null) {
                startActivity(new Intent(this, ListIncidentsActivity.class));
            } else {
                startActivity(new Intent(this, IncidentsMenuActivity.class));
            }
        });
        confirm.setNegativeButton("Cancelar", (dialogInterface, i) -> {
            dialogInterface.cancel();
        });
        AlertDialog alertConfirm = confirm.create();
        alertConfirm.show();
    }
}
