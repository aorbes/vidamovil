package incidents;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.movil.vida.DaoApp;
import com.movil.vida.R;
import com.movil.vida.dao.DaoSession;
import com.movil.vida.dao.IncidentImages;
import com.movil.vida.dao.IncidentImagesDao;
import com.movil.vida.dao.Incidents;
import com.movil.vida.dao.IncidentsDao;
import com.movil.vida.dao.RiskIncidentDao;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import incidents.normal.WizardAddIncident;
import incidents.plantmine.WizardAddIncidentPM;
import incidents.vehicleroad.WizardAddIncidentVR;

//import android.support.v7.app.AlertDialog;

/**
 * Created by alex_.
 */
public class IncidentsAdapter extends RecyclerView.Adapter<IncidentsAdapter.MyViewHolder> {

    private ArrayList<Incidents> item;
    IncidentsDao incidentsDao;
    IncidentImagesDao incidentImagesDao;
    RiskIncidentDao riskIncidentDao;
    Context mContext;

    public IncidentsAdapter(ArrayList<Incidents> item, Context mContext) {
        super();
        this.item = item;
        this.mContext = mContext;
        DaoSession daoSession = ((DaoApp) mContext.getApplicationContext()).getDaoSession();
        incidentImagesDao = daoSession.getIncidentImagesDao();
        incidentsDao = daoSession.getIncidentsDao();
        riskIncidentDao = daoSession.getRiskIncidentDao();
    }

    @Override
    public IncidentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_incident_item, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.date.setText(item.get(position).getReporterDate());
        holder.incidetType.setText(item.get(position).getIncidentType());
        holder.incidentCause.setText(item.get(position).getCause());
        if (item.get(position).getExternalType().contains("N")) {
            holder.incidentClasification.setText("Actividades Planta Mina");
        } else if (item.get(position).getExternalType().contains("P")) {
            holder.incidentClasification.setText("Vehículo de Logistica Planta/Mina");
        } else if (item.get(position).getExternalType().contains("C")) {
            holder.incidentClasification.setText("Vehículo de Logistica Carretera");
        }

        holder.id.setText(item.get(position).getId() + "");
        List<IncidentImages> images = incidentImagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(item.get(position).getId())).list();
        if (images.size() > 0) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(images.get(0).getImage());
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            holder.photo.setImageBitmap(theImage);
        } else {
            holder.photo.setImageResource(R.drawable.ic_incident_image);
        }
        if (item.get(position).getSyncState() == 0) {
            holder.sync_state_image.setImageResource(R.drawable.ic_sync_black_24dp);
        } else {
            holder.sync_state_image.setImageResource(R.drawable.ic_check_black_24dp);
        }

    }

    @Override
    public int getItemCount() {
        return item.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView mCardView;
        TextView date, incidetType, incidentCause, incidentClasification, id;
        ImageView photo, sync_state_image;
        Button btnDeleteIncident, btnEditIncident;

        public MyViewHolder(View v) {
            super(v);


            btnDeleteIncident = v.findViewById(R.id.btnDeleteIncident);
            btnEditIncident = v.findViewById(R.id.btnEditIncident);
            mCardView = v.findViewById(R.id.card_view);
            date = v.findViewById(R.id.lblDate);
            incidetType = v.findViewById(R.id.lblIncidentType);
            incidentCause = v.findViewById(R.id.lblCause);
            incidentClasification = v.findViewById(R.id.lblClasification);
            photo = v.findViewById(R.id.imgIncidente);
            sync_state_image = v.findViewById(R.id.sync_state_img);
            id = v.findViewById(R.id.lblId);

            btnEditIncident.setOnClickListener(a -> {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    Incidents i = item.get(getAdapterPosition());
                    Bundle args = new Bundle();
                    AppCompatActivity activity = (AppCompatActivity) a.getContext();
                    if (i.getExternalType().contains("P")) {
                        args.putLong("IncidentPMID", i.getId());
                        Intent intent = new Intent(a.getContext(), WizardAddIncidentPM.class);
                        intent.putExtras(args);
                        activity.startActivity(intent);
                    }
                    if (i.getExternalType().contains("N")) {
                        args.putLong("IncidentNID", i.getId());
                        Intent intent = new Intent(a.getContext(), WizardAddIncident.class);
                        intent.putExtras(args);
                        activity.startActivity(intent);
                    }
                    if (i.getExternalType().contains("C")) {
                        args.putLong("IncidentCID", i.getId());
                        Intent intent = new Intent(a.getContext(), WizardAddIncidentVR.class);
                        intent.putExtras(args);
                        activity.startActivity(intent);
                    }
                }
            });


            btnDeleteIncident.setOnClickListener(view -> {
                AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
                adb.setTitle("Está seguro querer eliminar el incidente?");
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            Incidents i = item.get(pos);
                            incidentImagesDao.queryBuilder().where(IncidentImagesDao.Properties.IncidentId.eq(i.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                            riskIncidentDao.queryBuilder().where(RiskIncidentDao.Properties.IncidentId.eq(i.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
                            incidentsDao.delete(i);
                            item.remove(pos);
                            notifyItemRemoved(pos);

                        }
                    }
                });
                adb.setNegativeButton("Cancelar", (v1, v2) -> {
                });
                adb.show();
            });
        }


    }

}
