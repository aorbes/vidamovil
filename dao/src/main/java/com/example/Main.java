package com.example;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;

public class Main {


    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(51, "com.movil.vida.dao");
        schema.enableKeepSectionsByDefault();
        createDatabae(schema);
        DaoGenerator daoGenerator = new DaoGenerator();
        daoGenerator.generateAll(schema, args[0]);
    }

    private static void createDatabae(Schema schema) {
        //Incident Entity
        Entity incidents = schema.addEntity("Incidents");
        incidents.addIdProperty();
        incidents.addStringProperty("creationDate");
        incidents.addStringProperty("reporterDate");
        incidents.addStringProperty("incidentType");
        incidents.addStringProperty("reportDepartmentName");
        incidents.addStringProperty("reportType");
        incidents.addStringProperty("reportPerson");
        incidents.addStringProperty("responsibleDepartmentName");
        incidents.addStringProperty("involvedType");
        incidents.addStringProperty("involvedName");
        incidents.addStringProperty("ubicationType");
        incidents.addStringProperty("areaName");
        incidents.addStringProperty("relate");
        incidents.addStringProperty("propertyDamages");
        incidents.addStringProperty("specificArea");
        incidents.addStringProperty("relateType");
        incidents.addStringProperty("actionName");
        incidents.addStringProperty("status");
        incidents.addStringProperty("priority");
        incidents.addStringProperty("panel");
        incidents.addStringProperty("gap");
        incidents.addStringProperty("risk");
        incidents.addStringProperty("cost");
        incidents.addIntProperty("employeeId");
        incidents.addStringProperty("comment");
        incidents.addStringProperty("activityType");
        incidents.addStringProperty("cause");
        incidents.addStringProperty("bodyPart");
        incidents.addStringProperty("injuryType");
        incidents.addDateProperty("expirationDate");
        incidents.addStringProperty("mainPlace");
        incidents.addStringProperty("contractorNumber");
        incidents.addStringProperty("sdSend");
        incidents.addBooleanProperty("enviromentalDmgs");
        incidents.addStringProperty("logPersonVerify");
        incidents.addDateProperty("closeDate");
        incidents.addIntProperty("syncState");
        incidents.addStringProperty("emails");
        incidents.addStringProperty("externalType");
        incidents.addLongProperty("sdCategory");
        incidents.addLongProperty("sdSubcategory");
        incidents.addLongProperty("vehicleId");
        Property contractor_id = incidents.addLongProperty("contractorId").nonPrimitiveType().getProperty();


        //Incidents_Images Entity
        Entity incident_images = schema.addEntity("IncidentImages");
        incident_images.addIdProperty();
        Property incident_id = incident_images.addLongProperty("incidentId").nonPrimitiveType().getProperty();
        incident_images.addToOne(incidents, incident_id);
        incidents.addToMany(incident_images, incident_id);
        incident_images.addByteArrayProperty("image");

        //User Entity
        Entity user = schema.addEntity("User");
        user.addIdProperty();
        user.addStringProperty("name");
        user.addStringProperty("email");
        user.addStringProperty("employeeId");
        // user.addContentProvider();
        //Department Entity
        Entity depatment = schema.addEntity("Deparmet");
        depatment.addIdProperty();
        depatment.addStringProperty("name");
        //Area Entity
        Entity area = schema.addEntity("Area");
        area.addIdProperty();
        area.addStringProperty("name");
        //Rules Entity
        Entity rule = schema.addEntity("rule");
        rule.addIdProperty();
        rule.addStringProperty("name");


        //IVL Entity

        Entity ivl = schema.addEntity("Ivl");
        ivl.addIdProperty();
        ivl.addStringProperty("creationDate");
        ivl.addStringProperty("startHour");
        ivl.addStringProperty("endHour");
        ivl.addLongProperty("responsibleId");
        ivl.addIntProperty("numPersons");
        ivl.addLongProperty("companionId");
        ivl.addStringProperty("areaName");
        ivl.addStringProperty("comments");
        ivl.addLongProperty("liderId");
        ivl.addLongProperty("contractorId");
        ivl.addStringProperty("activityType");
        ivl.addStringProperty("mainPlace");
        ivl.addStringProperty("interactionType");
        ivl.addIntProperty("syncState");

        //Entity ivl_images
        Entity ivl_images = schema.addEntity("IvlImages");
        ivl_images.addIdProperty();
        Property ivl_id = ivl_images.addLongProperty("ivlId").nonPrimitiveType().getProperty();
        ivl_images.addToOne(ivl, ivl_id);
        ivl.addToMany(ivl_images, ivl_id);
        ivl_images.addByteArrayProperty("image");

        //Ivl_actions
        Entity ivl_action = schema.addEntity("IvlAction");
        ivl_action.addIdProperty();
        ivl_action.addStringProperty("improveSituation");
        ivl_action.addStringProperty("improveComprimise");
        ivl_action.addLongProperty("responsibleId");
        ivl_action.addStringProperty("expirationDate");
        Property ivl_action_id = ivl_action.addLongProperty("ivlId").nonPrimitiveType().getProperty();
        ivl_action.addToOne(ivl, ivl_action_id);
        ivl.addToMany(ivl_action, ivl_action_id);


        //IVlResponsibles
        Entity ivlResponsible = schema.addEntity("IvlResposible");
        ivlResponsible.addIdProperty();
        ivlResponsible.addStringProperty("name");

        //ILVCatalog
        Entity ivlCatalog = schema.addEntity("IvlCatalog");
        ivlCatalog.addIdProperty();
        ivlCatalog.addStringProperty("name");
        ivlCatalog.addStringProperty("typeprof");

        //Contractors
        Entity contractor = schema.addEntity("Contractor");
        contractor.addIdProperty();
        contractor.addStringProperty("name");
        contractor.addStringProperty("contractornumber");
        incidents.addToOne(contractor, contractor_id);

        //IVLCatalogReg
        Entity ivlCatalogReg = schema.addEntity("IvlCatalogReg");
        ivlCatalogReg.addIdProperty();
        Property ivlreg_id = ivlCatalogReg.addLongProperty("ivlId").nonPrimitiveType().getProperty();
        ivlCatalogReg.addToOne(ivl, ivlreg_id);
        ivl.addToMany(ivlCatalogReg, ivlreg_id);
        Property ivlcatreg_id = ivlCatalogReg.addLongProperty("ivlcatId").nonPrimitiveType().getProperty();
        ivlCatalogReg.addToOne(ivlCatalog, ivlcatreg_id);
        ivlCatalog.addToMany(ivlCatalogReg, ivlcatreg_id);

        //Area Innspections
        Entity areaInspections = schema.addEntity("AreaInspections");
        areaInspections.addIdProperty();
        areaInspections.addLongProperty("creatorEmployee");
        areaInspections.addStringProperty("place");
        areaInspections.addLongProperty("reporterEmployee");
        areaInspections.addStringProperty("revisionDate");
        areaInspections.addStringProperty("description");
        areaInspections.addStringProperty("inspectedArea");
        areaInspections.addLongProperty("securityConsultor");
        areaInspections.addLongProperty("areaLider");
        areaInspections.addStringProperty("supervisors");
        areaInspections.addStringProperty("participants");


        //Area Inspection Actiones
        Entity areaInspectionAction = schema.addEntity("AreaInspectionAction");
        areaInspectionAction.addIdProperty();
        Property inspectionId = areaInspectionAction.addLongProperty("inspectionId").getProperty();
        areaInspections.addToMany(areaInspectionAction, inspectionId);
        areaInspectionAction.addStringProperty("place");
        areaInspectionAction.addStringProperty("exactPlace");
        areaInspectionAction.addStringProperty("actionType");
        areaInspectionAction.addStringProperty("inversion");
        areaInspectionAction.addStringProperty("causeDescription");
        areaInspectionAction.addStringProperty("correctiveAction");
        areaInspectionAction.addStringProperty("priority");
        Property user_exec_id = areaInspectionAction.addLongProperty("actionExecuteEesponsible").getProperty();
        // areaInspectionAction.addToOne(user, user_exec_id);
        areaInspectionAction.addStringProperty("initDate");
        areaInspectionAction.addStringProperty("limitDate");
        Property user_breach_id = areaInspectionAction.addLongProperty("breachInform").getProperty();
        // areaInspectionAction.addToOne(user, user_breach_id);
        areaInspectionAction.addStringProperty("closeEmployeesNotify");
        areaInspectionAction.addStringProperty("comment");
        areaInspectionAction.addIntProperty("syncState");

        //Area inspection action images
        Entity areaInspectionActionImages = schema.addEntity("AreaInsActionImages");
        areaInspectionActionImages.addIdProperty();
        Property areainspectionId = areaInspectionActionImages.addLongProperty("areaInspectionActionId").getProperty();
        areaInspectionAction.addToMany(areaInspectionActionImages, areainspectionId);
        areaInspectionActionImages.addToOne(areaInspectionAction, areainspectionId);
        areaInspectionActionImages.addByteArrayProperty("image");
        areaInspectionActionImages.addLongProperty("inspectionId");

        //Experiential Plan

        Entity experientialPlan = schema.addEntity("ExperientialPlan");
        experientialPlan.addIdProperty();
        experientialPlan.addStringProperty("registrationDate");
        experientialPlan.addLongProperty("name");
        experientialPlan.addLongProperty("coach");
        experientialPlan.addStringProperty("area");
        experientialPlan.addStringProperty("time");
        experientialPlan.addStringProperty("comment");
        experientialPlan.addStringProperty("desviation");
        experientialPlan.addStringProperty("personalType");
        experientialPlan.addIntProperty("syncState");

        //Vehicle
        Entity vehicle = schema.addEntity("Vehicle");
        vehicle.addIdProperty();
        vehicle.addStringProperty("vehicleId");
        vehicle.addStringProperty("internalId");
        vehicle.addStringProperty("externalId");

        //Driver
        Entity driver = schema.addEntity("Driver");
        driver.addIdProperty();
        driver.addStringProperty("ci");
        driver.addStringProperty("lastName");
        driver.addStringProperty("firstName");
        driver.addStringProperty("birthDate");
        driver.addStringProperty("address");
        driver.addStringProperty("state");
        driver.addStringProperty("area");
        driver.addStringProperty("photo");

        //VehicleDriver
        Entity vehicleDriver = schema.addEntity("VehicleDriver");
        vehicleDriver.addIdProperty();
        vehicleDriver.addLongProperty("driverId");
        vehicleDriver.addLongProperty("vehicleId");

        //SdCategoryIncident
        Entity sdCategoryIncident = schema.addEntity("SdCategoryIncident");
        sdCategoryIncident.addIdProperty();
        sdCategoryIncident.addStringProperty("name");

        //SdSubcategoryIncident
        Entity sdSubcategoryIncident = schema.addEntity("SdSubcategoryIncident");
        sdSubcategoryIncident.addIdProperty();
        sdSubcategoryIncident.addStringProperty("name");
        sdSubcategoryIncident.addLongProperty("categoryId");

        //risk
        Entity risk = schema.addEntity("Risk");
        risk.addIdProperty();
        risk.addStringProperty("name");

        //riskincident
        Entity riskincident = schema.addEntity("RiskIncident");
        riskincident.addIdProperty();
        riskincident.addLongProperty("incidentId");
        riskincident.addIntProperty("riskId");

        //general Audits
        Entity generalAudit = schema.addEntity("GeneralAudit");
        generalAudit.addIdProperty();
        generalAudit.addStringProperty("name");
        generalAudit.addStringProperty("recomendations");

        //general audit fiels
        Entity generalAuditField = schema.addEntity("GeneralAuditField");
        generalAuditField.addIdProperty();
        generalAuditField.addStringProperty("name");
        generalAuditField.addStringProperty("type");
        generalAuditField.addLongProperty("auditId");

        //general audit category
        Entity generalAuditCategory = schema.addEntity("GeneralAuditCategory");
        generalAuditCategory.addIdProperty();
        generalAuditCategory.addStringProperty("name");
        generalAuditCategory.addLongProperty("auditId");

        //general audit item
        Entity generalAuditItem = schema.addEntity("GeneralAuditItem");
        generalAuditItem.addIdProperty();
        generalAuditItem.addStringProperty("name");
        generalAuditItem.addStringProperty("options");
        generalAuditItem.addLongProperty("auditCategoryId");

        //app photos
        Entity appPhotos = schema.addEntity("AppPhotos");
        appPhotos.addIdProperty();
        appPhotos.addByteArrayProperty("photo");
        appPhotos.addStringProperty("type");
        appPhotos.addLongProperty("parentId");

        //generalauditregisrty
        Entity auditRegistry = schema.addEntity("AuditRegistry");
        auditRegistry.addIdProperty();
        auditRegistry.addLongProperty("generalAuditId");
        auditRegistry.addStringProperty("result");
        auditRegistry.addStringProperty("creatorUser");
        auditRegistry.addStringProperty("observation");
        auditRegistry.addStringProperty("registrationDate");
        auditRegistry.addLongProperty("auditorId");
        auditRegistry.addStringProperty("mainPlace");
        auditRegistry.addStringProperty("emails");
        auditRegistry.addIntProperty("synState");

        //generalauditcaregistry
        Entity auditFieldRegistry = schema.addEntity("AuditFieldRegistry");
        auditFieldRegistry.addIdProperty();
        auditFieldRegistry.addLongProperty("fieldId");
        auditFieldRegistry.addStringProperty("value");
        auditFieldRegistry.addLongProperty("auditId");

        //generalAuditFieldRegistry
        Entity auditItemRegistry = schema.addEntity("AuditItemRegistry");
        auditItemRegistry.addIdProperty();
        auditItemRegistry.addLongProperty("categoryId");
        auditItemRegistry.addLongProperty("itemId");
        auditItemRegistry.addStringProperty("selectedOption");
        auditItemRegistry.addLongProperty("auditId");
    }


}
