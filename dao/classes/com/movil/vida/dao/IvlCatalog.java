package com.movil.vida.dao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.List;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "IVL_CATALOG".
 */
@Entity(active = true)
public class IvlCatalog {

    @Id
    private Long id;
    private String name;
    private String typeprof;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient IvlCatalogDao myDao;

    @ToMany(joinProperties = {
            @JoinProperty(name = "id", referencedName = "ivlcatId")
    })
    private List<IvlCatalogReg> ivlCatalogRegList;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public IvlCatalog() {
    }

    public IvlCatalog(Long id) {
        this.id = id;
    }

    @Generated
    public IvlCatalog(Long id, String name, String typeprof) {
        this.id = id;
        this.name = name;
        this.typeprof = typeprof;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getIvlCatalogDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeprof() {
        return typeprof;
    }

    public void setTypeprof(String typeprof) {
        this.typeprof = typeprof;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    @Generated
    public List<IvlCatalogReg> getIvlCatalogRegList() {
        if (ivlCatalogRegList == null) {
            __throwIfDetached();
            IvlCatalogRegDao targetDao = daoSession.getIvlCatalogRegDao();
            List<IvlCatalogReg> ivlCatalogRegListNew = targetDao._queryIvlCatalog_IvlCatalogRegList(id);
            synchronized (this) {
                if (ivlCatalogRegList == null) {
                    ivlCatalogRegList = ivlCatalogRegListNew;
                }
            }
        }
        return ivlCatalogRegList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated
    public synchronized void resetIvlCatalogRegList() {
        ivlCatalogRegList = null;
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
