package com.movil.vida.dao;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.ToOne;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "IVL_IMAGES".
 */
@Entity(active = true)
public class IvlImages {

    @Id
    private Long id;
    private Long ivlId;
    private byte[] image;

    /** Used to resolve relations */
    @Generated
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated
    private transient IvlImagesDao myDao;

    @ToOne(joinProperty = "ivlId")
    private Ivl ivl;

    @Generated
    private transient Long ivl__resolvedKey;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated
    public IvlImages() {
    }

    public IvlImages(Long id) {
        this.id = id;
    }

    @Generated
    public IvlImages(Long id, Long ivlId, byte[] image) {
        this.id = id;
        this.ivlId = ivlId;
        this.image = image;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getIvlImagesDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIvlId() {
        return ivlId;
    }

    public void setIvlId(Long ivlId) {
        this.ivlId = ivlId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    /** To-one relationship, resolved on first access. */
    @Generated
    public Ivl getIvl() {
        Long __key = this.ivlId;
        if (ivl__resolvedKey == null || !ivl__resolvedKey.equals(__key)) {
            __throwIfDetached();
            IvlDao targetDao = daoSession.getIvlDao();
            Ivl ivlNew = targetDao.load(__key);
            synchronized (this) {
                ivl = ivlNew;
            	ivl__resolvedKey = __key;
            }
        }
        return ivl;
    }

    @Generated
    public void setIvl(Ivl ivl) {
        synchronized (this) {
            this.ivl = ivl;
            ivlId = ivl == null ? null : ivl.getId();
            ivl__resolvedKey = ivlId;
        }
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void delete() {
        __throwIfDetached();
        myDao.delete(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void update() {
        __throwIfDetached();
        myDao.update(this);
    }

    /**
    * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
    * Entity must attached to an entity context.
    */
    @Generated
    public void refresh() {
        __throwIfDetached();
        myDao.refresh(this);
    }

    @Generated
    private void __throwIfDetached() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
